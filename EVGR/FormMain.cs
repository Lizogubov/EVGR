﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Windows.Shapes;
using EVGR.Properties;
using Color = System.Drawing.Color;
using Path = System.IO.Path;
using Rectangle = System.Drawing.Rectangle;

namespace EVGR
{
   public partial class FormMain : Form
   {
      private PanelProperties panelProps = null;
      private Figures figures;
      //цвет для передачи в окна св-в фигур
      private Color ColorToChangeButton;
      //создаем массив кнопок палитры цветов
      private Button[] palleteButtons;
      //св-во для возможности выбора цвета контура 
      private bool colorContoured;

      public FormMain()
      {
         ////////////////////////
         InitializeComponent();
         /////////////////////// 
         //
         DoubleBuffered = true;
         figures = new Figures(this);
         pnPanelDrawing.GetReferenceOnFormMain(this);
         new CounterFPS(this);
         //инициализируем массив кнопок палитры цветов
         palleteButtons = new Button[]
         {
            btColor1, btColor2, btColor3, btColor4, btColor5,
            btColor6, btColor7, btColor8, btColor9, btColor10,
            btColor11, btColor12, btColor13, btColor14, btColor15,
            btColor16, btColor17, btColor18, btColor19, btColor20,
         };

      }

      public Panel GetPanel_properties_lines
      {
         get { return pnLinesProps; }
      }

      public Panel GetPanel_properties_ellipses
      {
         get { return pnEllipsesProps; }
      }

      public Panel GetPanel_properties_polygons
      {
         get { return pnPolygonsProps; }
      }

      public Panel GetPanel_properties_rectangles
      {
         get { return pnRectanglesProps; }
      }

      public Panel GetPanel_properties_pies
      {
         get { return pnPiesProps; }
      }

      public Panel GetPanel_properties_arcs
      {
         get { return pnArcsProps; }
      }

      public Button GetButtonChangeColor
      {
         get { return btColorFilling; }
      }

      public Button GetButtonChangeContourColor
      {
         get { return btColorContour; }
      }

      public NumericUpDown GetNumericUpDown_Width
      {
         get { return numWidth; }
      }

      public ToolStripStatusLabel GetLabelCoordinates
      {
         get { return lbCoordinates; }
      }

      public ToolStripStatusLabel GetLabelSize
      {
         get { return lbSize; }
      }

      public RichTextBox GetJournalRichTextBox
      {
         get { return tbJornal; }
      }

      public PanelProperties PanelProps
      {
         get { return panelProps; }
         set { panelProps = value; }
      }

      public Figures Figures
      {
         get { return figures; }
         set { figures = value; }
      }

      #region Обработка событий нажатия кнопок/чекбоксов                                                       

      // Изменение цвета фона 
      private void cbPanelDrawingBackground_SelectedIndexChanged(object sender, EventArgs e)
      {
         //изменение цвета окна пользователя на цвет, выбранный самим пользователем
         int id = ((ComboBox) sender).SelectedIndex;
         //для изменения цвета окна пользователя исходя из его значения по умолчанию
         if (id > 19)
            id = 19;
         pnPanelDrawing.BackColor = palleteButtons[id].BackColor;
      }

      private void cbPanelDrawingBackground_MouseClick(object sender, MouseEventArgs e)
      {
         Color[] colors = GetColors();
         DisplayColorSamples(cbPanelDrawingBackground, colors);
      }

      private void cbPanelDrawingBackground_DrawItem(object sender, DrawItemEventArgs e)
      {
         // Отступы вокруг прямоугольников с цветами
         const int MarginWidth = 2, MarginHeight = 2;

         {
            if (e.Index < 0) return;
            // Очистка фона комбобокса
            e.DrawBackground();

            // Отрисовка прямоугольника с цветом
            int hgt = e.Bounds.Height - 2*MarginHeight;
            Rectangle rect = new Rectangle(e.Bounds.X + MarginWidth,
               e.Bounds.Y + MarginHeight, hgt + 60, hgt - 1);
            ComboBox cbo = sender as ComboBox;
            Color color = (Color) cbo.Items[e.Index];
            using (SolidBrush brush = new SolidBrush(color))
            {
               e.Graphics.FillRectangle(brush, rect);
            }

            // Окантовка
            e.Graphics.DrawRectangle(Pens.Black, rect);

            // Фокус на выбранном цвете
            e.DrawFocusRectangle();
         }
      }

      private void cbContourStyle_MouseClick(object sender, MouseEventArgs e)
      {
         // запоминаем индекс текущего элемента
         var curIndex = cbContourStyle.SelectedIndex;
         // очищаем комбобокс
         cbContourStyle.Items.Clear();
         // добавляем стили в комбобокс за исключением последнего, т.к. он такой же как и первый
         var styles = Enum.GetValues(typeof (DashStyle));
         foreach (var style in styles)
         {
            if ((int) style != (styles.Length - 1))
               cbContourStyle.Items.Add(style);
         }
         cbContourStyle.SelectedIndex = curIndex;
         // событие отрисовки для комбобокса с выбором фона
         cbContourStyle.DrawItem += cbContourStyle_DrawItem;

      }

      private void cbContourStyle_DrawItem(object sender, DrawItemEventArgs e)
      {
         if (e.Index < 0) return;
         // Очистка фона комбобокса
         e.DrawBackground();

         Pen dashPen = new Pen(Color.Black, 4);
         dashPen.DashStyle = (DashStyle) cbContourStyle.Items[e.Index];
         using (dashPen)
         {
            e.Graphics.DrawLine(dashPen, new PointF(e.Bounds.X + 2, e.Bounds.Y + e.Bounds.Height/2),
               new PointF(e.Bounds.X + e.Bounds.Width - 4, e.Bounds.Y + e.Bounds.Height/2));
         }
         // Фокус на выбранном стиле контура
         e.DrawFocusRectangle();
      }

      private void cbContourStyle_SelectedIndexChanged(object sender, EventArgs e)
      {
         SwitchConditions.SetDashStyle(cbContourStyle.SelectedIndex);

         if (FigureJustDrawn())
         {
            Figures.Poryadok.Last().DashStyle = SwitchConditions.MyDashStyle;
         }
         else if (SomeFigureSelected())
         {
            Figures.Poryadok.Find(x => x.IsSelected == true).DashStyle = SwitchConditions.MyDashStyle;
         }

      }

      private void cbHatchStyle_SelectedIndexChanged(object sender, EventArgs e)
      {
         SwitchConditions.SetHatchStyle(cbHatchStyle.SelectedIndex);

         if (FigureJustDrawn())
         {
            if (cbHatchStyle.SelectedIndex != 0)
            {
               Figures.Poryadok.Last().HatchStyleOn = true;
               Figures.Poryadok.Last().HatchStyle = SwitchConditions.MyHatchStyle;
            }
            else Figures.Poryadok.Last().HatchStyleOn = false;
         }
         else if (SomeFigureSelected())
         {
            if (cbHatchStyle.SelectedIndex != 0)
            {
               Figures.Poryadok.Find(x => x.IsSelected == true).HatchStyleOn = true;
               Figures.Poryadok.Find(x => x.IsSelected == true).HatchStyle = SwitchConditions.MyHatchStyle;
            }
            else Figures.Poryadok.Find(x => x.IsSelected == true).HatchStyleOn = false;

         }
      }

      private void numWidth_ValueChanged(object sender, EventArgs e)
      {
         if (FigureJustDrawn())
            Figures.Poryadok.Last().PenThickness = (float) numWidth.Value;
         else if (SomeFigureSelected())
            Figures.Poryadok.Find(x => x.IsSelected == true).PenThickness = (float) numWidth.Value;
      }

      private void btColorFilling_BackColorChanged(object sender, EventArgs e)
      {
         if (FigureJustDrawn())
            Figures.Poryadok.Last().ColorFilling = btColorFilling.BackColor;
         else if (SomeFigureSelected())
            Figures.Poryadok.Find(x => x.IsSelected == true).ColorFilling = btColorFilling.BackColor;
      }

      private void btColorContour_BackColorChanged(object sender, EventArgs e)
      {
         if (FigureJustDrawn())
            Figures.Poryadok.Last().ColorContour = btColorContour.BackColor;
         else if (SomeFigureSelected())
            Figures.Poryadok.Find(x => x.IsSelected == true).ColorContour = btColorContour.BackColor;
      }

      // Включение\выключение рисования с закрашиванием фигуры
      private void checkBox_Filling_CheckedChanged(object sender, EventArgs e)
      {
         if (cbFilling.Checked)
         {
            if (FigureJustDrawn())
            {
               Figures.Poryadok.Last().Filled = true;
               Figures.Poryadok.Last().Contoured = false;
            }
            else if (SomeFigureSelected())
            {
               Figures.Poryadok.Find(x => x.IsSelected == true).Filled = true;
               Figures.Poryadok.Find(x => x.IsSelected == true).Contoured = false;
            }
            SwitchConditions.Filled = true;
            cbCountoring.Checked = false;
            SwitchConditions.Contoured = false;

         }
         else if (!cbFilling.Checked)
         {
            if (FigureJustDrawn())
            {
               Figures.Poryadok.Last().Filled = false;
            }
            else if (SomeFigureSelected())
            {
               Figures.Poryadok.Find(x => x.IsSelected == true).Filled = false;
            }
            SwitchConditions.Filled = false;

         }
      }

      // Включение\выключение рисования с контурированием фигуры
      private void checkBox_Countoring_CheckedChanged(object sender, EventArgs e)
      {
         if (cbCountoring.Checked)
         {
            if (FigureJustDrawn())
            {
               Figures.Poryadok.Last().Contoured = true;
               Figures.Poryadok.Last().Filled = false;
            }
            else if (SomeFigureSelected())
            {
               Figures.Poryadok.Find(x => x.IsSelected == true).Contoured = true;
               Figures.Poryadok.Find(x => x.IsSelected == true).Filled = false;
            }
            SwitchConditions.Contoured = true;
            cbFilling.Checked = false;
            SwitchConditions.Filled = false;
         }
         else if (!cbCountoring.Checked)
         {
            if (FigureJustDrawn())
            {
               Figures.Poryadok.Last().Contoured = false;
            }
            else if (SomeFigureSelected())
            {
               Figures.Poryadok.Find(x => x.IsSelected == true).Contoured = false;
            }

            SwitchConditions.Contoured = false;

         }
      }

      // Древо параметров со всплывающими панелями
      private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
      {
         /* сделать невидимой панель, выбранную ранее*/
         if (twMain.SelectedNode.Checked = Visible)
         {
            pnForPallette.Visible = false;
            pnExtension.Visible = false;
            pnBackground.Visible = false;
            pnFiguresToDraw.Visible = false;
         }
         switch (((TreeView) sender).SelectedNode.Name)
         {
            case "NodeExtension":
               pnExtension.Visible = true;
               break;
            case "NodePallette":
               pnForPallette.Visible = true;
               break;
            case "NodeBackground":
               pnBackground.Visible = true;
               break;
            case "NodeGeomFigures":
               pnFiguresToDraw.Visible = true;
               break;
         }
      }

      // Выбор цвета для фигур при рисовании
      private void button_figures_color_MouseUp(object sender, MouseEventArgs e)
      {
         if (e.Button == System.Windows.Forms.MouseButtons.Left)
         {
            pnForPallette.Visible = true;
            colorContoured = false;
         }
         if (e.Button == System.Windows.Forms.MouseButtons.Right)
         {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = true;
            MyDialog.ShowHelp = true;
            MyDialog.Color = btColorFilling.ForeColor;
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
               if (pnFiguresToDraw.Visible)
                  btColorFilling.BackColor = MyDialog.Color;
            }
         }
      }

      // Выбор цвета контура
      private void button_figures_colorContour_MouseUp(object sender, MouseEventArgs e)
      {
         if (e.Button == System.Windows.Forms.MouseButtons.Left)
         {
            pnForPallette.Visible = true;
            colorContoured = true;
         }
         if (e.Button == System.Windows.Forms.MouseButtons.Right)
         {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = true;
            MyDialog.ShowHelp = true;
            MyDialog.Color = btColorFilling.ForeColor;
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
               if (pnFiguresToDraw.Visible)
                  btColorContour.BackColor = MyDialog.Color;
            }
         }
      }

      // Журнал событий (показать\скрыть)
      private void button_journal_Click(object sender, EventArgs e)
      {
         if (pnForJournal.Visible)
            pnForJournal.Visible = false;
         else
            pnForJournal.Visible = true;
      }


      // Включение рисования линии
      private void button_LineDrawing_Click(object sender, EventArgs e)
      {
         DrawingDifferentFiguresOn("lineDrawing", false, false);
         ChangeImageOfFigure(sender);
      }

      // Включение рисования прямоугольника
      private void button_RectangleDrawing_Click(object sender, EventArgs e)
      {
         DrawingDifferentFiguresOn("rectangleDrawing", true, true);
         ChangeImageOfFigure(sender);
      }

      // Включение рисования многоугольника
      private void button_PolygonDrawing_Click(object sender, EventArgs e)
      {
         DrawingDifferentFiguresOn("polygonDrawing", true, true);
         ChangeImageOfFigure(sender);
      }

      // Включение рисования сектора
      private void button_PieDrawing_Click(object sender, EventArgs e)
      {
         DrawingDifferentFiguresOn("pieDrawing", true, true);
         ChangeImageOfFigure(sender);
      }

      // Включение рисования эллипса
      private void button_EllipseDrawing_Click(object sender, EventArgs e)
      {
         DrawingDifferentFiguresOn("ellipseDrawing", true, true);
         ChangeImageOfFigure(sender);
      }

      // Включение рисования дуги
      private void button_ArcDrawing_Click(object sender, EventArgs e)
      {
         DrawingDifferentFiguresOn("arcDrawing", false, false);
         ChangeImageOfFigure(sender);
      }

      // Выбор цвета на палитре
      public void button_color_pallete_MouseUp(object sender, MouseEventArgs e)
      {
         int id = -1;
         if (e.Button == System.Windows.Forms.MouseButtons.Left)
         {
            //выбор цвета фигур через палитру
            if (pnForPallette.Visible)
            {
               id = DefiningOfPressedColorButton(sender, id);
               if (!colorContoured && pnFiguresToDraw.Visible)
                  btColorFilling.BackColor = palleteButtons[id].BackColor;
               else if (colorContoured && pnFiguresToDraw.Visible)
                  btColorContour.BackColor = palleteButtons[id].BackColor;
               else if (panelProps != null)
                  ChoosingColor_in_PanelPropertiesIntegrated(id);
            }
         }
         // создание возможности выбора цвета пользователем самостоятельно
         else if (e.Button == System.Windows.Forms.MouseButtons.Right)
         {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = true;
            MyDialog.ShowHelp = true;
            MyDialog.Color = btColor1.ForeColor;
            id = DefiningOfPressedColorButton(sender, id);
            // замена цвета кнопки на выбранный из стандартной палитры
            if (MyDialog.ShowDialog() == DialogResult.OK && id != -1)
               palleteButtons[id].BackColor = MyDialog.Color;
         }
      }

      // Кнопка для инициализации выделения
      private void button_select_Click(object sender, EventArgs e)
      {
         SwitchConditions.ChangeCondition("selecting");
         ChangeImageOfFigure(sender);
      }

      // Очистка экрана, журнала и всех коллекций
      private void miNew_Click(object sender, EventArgs e)
      {
         DialogResult dr =
            MessageBox.Show("Вы уверены, что хотите создать новый документ? Информация не будет сохранена!",
               "Создание нового документа.", MessageBoxButtons.YesNo);
         if (dr == DialogResult.Yes)
         {
            tbJornal.Clear();
            figures.ClearAllCollections();
            pnArcsProps.Controls.Clear();
            pnLinesProps.Controls.Clear();
            pnEllipsesProps.Controls.Clear();
            pnPiesProps.Controls.Clear();
            pnPolygonsProps.Controls.Clear();
            pnRectanglesProps.Controls.Clear();
            // состояния в false
            SwitchConditions.PolygonPressed = false;
            SwitchConditions.LinePressed = false;
            SwitchConditions.RectanglePressed = false;
         }
      }

      // Метод для открытия файла
      private void miOpen_Click(object sender, EventArgs e)
      {
         OpenFileDialog dialog = new OpenFileDialog();
         dialog.Filter = "Файл EVGR  (*.gr)|*.gr";
         dialog.FilterIndex = 1;
         dialog.RestoreDirectory = true;
         dialog.Multiselect = false;
         try
         {
            dialog.InitialDirectory =
               Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Environment.CurrentDirectory))) +
               @"\EVGR\saves";

            if (dialog.ShowDialog() != DialogResult.OK)
               return;
            string path = dialog.FileName;
            if (dialog.FilterIndex == 1)
            {
               LoadAsBinary(path);
            }
         }
         catch (Exception exp)
         {
            MessageBox.Show("Не удалось загрузить файл!" + exp.Message);
         }
      }


      // Метод для сохранения файла
      private void miSaveAs_Click(object sender, EventArgs e)
      {
         SaveFileDialog dialog = new SaveFileDialog();
         SetDialogOptions(dialog);
         string path = "";
         try
         {
            dialog.InitialDirectory =
               Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Environment.CurrentDirectory))) +
               @"\EVGR\saves";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
               this.Text = "Файл [" + dialog.FileName + "] ";
               path = dialog.FileName + "." + dialog.DefaultExt;
            }
            else return;

            if (dialog.FilterIndex == 1)
            {
               SaveAsBinary(path);
            }
            else if (dialog.FilterIndex == 2)
            {
               SaveAsJPEG(path);
            }
            else if (dialog.FilterIndex == 3)
            {
               using (Bitmap bmpSave = new Bitmap(pnPanelDrawing.Width, pnPanelDrawing.Height))
               {
                  // Отрисовываем наше изображение с панели на битмап, чтобы затем его сохранить
                  pnPanelDrawing.DrawToBitmap(bmpSave, pnPanelDrawing.Bounds);
                  bmpSave.Save(path, ImageFormat.Png);
               }
            }
            MessageBox.Show("=>Saved as " + path);
         }
         catch (Exception exp)
         {
            MessageBox.Show("Не удалось сохранить файл!" + exp.Message);
         }
      }



      // Событие, срабатывающее при закрытии программы
      private void Form_main_FormClosing(object sender, FormClosingEventArgs e)
      {
         /* DialogResult dr =  MessageBox.Show("Вы уверены, что хотите закрыть приложение? Информация не будет сохранена!",
                " Приложение будет закрыто.",  MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
                e.Cancel = false;
            else e.Cancel = true;*/
      }

      // Загрузка формы
      private void Form_main_Load(object sender, EventArgs e)
      {

      }

      // Кнопки для работы со свойствами фигур 

      private void button_properties_linesOnOff_Click(object sender, EventArgs e)
      {
         ShowHidePnPropFigures(btLinesPropsOnOff, pnLinesProps);
      }

      private void button_properties_rectanglesOnOff_Click(object sender, EventArgs e)
      {
         ShowHidePnPropFigures(btRectanglesPropsOnOff, pnRectanglesProps);
      }

      private void button_properties_piesOnOff_Click(object sender, EventArgs e)
      {
         ShowHidePnPropFigures(btPiesPropsOnOff, pnPiesProps);
      }

      private void button_properties_ellipsesOnOff_Click(object sender, EventArgs e)
      {
         ShowHidePnPropFigures(btEllipsesPropsOnOff, pnEllipsesProps);
      }

      private void button_properties_arcsOnOff_Click(object sender, EventArgs e)
      {
         ShowHidePnPropFigures(btArcsPropsOnOff, pnArcsProps);
      }

      private void button_properties_polygonsOnOff_Click(object sender, EventArgs e)
      {
         ShowHidePnPropFigures(btPolygonsPropsOnOff, pnPolygonsProps);
      }

      // Изменение масштаба
      private void buttonScaling_MouseDown(object sender, MouseEventArgs e)
      {
         ChangeImageOfFigure(sender);
         if (e.Button == MouseButtons.Left)
         {
            pnPanelDrawing.ZoomIn();
         }
         else if (e.Button == MouseButtons.Right)
         {
            pnPanelDrawing.ZoomOut();
         }
      }

      private void btSetPanelDrawingSize_Click(object sender, EventArgs e)
      {
         // ширина, высота
         uint X, Y;
         if (!uint.TryParse(tbPanelDrawingX.Text, out X) || !uint.TryParse(tbPanelDrawingY.Text, out Y) ||
             X > 5000 || Y > 5000)
         {
            MessageBox.Show("Значения должны быть положительными, целыми и меньше 5000x5000  !", "Повторите ввод");
         }
         else
         {
            pnPanelDrawing.SetDefSize(X, Y);
         }
      }

      // Включение сглаживания
      private void ToolStripMenuItem_antialiasing_on_Click(object sender, EventArgs e)
      {
         SwitchConditions.Antialiasing = true;
         miAntialiasing_on.CheckState = CheckState.Checked;
         miAntialiasing_off.CheckState = CheckState.Unchecked;
      }

      // Выключение сглаживания
      private void ToolStripMenuItem_antialiasing_off_Click(object sender, EventArgs e)
      {
         SwitchConditions.Antialiasing = false;
         miAntialiasing_on.CheckState = CheckState.Unchecked;
         miAntialiasing_off.CheckState = CheckState.Checked;
      }

      // Выход из программы
      private void ToolStripMenuItemExit_Click(object sender, EventArgs e)
      {
         this.Close();
      }

      #endregion

      #region Вспомогательные методы 

      /// <summary>
      ///  Метод проверяет, была ли нарисована хоть одна фигура и выделена ли она?
      /// </summary>
      private bool FigureJustDrawn()
      {
         return Figures.Poryadok.Count != 0 && Figures.Poryadok.Last().IsSelected;
      }

      /// <summary>
      /// Если выделена какая-нибудь фигура
      /// </summary>
      /// <returns></returns>
      private bool SomeFigureSelected()
      {
         if (Figures.Poryadok.Find(x => x.IsSelected == true) != null)
            return true;
         else return false;
      }

      /// <summary>
      /// Включение\выключение рисования, обводки, закраски для разных фигур
      /// </summary>
      private void DrawingDifferentFiguresOn(string figure, bool isCountoring, bool isFilling)
      {
         SwitchConditions.ChangeCondition(figure);
         cbCountoring.Enabled = isCountoring;
         cbFilling.Enabled = isFilling;
      }

      /// <summary>
      /// Определение нажатой кнопки по сендеру
      /// </summary>
      private int DefiningOfPressedColorButton(object sender, int id)
      {
         for (int i = 0; i < palleteButtons.Length; ++i)
         {
            if (palleteButtons[i] == sender) id = i;
         }
         return id;
      }

      /// <summary>
      /// Выбор цвета на панеле свойств фигур, встроенной в форму справа 
      /// </summary>
      private void ChoosingColor_in_PanelPropertiesIntegrated(int id)
      {
         ColorToChangeButton = palleteButtons[id].BackColor;
         if (panelProps.BtColorContour != null &&
             panelProps.Index == 2)
         {
            panelProps.BtColorContour.BackColor = ColorToChangeButton;
            panelProps.EditedFigure.ColorContour = ColorToChangeButton;
         }
         else if (panelProps.BtColorPen != null &&
                  panelProps.Index == 1)
         {
            panelProps.BtColorPen.BackColor = ColorToChangeButton;
            panelProps.EditedFigure.ColorFilling = ColorToChangeButton;
         }
         panelProps = null;
      }


      /// <summary>
      /// Показать\скрыть панель с фигурами
      /// </summary>
      private void ShowHidePnPropFigures(System.Windows.Forms.Button button, System.Windows.Forms.Panel panelWithFigures)
      {
         Cursor = Cursors.WaitCursor;
         if (panelWithFigures.Visible == false)
            panelWithFigures.Visible = true;
         else panelWithFigures.Visible = false;
         Cursor = Cursors.Default;
      }

      /// <summary>
      /// Изменение цвета в окне редактирования фигур
      /// </summary>
      public void Panel_Pallette_ChangeVisible()
      {
         pnForPallette.Visible = true;
         pnFiguresToDraw.Visible = false;
      }

      /// <summary>
      /// Получаем цвета
      /// </summary>
      /// <returns></returns>
      private Color[] GetColors()
      {
         Color[] colors = new Color[20];
         for (int i = 0; i < palleteButtons.Length; i++)
         {
            colors[i] = palleteButtons[i].BackColor;
         }
         return colors;
      }

      /// <summary>
      /// Отображаем цвета в комбобоксе
      /// </summary>
      public void DisplayColorSamples(
         ComboBox cbo, Color[] colors)
      {
         // запоминаем индекс текущего элемента
         var curIndex = cbPanelDrawingBackground.SelectedIndex;
         // очищаем комбобокс
         cbo.Items.Clear();
         // добавляем цвета в комбобокс
         foreach (Color color in colors)
            cbPanelDrawingBackground.Items.Add(color);
         // возвращаем индекс текущего элемента
         cbPanelDrawingBackground.SelectedIndex = curIndex;
         // событие отрисовки для комбобокса с выбором фона
         cbPanelDrawingBackground.DrawItem += cbPanelDrawingBackground_DrawItem;
      }

      private void LoadAsBinary(string path)
      {
         BinaryFormatter binFormat = new BinaryFormatter();
         using (Stream fStream = new FileStream(path,
            FileMode.Open, FileAccess.Read, FileShare.None))
         {
            var allFigures = (List<Figures>) binFormat.Deserialize(fStream);
            var journal = (string) binFormat.Deserialize(fStream);
            figures.FillingCollectionsWithLoaded(allFigures);
            tbJornal.Text = journal;
            fStream.Flush();
         }
      }

      private void SaveAsJPEG(string path)
      {
         using (Bitmap bmpSave = new Bitmap(pnPanelDrawing.Width, pnPanelDrawing.Height))
         {
            // Отрисовываем наше изображение с панели на битмап, чтобы затем его сохранить
            pnPanelDrawing.DrawToBitmap(bmpSave, pnPanelDrawing.Bounds);
            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            Encoder myEncoder = Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            // Устанавливаем уровень качества в 100 - максимальный , с минимальным сжатием
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            bmpSave.Save(path, jpgEncoder, myEncoderParameters);
         }
      }

      private ImageCodecInfo GetEncoder(ImageFormat format)
      {

         ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

         foreach (ImageCodecInfo codec in codecs)
         {
            if (codec.FormatID == format.Guid)
            {
               return codec;
            }
         }
         return null;
      }

      private void SaveAsBinary(string path)
      {
         BinaryFormatter binFormat = new BinaryFormatter();
         using (Stream fStream = new FileStream(path,
            FileMode.Create, FileAccess.Write, FileShare.None))
         {
            binFormat.Serialize(fStream, figures.Poryadok);
            binFormat.Serialize(fStream, tbJornal.Text);
            fStream.Flush();
         }
      }

      private static void SetDialogOptions(SaveFileDialog dialog)
      {
         dialog.OverwritePrompt = true;
         dialog.Filter = "Файл EVGR  (*.gr)|*.gr|JPEG-файл (*.jpeg, *.jpg)|*.jpeg|PNG-файл (*.png)|*.png";
         dialog.FilterIndex = 1;
         dialog.RestoreDirectory = true;
         dialog.FileName = "Новый файл";
      }


      #endregion

      private void miAboutProgram_Click(object sender, EventArgs e)
      {
         MessageBox.Show("Данная программа позволяет создавать/обрабатывать/сохранять изображения в векторном формате," +
                         " а также позволяет сохранять изображения в растровых форматах JPEG и PNG.",
            "О программе");
      }

      private void miAboutCreator_Click(object sender, EventArgs e)
      {
         MessageBox.Show("Данное приложение было разработано студентом 533 группы Лизогубовым Артемом.",
            "Об авторе");
      }

      private void cbHatchStyle_MouseClick(object sender, MouseEventArgs e)
      {
         // запоминаем индекс текущего элемента
         var curIndex = cbHatchStyle.SelectedIndex;
         // очищаем комбобокс
         cbHatchStyle.Items.Clear();

         // добавляем стили в комбобокс
         var styles = Enum.GetValues(typeof (HatchStyle));
         cbHatchStyle.Items.Add("Сплошной цвет");
         int num = 0;
         foreach (var style in styles)
         {
            if (num != 0 && num != 5 && num != 6)
               cbHatchStyle.Items.Add(style);
            num++;
         }
         cbHatchStyle.SelectedIndex = curIndex;
         // событие отрисовки для комбобокса с выбором фона
         cbHatchStyle.DrawItem += cbHatchStyle_DrawItem;
      }

      private string[] Array = 
      {
         "Сплошной цвет",
         "Горизонтальная",
         "Вертикальная",
         "Диагональная вниз",
         "Диагональная вверх",
         "Крупная клетка",
         "Диагональная клетка",
         "05 процентов",
         "10 процентов",
         "20 процентов",
         "25 процентов",
         "30 процентов",
         "40 процентов",
         "50 процентов",
         "60 процентов",
         "70 процентов",
         "75 процентов",
         "80 процентов",
         "90 процентов",
         "Легкая диагональная вниз",
         "Легкая диагональная вверх",
         "Плотная диагональная вниз",
         "Плотная диагональная вверх",
         "Широкая диагональная вниз",
         "Широкая диагональная вверх",
         "Легкая вевртикальная",
         "Легкая горизонтальная",
         "Узкая вертикальная",
         "Узкая горизонтальная",
         "Плотная вертикальная",
         "Плотная горизонтальная",
         "Штриховая диагональная вниз",
         "Штриховая диагональная вверх",
         "Штриховая горизонтальная",
         "Штриховая вертикальная",
         "Мелкие конфетти",
         "Крупные конфетти",
         "Зигзаг",
         "Волна",
         "Диагональный кирпич",
         "Горизонтальный кирпич",
         "Плетенка",
         "Шотландка",
         "Уголки",
         "Точечная сетка",
         "Точечные ромбики",
         "Дранка",
         "Шпалера",
         "Сферы",
         "Мелкая клетка",
         "Мелкая шахматная доска",
         "Крупная шахматная доска",
         "Контурные ромбики",
         "Ромбики"
      };

      private void cbHatchStyle_DrawItem(object sender, DrawItemEventArgs e)
      {
         if (e.Index < 0) return;
         // Очистка фона комбобокса
         e.DrawBackground();
         // Отрисовка небольших кусков текстур в каждой строке комбобокса
         int hgt = e.Bounds.Height;
         Rectangle rect = new Rectangle(e.Bounds.X, e.Bounds.Y, hgt*2, hgt);
         ComboBox cbo = sender as ComboBox;
         if (e.Index > 0)
         {
            var curHatch = (HatchStyle) cbHatchStyle.Items[e.Index];
            using (HatchBrush brush = new HatchBrush(curHatch, Color.Black, Color.White))
            {
               e.Graphics.FillRectangle(brush, rect);
            }
         }
         // Отрисовка названий текстур справа от их рисунка
         using (Font font = new Font(cbo.Font.FontFamily,
            cbo.Font.Size, FontStyle.Regular))
         {
            using (StringFormat sf = new StringFormat())
            {
               sf.Alignment = StringAlignment.Near;
               sf.LineAlignment = StringAlignment.Center;
               int x = hgt*2;
               int y = e.Bounds.Y + e.Bounds.Height/2;
               e.Graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
               e.Graphics.DrawString(Array[e.Index], font,
                  Brushes.Black, x, y, sf);
            }
         }
         // Фокус на выбранном стиле контура
         e.DrawFocusRectangle();
      }


      /// <summary>
      /// Метод меняет картинку кнопки (например, при переключении с рисования линии на выделение) 
      /// </summary>
      private void ChangeImageOfFigure(object sender)
      {
         btLineDrawing.Image = Resources.Line;
         btRectangleDrawing.Image = Resources.Rectangle;
         btPolygonDrawing.Image = Resources.Polygon;
         btPieDrawing.Image = Resources.Pie; 
         btEllipseDrawing.Image = Resources.Ellipse; 
         btArcDrawing.Image = Resources.Arc;
         btSelect.Image = Resources.select;
         btScaling.BackgroundImage = Resources.zoom;

         if (((Button)sender).Name == "btLineDrawing") 
            btLineDrawing.Image = Resources.LinePressed; 
         else if (((Button)sender).Name == "btRectangleDrawing")
            btRectangleDrawing.Image = Resources.RectanglePressed;
         else if (((Button)sender).Name == "btPolygonDrawing")
            btPolygonDrawing.Image = Resources.PolygonPressed;
         else if (((Button)sender).Name == "btPieDrawing")
            btPieDrawing.Image = Resources.PiePressed;
         else if (((Button)sender).Name == "btEllipseDrawing")
            btEllipseDrawing.Image = Resources.EllipsePressed;
         else if (((Button)sender).Name == "btArcDrawing")
            btArcDrawing.Image = Resources.ArcPressed;
         else if (((Button)sender).Name == "btSelect")
            btSelect.Image = Resources.selectPressed;
         else if (((Button)sender).Name == "btScaling")
            btScaling.BackgroundImage = Resources.zoomPressed;
      }

      private void pnPanelDrawing_SizeChanged(object sender, EventArgs e)
      {

      }

     

      
   }
}