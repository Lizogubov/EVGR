﻿using System.Windows.Forms;
using System.Windows.Ink;

namespace EVGR
{
   public class CounterFPS
   {
      private static int counter;

      private static int Counter
      {
         get { return counter; }
         set { counter = value; }
      }


      public static void Increment()
      {
         Counter++;
      }

      /// <summary>
      /// Объявляем таймер, который будет подсчитывать кол-во ФПС (frames per second) каждую секунду
      /// </summary>
      public CounterFPS(FormMain fm)
      {
         new Timer() {Enabled = true, Interval = 1000}.Tick += delegate
         {
            fm.Text = string.Format("EVGR  FPS: {0}", counter);
            counter = 0;
         };
      }
   }
}
