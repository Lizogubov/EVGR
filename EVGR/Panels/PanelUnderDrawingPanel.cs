﻿using System.Drawing;
using System.Windows.Forms;

namespace EVGR
{
   public partial class PanelUnderDrawingPanel : Panel
   {
      public PanelUnderDrawingPanel()
      {
         InitializeComponent();
      }

      protected override Point ScrollToControl(Control activeControl)
      {
         return DisplayRectangle.Location;
      }
   }
}
