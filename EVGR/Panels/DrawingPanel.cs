﻿ using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Panel = System.Windows.Forms.Panel;

namespace EVGR
{
   public partial class DrawingPanel :  Panel
   {
      private float scaling = 1.0f;       // текущее значение масштабирования
      private float scalingKoef = 1.2f;   // текущий коэффициент масштабирования
      private FormMain formMain = null;   // ссылка на главную форму
      private Figures figures = null;     // ссылка на объект фигур
      private static PointF elocation;    // координаты курсора 

      public DrawingPanel()
      {
         InitializeComponent();
         this.SetStyle(
            ControlStyles.Selectable | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint |
            ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true);

         if (!IsDesignMode())
         {
            // Включение перерисовки
            Application.Idle += delegate { Invalidate(); };
            // Отрисовка всех объектов, содержащихся на панели
            this.Paint += PanelPaint;
         }
         
         this.MouseEnter += PanelMouseEnter;
         this.MouseLeave += PanelMouseLeave;
         this.MouseDown += PanelMouseDown;
         this.MouseMove += PanelMouseMove;
         this.MouseUp += PanelMouseUp;
         this.KeyDown += PanelKeyDown;
         this.ClientSizeChanged += PanelSizeChanged;
      }

      /// <summary>
      /// Координаты точки мыши, меняющиеся в соответсвии с масштабированием
      /// </summary>
      public static PointF Elocation
      {
         get { return elocation; }
         set { elocation = value; }
      }

      /// <summary>
      /// Получение ссылки на главную форму
      /// </summary>
      public void GetReferenceOnFormMain(FormMain fm)
      {
         formMain = fm;
         figures = formMain.Figures;
         SendThisSizeInfoOnFormMain();
      } 

      /// <summary>
      /// Установка размеров панели для рисования определенными значениями ширины и высоты 
      /// </summary>
      public void SetDefSize(uint width, uint height)
      {
         this.Height = (int)(height * scaling);
         this.Width = (int)(width * scaling);
      }


      private float innacuracyValue = 0.5f; // погрещность образующаяся в следствие приведения по int

      /// <summary>
      /// Увеличение масштаба
      /// </summary>
      public void ZoomIn()
      {
         float maxScaling = 6;
         if (scaling <= maxScaling)
         {
            scaling *= scalingKoef;
            this.Width = (int)(this.Width * scalingKoef + innacuracyValue);
            this.Height = (int)(this.Height * scalingKoef + innacuracyValue);
         }
      }
      
      /// <summary>
      /// Уменьшение масштаба
      /// </summary>
      public void ZoomOut()
      {
         float minScaling = 0.1f;
         if (scaling >= minScaling)
         {
            scaling /= scalingKoef;
            this.Width = (int)(this.Width / scalingKoef + innacuracyValue);
            this.Height = (int)(this.Height / scalingKoef + innacuracyValue);
         }
      }

      /// <summary>
      /// Метод, отвечающий за получение фокуса панелью при нажатии по ней
      /// </summary>
      protected override void OnMouseDown(MouseEventArgs e)
      {
         if (!this.Focused)
            this.Focus();
         base.OnMouseDown(e);
      }
        
      /// <summary>
      /// Метод, с помощью которого можно передавать события о нажатии клавиш стрелок
      /// </summary>
      protected override bool IsInputKey(Keys keyData)
      {
         if (keyData == Keys.Up || keyData == Keys.Down) return true;
         if (keyData == Keys.Left || keyData == Keys.Right) return true;
         return base.IsInputKey(keyData);
      }
      
      /// <summary>
      /// Метод проверяющий, работает программа, или же мы находимся в дизайнере
      /// </summary>
      private bool IsDesignMode()
      {
         if (Site != null)
            return Site.DesignMode;

         StackTrace stackTrace = new StackTrace();
         int frameCount = stackTrace.FrameCount - 1;

         for (int frame = 0; frame < frameCount; frame++)
         {
            Type type = stackTrace.GetFrame(frame).GetMethod().DeclaringType;
            if (typeof (IDesignerHost).IsAssignableFrom(type))
               return true;
         }
         return false;
      }

      /// <summary>
      /// При входе мышью в окно рисования присвоить перекрестный курсор
      /// </summary>
      private void PanelMouseEnter(object sender, EventArgs e)
      {
         this.Cursor = Cursors.Cross;
      }

      /// <summary>
      /// Происходит при изменении размера панели
      /// </summary>
      private void PanelSizeChanged(object sender, EventArgs e)
      {
         SendThisSizeInfoOnFormMain();
      }

      /// <summary>
      /// При выходе из окна рисования присвоить курсору стандартное изображение
      /// </summary>
      private void PanelMouseLeave(object sender, EventArgs e)
      {
         Cursor = Cursors.Default;
      }

      /// <summary>
      /// Метод для улавливания нажатий клавиш от пользователя 
      /// </summary>
      private void PanelKeyDown(object sender, KeyEventArgs e)
      {
        if(e.KeyCode == Keys.Left)
           figures.MoveFiguresByArrows(-1,0);
        else if(e.KeyCode == Keys.Right)
           figures.MoveFiguresByArrows(1, 0);
        else if(e.KeyCode == Keys.Up)
           figures.MoveFiguresByArrows(0, -1);
        else if(e.KeyCode == Keys.Down)
           figures.MoveFiguresByArrows(0, 1);
      }
      
      /// <summary>
      /// Перерисовка всех объектов в окне рисования по таймеру
      /// </summary>
      private void PanelPaint(object sender, PaintEventArgs e)
      {
         CounterFPS.Increment(); 
         e.Graphics.ScaleTransform(scaling, scaling, MatrixOrder.Prepend);  
         SetAntialiasing(e);
         figures.Paint(e);
      }

      

      /// <summary>
      /// Метод,срабатывающий при нажатии мышью по окну рисования
      /// </summary>
      private void PanelMouseDown(object sender, MouseEventArgs e)
      {
         try
         {
            Elocation = new PointF(e.Location.X / scaling, e.Location.Y / scaling);
            if (Cursor == Cursors.SizeNWSE || Cursor == Cursors.SizeWE || Cursor == Cursors.SizeNS)
            {
               SwitchConditions.PanelDrawingResizing = true;
            }
            else if (e.Button == MouseButtons.Left)
            {
               if (SwitchConditions.ChangingFigure || SwitchConditions.Moving)
                  figures.SaveParamsToChangeMove(elocation);
               else if (SwitchConditions.IsDrawingNow())
                  figures.StartDrawingFigures(elocation);
            }
            else if (SwitchConditions.Selecting)
            {
               if (e.Button == MouseButtons.Right)
                  if (figures.SelectFigure(elocation))
                     SwitchConditions.SomeFigureSelected=true;
            }
         }
         catch (Exception ex)
         {
           MessageBox.Show(ex.Message);
         }
      }

      /// <summary>
      /// Метод,срабатывающий при передвижении мыши в окне рисования
      /// </summary>
      private void PanelMouseMove(object sender, MouseEventArgs e)
      {
         Elocation = new PointF(e.Location.X/scaling, e.Location.Y/scaling);
         // отслеживание координат
         formMain.GetLabelCoordinates.Text = string.Format("{0},{1}", Math.Round(elocation.X), Math.Round(elocation.Y));

         //выбор точек при зажатой левой клавиши мыши для изменения фигур/изменения размера панели рисования/рисования фигур
         if (e.Button == System.Windows.Forms.MouseButtons.Left)
         {
            if (SwitchConditions.ChangingFigure || SwitchConditions.Moving )
            {
               figures.MovingChangingFigures(elocation);
            }
            else if (SwitchConditions.PanelDrawingResizing)
            {
               ResizeDrawingPanel(e);
            }
            else
            {
               figures.DrawingFigures(elocation);
            }
         }
         else
         {
            ChangeCursorWhileMoving(e);
            SwitchConditions.ChangeMoveCondition("");
            if (SwitchConditions.SomeFigureSelected)
            {
               figures.DefineParamToChange(elocation);
               SetCursorInAccordanceWithDefinedParam();
            }
         }
      }

      
      /// <summary>
      /// Метод,срабатывающий при отпускании мыши в окне рисования
      /// </summary>
      private void PanelMouseUp(object sender, MouseEventArgs e)
      {
         try
         {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
               PointF elocation = new PointF(e.Location.X / scaling, e.Location.Y / scaling);
               if (SwitchConditions.ChangingFigure || SwitchConditions.Moving)
               {
                  figures.EndMovingChangingFigures();
                  this.Cursor = Cursors.Cross;
               }
               else if (SwitchConditions.PanelDrawingResizing)
               {
                  SwitchConditions.PanelDrawingResizing = false;
               }
               else if (SwitchConditions.IsDrawingNow())
                  figures.EndDrawingFigures(elocation);
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
         
      }
        
      /// <summary>
      /// Метод, отвечающий за изменение вида курсора при его перемещении
      /// </summary>
      private void ChangeCursorWhileMoving(MouseEventArgs e)
      {
         if (CursorInRightSide(e))
            Cursor = Cursors.SizeWE;
         else if (CursorInBottomSide(e))
            Cursor = Cursors.SizeNS;
         else if (CursorInBottomRightCorner(e))
            Cursor = Cursors.SizeNWSE;
         else
            Cursor = Cursors.Cross;
      }

      /// <summary>
      /// Меняем размер панели
      /// </summary>
      private void ResizeDrawingPanel(MouseEventArgs e)
      {
         if (Cursor == Cursors.SizeNWSE)
         {
            this.Width = e.X;
            this.Height = e.Y;
         }
         else if (Cursor == Cursors.SizeNS)
         {
            this.Height = e.Y;
         }
         else if (Cursor == Cursors.SizeWE)
         {
            this.Width = e.X;
         }
      }

      /// <summary>
      /// Передаем значение о размерах панели в главную форму
      /// </summary>
      private void SendThisSizeInfoOnFormMain()
      {
         if (formMain != null)
            formMain.GetLabelSize.Text = this.Width + " x " + this.Height;
      }

      private bool CursorInBottomRightCorner(MouseEventArgs e)
      {
         return this.Width - 10 < e.X && this.Height - 10 < e.Y;
      }

      private bool CursorInBottomSide(MouseEventArgs e)
      {
         return this.Height + 15 > e.Location.Y &&
                e.Location.Y > this.Height - 5 &&
                e.Location.X < this.Width - 15;
      }

      private bool CursorInRightSide(MouseEventArgs e)
      {
         return this.Width + 15 > e.Location.X &&
                e.Location.X > this.Width - 5 &&
                e.Location.Y < this.Height - 15;
      }

      private void SetCursorInAccordanceWithDefinedParam()
      {
         if (SwitchConditions.Moving)
            this.Cursor = Cursors.SizeAll;
         else if (SwitchConditions.ChangingFigure)
            this.Cursor = Cursors.Hand;
      }

      /// <summary>
      /// Задаем режим сглаживания при отображении
      /// </summary>
      private static void SetAntialiasing(PaintEventArgs e)
      {
         if (SwitchConditions.Antialiasing)
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
         else e.Graphics.SmoothingMode = SmoothingMode.None;
      }


   }
}


