﻿using System;
using System.Drawing;
using System.Windows.Forms;
using EVGR.Properties;


namespace EVGR
{
   public class PanelProperties : UserControl
   {
      private Figures editedFigure;
      private FormMain formMain;
      // для контура и цвета фигур
      private int index = 0;
      private Button btColorContour;
      private Button btColorPen;
      private Button btFigureName;
      private Panel pnFigureProps;
      private Label lbName;
      private TextBox tbName;
      private Label lbPointStart;
      private Label lbPointStartX;
      private TextBox tbPointStartX;
      private Label lbPointStartY;
      private TextBox tbPointStartY;
      private Label lbPointEnd;
      private Label lbPointEndX;
      private TextBox tbPointEndX;
      private Label lbPointEndY;
      private TextBox tbPointEndY;
      private Label lbPointReference;
      private Label lbPointReferenceX;
      private TextBox tbPointReferenceX;
      private Label lbPointReferenceY;
      private TextBox tbPointReferenceY;
      private Label lbPenThickness;
      private NumericUpDown numPenThickness;
      private Label lbAngle;
      private NumericUpDown numAngle;
      private Label lbColorPen;
      private Label lbStartAngle;
      private NumericUpDown numStartAngle;
      private Label lbSweepAngle;
      private NumericUpDown numSweepAngle;
      private Label lbColorContour;
      private CheckBox cbFilling;
      private CheckBox cbContoured;
      private Button btSaveToJournal;
      private ComboBox cbForpolygonPoints;
      private Label lbPointPolygonX;
      private Label lbPointPolygonY;
      private TextBox tbPointPolygonX;
      private TextBox tbPointPolygonY;
      private ToolTip ttPanelProps;
      private Button btDelete;
      private Panel pnForButtons;

      public Button BtColorContour
      {
         get { return btColorContour; }
         set { btColorContour = value; }
      }

      public Button BtColorPen
      {
         get { return btColorPen; }
         set { btColorPen = value; }
      }

      public Figures EditedFigure
       {
           get { return editedFigure; }
           set { editedFigure = value; }
       }

      public int Index
       {
           get { return index; }
           set { index = value; }
       }

      #region  вспомогательные методы (для подсказок и включения/выключения параметров)

      //открытие/ скрытие панели св-в фигур
      public void BtFigureNameClick(object sender, EventArgs e)
      {
         if (pnFigureProps.Visible == false)
         {
            numPenThickness.Value = (decimal) editedFigure.PenThickness;

            if (editedFigure is MyArc)
            {
               ((MyArc) editedFigure).StartAngle += editedFigure.Angle;
               numStartAngle.Value = (decimal) ((MyArc) editedFigure).StartAngle;
            }
            else if (editedFigure is MyPie)
            {
               ((MyPie) editedFigure).StartAngle += editedFigure.Angle;
               numStartAngle.Value = (decimal) ((MyPie) editedFigure).StartAngle;
            }
            //приведение параметров после прошлых изменений
            if (!(editedFigure is MyPolygon))
            {
               tbPointEndX.Text = editedFigure.End.X.ToString("0.0");
               tbPointEndY.Text = editedFigure.End.Y.ToString("0.0");
               tbPointStartX.Text = editedFigure.Start.X.ToString("0.0");
               tbPointStartY.Text = editedFigure.Start.Y.ToString("0.0");
            }
            if (!(editedFigure is MyEllipse))
            {
               tbPointReferenceX.Text = editedFigure.Reference.X.ToString("0.0");
               tbPointReferenceY.Text = editedFigure.Reference.Y.ToString("0.0");
            }

            editedFigure.Angle = 0;
            editedFigure.PrevAngle = 0;
            if (!(editedFigure is MyEllipse))
               numAngle.Value = 0;

            formMain.PanelProps = this;
            pnFigureProps.Visible = true;

            ttPanelProps.ToolTipIcon = ToolTipIcon.Warning;
            ttPanelProps.ToolTipTitle = "Подсказка";
            ttPanelProps.IsBalloon = true;
            if (!(editedFigure is MyEllipse))
            {
               ttPanelProps.SetToolTip(tbPointReferenceX,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
               ttPanelProps.SetToolTip(tbPointReferenceY,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
            }
            if (!(editedFigure is MyPolygon))
            {
               ttPanelProps.SetToolTip(tbPointEndX,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
               ttPanelProps.SetToolTip(tbPointEndY,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
               ttPanelProps.SetToolTip(tbPointStartX,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
               ttPanelProps.SetToolTip(tbPointStartY,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
            }
            if (editedFigure is MyPolygon)
            {
               ttPanelProps.SetToolTip(tbPointPolygonX,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
               ttPanelProps.SetToolTip(tbPointPolygonY,
                  "Поле не должно быть пустым, разрешается использовать цифровые символы и \",\" для нецелых чисел!");
            }
         }
         else
         {
            pnFigureProps.Visible = false;
         }
      }

      //метод, выделяющий фигуру, в зависимости от того, открыты её св-ва или нет
      private void PnFigurePropsVisibleChanged(object sender, EventArgs e)
      {
         if (pnFigureProps.Visible)
         {
            formMain.Figures.UnselectAllItems();
            editedFigure.IsSelected = true;
         }
         else
         {
            editedFigure.IsSelected = false;
         }
      }

      private void PropertiesOn()
      {
         numPenThickness.Enabled = true;
         if (!(editedFigure is MyPolygon || editedFigure is MyEllipse))
         {
            tbPointStartX.Enabled = true;
            tbPointStartY.Enabled = true;
            tbPointReferenceX.Enabled = true;
            tbPointReferenceY.Enabled = true;
            tbPointEndX.Enabled = true;
            tbPointEndY.Enabled = true;
            numAngle.Enabled = true;
            if (editedFigure is MyPie || editedFigure is MyArc)
            {
               numStartAngle.Enabled = true;
               numSweepAngle.Enabled = true;
            }
         }
         else if (editedFigure is MyEllipse)
         {
            tbPointStartX.Enabled = true;
            tbPointStartY.Enabled = true;
            tbPointEndX.Enabled = true;
            tbPointEndY.Enabled = true;
         }
         else if (editedFigure is MyPolygon)
         {
            tbPointPolygonX.Enabled = true;
            tbPointPolygonY.Enabled = true;
            tbPointReferenceX.Enabled = true;
            tbPointReferenceY.Enabled = true;
            numAngle.Enabled = true;
         }
      }

      private void PropertiesOff()
      {
         numPenThickness.Enabled = false;
         if (!(editedFigure is MyPolygon || editedFigure is MyEllipse))
         {
            tbPointStartX.Enabled = false;
            tbPointStartY.Enabled = false;
            tbPointReferenceX.Enabled = false;
            tbPointReferenceY.Enabled = false;
            tbPointEndX.Enabled = false;
            tbPointEndY.Enabled = false;
            numAngle.Enabled = false;
            if (editedFigure is MyPie || editedFigure is MyArc)
            {
               numStartAngle.Enabled = false;
               numSweepAngle.Enabled = false;
            }
         }
         else if (editedFigure is MyEllipse)
         {
            tbPointStartX.Enabled = false;
            tbPointStartY.Enabled = false;
            tbPointEndX.Enabled = false;
            tbPointEndY.Enabled = false;
         }
         else if (editedFigure is MyPolygon)
         {
            tbPointPolygonX.Enabled = false;
            tbPointPolygonY.Enabled = false;
            tbPointReferenceX.Enabled = false;
            tbPointReferenceY.Enabled = false;
            numAngle.Enabled = false;
         }
      }

      /// <summary>
      /// В данном примере так и в предыдущем реализована проверка кода вводимого символа с использованием метода «Char.IsDigit», 
      /// но присутствует дополнительное условие, разрешающее ввод одного десятичного разделителя. Для этого используется метод «Text.IndexOf».
      /// Данный метод выполняет поиск знака точки, по словам используя текущий язык и региональные параметры. Поиск начинается с первой 
      /// позиции знака в данном экземпляре (текущей строке) и продолжается до последней позиции знака. Если данный символ не был найден,
      /// то метод возвращает значение «-1». В случае если символ был найден то метод возвращает целое десятичное число, указывающее в
      /// какой позиции находится данный символ и запрещает обработку ввода символа. 
      /// </summary>
      private void TbPointKeyPress(object sender, KeyPressEventArgs e)
      {
         // if (e.KeyChar == 22)
         if (!(Char.IsDigit(e.KeyChar)) && !((e.KeyChar == ',') &&
            (((TextBox) sender).Text.IndexOf(",") == -1) &&
            (((TextBox) sender).Text.Length != 0)) && e.KeyChar != (char) Keys.Back)
         {
            {
               e.Handled = true;
            }
         }

         TextBox temp = sender as TextBox;
         if (temp.Text.Length > 0)
            if (System.Convert.ToDouble(temp.Text) > 9999.999)
               temp.Text = "9999,999";
      }

      #endregion

      #region методы для изменения значений свойств фигур

      private void BtDeleteClick(object sender, EventArgs e)
      {
         DialogResult dr = MessageBox.Show("Вы уверены, что хотите удалить этот элемент?",
            "Удаление эелемента", MessageBoxButtons.YesNo);
         if (dr == DialogResult.Yes)
         {
            pnFigureProps.Dispose();
            btFigureName.Dispose();
            formMain.Figures.CurrentObj = editedFigure;
         }
      }

      private void TbNameTextChanged(object sender, EventArgs e)
      {
         if (!(editedFigure is MyPolygon))
            btFigureName.Text = tbName.Text + " " + editedFigure.Start;
         else
            btFigureName.Text = tbName.Text + " " + ((MyPolygon) editedFigure).Points[0];
         editedFigure.Name = tbName.Text;
      }

      private void NumStartAngleValueChanged(object sender, EventArgs e)
      {
         if (editedFigure is MyPie)
         {
            ((MyPie) editedFigure).PrevStartAngle = ((MyPie) editedFigure).StartAngle;
            ((MyPie) editedFigure).StartAngle = (float) numStartAngle.Value;
            ((MyPie) editedFigure).ModifyPointsPieArcWithStartAngle();
         }
         else if (editedFigure is MyArc)
         {
            ((MyArc) editedFigure).PrevStartAngle = ((MyArc) editedFigure).StartAngle;
            ((MyArc) editedFigure).StartAngle = (float) numStartAngle.Value;
            ((MyArc) editedFigure).ModifyPointsPieArcWithStartAngle();
         }
      }

      private void NumSweepAngleValueChanged(object sender, EventArgs e)
      {
         if (editedFigure is MyPie)
         {
            ((MyPie) editedFigure).PrevSweepAngle = ((MyPie) editedFigure).SweepAngle;
            ((MyPie) editedFigure).SweepAngle = (float) numSweepAngle.Value;
            ((MyPie) editedFigure).ModifyPointsPieArcWithSweepAngle();
         }
         else if (editedFigure is MyArc)
         {
            ((MyArc) editedFigure).PrevSweepAngle = ((MyArc) editedFigure).SweepAngle;
            ((MyArc) editedFigure).SweepAngle = (float) numSweepAngle.Value;
            ((MyArc) editedFigure).ModifyPointsPieArcWithSweepAngle();
         }
      }

      private void NumAngleValueChanged(object sender, EventArgs e)
      {
         editedFigure.Angle = (float) numAngle.Value;
         if (pnFigureProps.Visible)
         {
            if (editedFigure is MyRectangle)
            {
               ((MyRectangle) editedFigure).RotateRectangle();
            }
            else if (editedFigure is MyArc)
            {
               ((MyArc) editedFigure).ModifyPointsPieArcWithAngle();
            }
            else if (editedFigure is MyPie)
            {
               ((MyPie) editedFigure).ModifyPointsPieArcWithAngle();
            }
            else if (editedFigure is MyPolygon)
            {
               ((MyPolygon) editedFigure).RotatePolygon();
            }
            else if (editedFigure is MyLines)
            {
               editedFigure.End = new PointF(Single.Parse(tbPointEndX.Text),
                  Single.Parse(tbPointEndY.Text));
               editedFigure.Start = new PointF(Single.Parse(tbPointStartX.Text),
                  Single.Parse(tbPointStartY.Text));
               ((MyLines) editedFigure).End = ((MyLines) editedFigure).ModifyPointsLine(((MyLines) editedFigure).End);
               ((MyLines) editedFigure).Start =
                  ((MyLines) editedFigure).ModifyPointsLine(((MyLines) editedFigure).Start);
               ((MyLines) editedFigure).ConvertPointsOfLineWithAngle();
               editedFigure.Reference = new PointF(Single.Parse(tbPointReferenceX.Text),
                  Single.Parse(tbPointReferenceY.Text));
            }
         }
      }

      private void NumPenThicknessValueChanged(object sender, EventArgs e)
      {
         editedFigure.PenThickness = (float) numPenThickness.Value;
         if (editedFigure is MyLines)
         {
            ((MyLines) editedFigure).ConvertPointsOfLineWhileDrawing();
         }
      }

      private void TbPointEndXTextChanged(object sender, EventArgs e)
      {
         if (pnFigureProps.Visible)
         {
            if (tbPointEndX.Text != String.Empty && tbPointEndX.Text != ",")
            {
               PropertiesOn();
               editedFigure.End = new PointF(Single.Parse(tbPointEndX.Text),
                  Single.Parse(tbPointEndY.Text));
               if (editedFigure is MyRectangle)
               {
                  numAngle.Value = 0;
                  ((MyRectangle) editedFigure).BuildRectangle();
               }
               else if (editedFigure is MyArc)
               {
                  numAngle.Value = 0;
                  ((MyArc) editedFigure).StartAngle = ((MyArc) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyArc) editedFigure).StartAngle;
               }
               else if (editedFigure is MyPie)
               {
                  numAngle.Value = 0;
                  ((MyPie) editedFigure).StartAngle = ((MyPie) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyPie) editedFigure).StartAngle;
               }
               else if (editedFigure is MyLines)
               {
                  numAngle.Value = 0;
                  ((MyLines) editedFigure).ConvertPointsOfLineWhileDrawing();
               }
            }
            else
            {
               PropertiesOff();
               tbPointEndX.Enabled = true;
               tbPointEndX.Focus();
            }
         }
      }

      private void TbPointEndYTextChanged(object sender, EventArgs e)
      {
         if (pnFigureProps.Visible)
         {
            if (tbPointEndY.Text != String.Empty && tbPointEndY.Text != ",")
            { 
               PropertiesOn();
               editedFigure.End = new PointF(Single.Parse(tbPointEndX.Text),
                  Single.Parse(tbPointEndY.Text));
               if (editedFigure is MyRectangle)
               {
                  numAngle.Value = 0;
                  ((MyRectangle) editedFigure).BuildRectangle();
               }
               else if (editedFigure is MyArc)
               {
                  numAngle.Value = 0;
                  ((MyArc) editedFigure).StartAngle = ((MyArc) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyArc) editedFigure).StartAngle;
               }
               else if (editedFigure is MyPie)
               {
                  numAngle.Value = 0;
                  ((MyPie) editedFigure).StartAngle = ((MyPie) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyPie) editedFigure).StartAngle;
               }
               else if (editedFigure is MyLines)
               {
                  numAngle.Value = 0;
                  ((MyLines) editedFigure).ConvertPointsOfLineWhileDrawing();
               }
            }
            else
            {
               PropertiesOff();
               tbPointEndY.Enabled = true;
               tbPointEndY.Focus();
            }
         }
      }

      private void TbPointStartXTextChanged(object sender, EventArgs e)
      {
         if (pnFigureProps.Visible)
         {
            if (tbPointStartX.Text != String.Empty && tbPointStartX.Text != ",")
            {
               PropertiesOn();
               editedFigure.Start = new PointF(Single.Parse(tbPointStartX.Text),
                  Single.Parse(tbPointStartY.Text));
               if (editedFigure is MyRectangle)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyRectangle) editedFigure).BuildRectangle();
               }
               else if (editedFigure is MyArc)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyArc) editedFigure).StartAngle = ((MyArc) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyArc) editedFigure).StartAngle;
               }
               else if (editedFigure is MyPie)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyPie) editedFigure).StartAngle = ((MyPie) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyPie) editedFigure).StartAngle;
               }
               else if (editedFigure is MyLines)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyLines) editedFigure).ConvertPointsOfLineWhileDrawing();
               }
            }
            else
            {
               PropertiesOff();
               tbPointStartX.Enabled = true;
               tbPointStartX.Focus();
            }
            //имя элемента на кнопке
            btFigureName.Text = editedFigure.Name + editedFigure.Start;
         }
      }

      private void TbPointStartYTextChanged(object sender, EventArgs e)
      {
         if (pnFigureProps.Visible)
         {
            if (tbPointStartY.Text != String.Empty && tbPointStartY.Text != ",")
            {
               PropertiesOn();
               editedFigure.Start = new PointF(Single.Parse(tbPointStartX.Text),
                  Single.Parse(tbPointStartY.Text));
               if (editedFigure is MyRectangle)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyRectangle) editedFigure).BuildRectangle();
               }
               else if (editedFigure is MyArc)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyArc) editedFigure).StartAngle = ((MyArc) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyArc) editedFigure).StartAngle;
               }
               else if (editedFigure is MyPie)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyPie) editedFigure).StartAngle = ((MyPie) editedFigure).GetStartAngle();
                  numStartAngle.Value = (decimal) ((MyPie) editedFigure).StartAngle;
               }
               else if (editedFigure is MyLines)
               {
                  numAngle.Value = 0;
                  editedFigure.Reference = editedFigure.Start;
                  tbPointReferenceX.Text = editedFigure.Start.X.ToString();
                  tbPointReferenceY.Text = editedFigure.Start.Y.ToString();
                  ((MyLines) editedFigure).ConvertPointsOfLineWhileDrawing();
               }
            }
            else
            {
               PropertiesOff();
               tbPointStartY.Enabled = true;
               tbPointStartY.Focus();
            }
            btFigureName.Text = editedFigure.Name + editedFigure.Start;
         }
      }

      private void TbPointReferenceXTextChanged(object sender, EventArgs e)
      {
         if (tbPointReferenceX.Text != String.Empty && tbPointReferenceX.Text != ",")
         {
            PropertiesOn();
            if (editedFigure is MyPolygon)
            {
               numAngle.Value = 0;
               ((MyPolygon) editedFigure).RotatePolygon();
            }
            else if (editedFigure is MyArc)
            {
               numAngle.Value = 0;
            }
            else if (editedFigure is MyPie)
            {
               numAngle.Value = 0;
            }
            else if (editedFigure is MyRectangle)
            {
               numAngle.Value = 0;
            }
            else if (editedFigure is MyLines)
            {
               numAngle.Value = 0;
            }
            if (pnFigureProps.Visible)
               editedFigure.Reference = new PointF(Single.Parse(tbPointReferenceX.Text),
                  Single.Parse(tbPointReferenceY.Text));
         }
         else
         {
            PropertiesOff();
            tbPointReferenceX.Enabled = true;
            tbPointReferenceX.Focus();
         }
      }

      private void TbPointReferenceYTextChanged(object sender, EventArgs e)
      {
         if (tbPointReferenceY.Text != String.Empty && tbPointReferenceY.Text != ",")
         {
            PropertiesOn();

            if (editedFigure is MyPolygon)
            {
               numAngle.Value = 0;
               ((MyPolygon) editedFigure).RotatePolygon();
            }
            else if (editedFigure is MyArc)
            {
               numAngle.Value = 0;
            }
            else if (editedFigure is MyPie)
            {
               numAngle.Value = 0;
            }
            else if (editedFigure is MyRectangle)
            {
               numAngle.Value = 0;
            }
            else if (editedFigure is MyLines)
            {
               numAngle.Value = 0;
            }
            if (pnFigureProps.Visible)
               editedFigure.Reference = new PointF(Single.Parse(tbPointReferenceX.Text),
                  Single.Parse(tbPointReferenceY.Text));
         }

         else
         {
            PropertiesOff();
            tbPointReferenceY.Enabled = true;
            tbPointReferenceY.Focus();
         }
      }

      private void CbForpolygonPointsSelectedIndexChanged(object sender, EventArgs e)
      {
         tbPointPolygonX.TextChanged -= TbPointPolygonXTextChanged;
         tbPointPolygonY.TextChanged -= TbPointPolygonYTextChanged;
         tbPointPolygonX.Text =
            ((MyPolygon) editedFigure).Points[cbForpolygonPoints.SelectedIndex].X.ToString("0.0");
         tbPointPolygonY.Text =
            ((MyPolygon) editedFigure).Points[cbForpolygonPoints.SelectedIndex].Y.ToString("0.0");
         tbPointPolygonX.TextChanged += TbPointPolygonXTextChanged;
         tbPointPolygonY.TextChanged += TbPointPolygonYTextChanged;
      }

      private void TbPointPolygonXTextChanged(object sender, EventArgs e)
      {
         if (tbPointPolygonX.Text != String.Empty && tbPointPolygonX.Text != ",")
         {
            PropertiesOn();
            numAngle.Value = 0;
            ((MyPolygon) editedFigure).Points[cbForpolygonPoints.SelectedIndex].X =
               Single.Parse(tbPointPolygonX.Text);
            //имя элемента на кнопке
            btFigureName.Text = tbName.Text + " " + ((MyPolygon) editedFigure).Points[0];
         }
         else
         {
            PropertiesOff();
            tbPointPolygonX.Enabled = true;
            tbPointPolygonX.Focus();
         }
      }

      private void TbPointPolygonYTextChanged(object sender, EventArgs e)
      {
         if (tbPointPolygonY.Text != String.Empty && tbPointPolygonY.Text != ",")
         {
            PropertiesOn();
            numAngle.Value = 0;
            ((MyPolygon) editedFigure).Points[cbForpolygonPoints.SelectedIndex].Y =
               Single.Parse(tbPointPolygonY.Text);
            //имя элемента на кнопке
            btFigureName.Text = tbName.Text + " " + ((MyPolygon) editedFigure).Points[0];
         }
         else
         {
            PropertiesOff();
            tbPointPolygonY.Enabled = true;
            tbPointPolygonY.Focus();
         }
      }

      private void BtColorPenMouseUp(object sender, MouseEventArgs e)
      {
         if (e.Button == System.Windows.Forms.MouseButtons.Left)
         {
            index = 1;
            formMain.Panel_Pallette_ChangeVisible();
            formMain.PanelProps = this;
         }
         if (e.Button == System.Windows.Forms.MouseButtons.Right)
         {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = true;
            MyDialog.ShowHelp = true;
            MyDialog.Color = BtColorPen.ForeColor;
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
               BtColorPen.BackColor = MyDialog.Color;
               editedFigure.ColorFilling = BtColorPen.BackColor;
            }
         }
      }

      private void BtColorContourMouseUp(object sender, MouseEventArgs e)
      {
         if (e.Button == System.Windows.Forms.MouseButtons.Left)
         {
            index = 2;
            formMain.Panel_Pallette_ChangeVisible();
            formMain.PanelProps = this;
         }
         if (e.Button == System.Windows.Forms.MouseButtons.Right)
         {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = true;
            MyDialog.ShowHelp = true;
            MyDialog.Color = BtColorPen.ForeColor;
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
               BtColorContour.BackColor = MyDialog.Color;
               editedFigure.ColorContour = BtColorContour.BackColor;
            }
         }
      }

      private void CbFillingCheckedChanged(object sender, EventArgs e)
      {
         if (!((editedFigure is MyLines) || (editedFigure is MyArc)))
         {
            editedFigure.Filled = cbFilling.Checked;
            //фигура не может быть одновременно и закрашенная и с контуром
            if (editedFigure.Filled)
               cbContoured.CheckState = CheckState.Unchecked;
         }
      }

      private void CbContouredCheckedChanged(object sender, EventArgs e)
      {
         if (!((editedFigure is MyLines) || (editedFigure is MyArc)))
         {
            editedFigure.Contoured = cbContoured.Checked;
            //фигура не может быть одновременно и закрашенная и с контуром
            if (editedFigure.Contoured)
               cbFilling.CheckState = CheckState.Unchecked;
         }
      }

      #endregion

      #region конструкторы для фигур

      //  Конструктор для окна свойств линии
      public PanelProperties(FormMain lformMain, MyLines editedLine)
      {
         this.formMain = lformMain;
         editedFigure = editedLine;

         AutoSize = true;
         Dock = DockStyle.Top;
         BackColor = Color.White;
         Cursor = DefaultCursor;


         pnFigureProps = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            Visible = false,
            AutoSize = true
         };
         this.Controls.Add(pnFigureProps);

         pnForButtons = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            AutoSize = true
         };
         this.Controls.Add(pnForButtons);

         btFigureName = new Button
         {
            Size = new Size(303, 40),
            Dock = DockStyle.Top,
            BackColor = Color.MintCream,
            Text = editedLine.Name + "  " + editedLine.Start,
         };
         pnForButtons.Controls.Add(btFigureName);

         btDelete = new Button
         {
            BackgroundImage = Resources.delete,
            Size = new Size(40, 40),
            BackgroundImageLayout = ImageLayout.Stretch,
            Dock = DockStyle.Right,
         };
         pnForButtons.Controls.Add(btDelete);


         //название элемента
         lbName = new Label
         {
            Location = new Point(10, 10),
            Text = "Название элемента"
         };
         pnFigureProps.Controls.Add(lbName);

         tbName = new TextBox
         {
            Location = new Point(115, 5),
            Text = editedLine.Name,
            Size = new Size(155, 20),
         };
         pnFigureProps.Controls.Add(tbName);

         //для точки начала
         lbPointStart = new Label
         {
            Location = new Point(10, 35),
            Text = "Точка начала"
         };
         pnFigureProps.Controls.Add(lbPointStart);
         lbPointStartX = new Label
         {
            Location = new Point(115, 35),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartX);
         tbPointStartX = new TextBox
         {
            Location = new Point(145, 30),
            Text = editedLine.Start.X.ToString("0.0"),
            Size = new Size(40, 20),
         };
         pnFigureProps.Controls.Add(tbPointStartX);
         lbPointStartY = new Label
         {
            Location = new Point(200, 35),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartY);
         tbPointStartY = new TextBox
         {
            Location = new Point(230, 30),
            Text = editedLine.Start.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartY);

         //для точки конца
         lbPointEnd = new Label
         {
            Location = new Point(10, 60),
            Text = "Точка конца",
         };
         pnFigureProps.Controls.Add(lbPointEnd);
         lbPointEndX = new Label
         {
            Location = new Point(115, 60),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndX);
         tbPointEndX = new TextBox
         {
            Location = new Point(145, 55),
            Text = editedLine.End.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndX);
         lbPointEndY = new Label
         {
            Location = new Point(200, 60),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndY);
         tbPointEndY = new TextBox
         {
            Location = new Point(230, 55),
            Text = editedLine.End.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndY);

         //для точки опоры
         lbPointReference = new Label
         {
            Location = new Point(10, 90),
            Text = "Точка опоры",
         };
         pnFigureProps.Controls.Add(lbPointReference);
         lbPointReferenceX = new Label
         {
            Location = new Point(115, 90),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceX);
         tbPointReferenceX = new TextBox
         {
            Location = new Point(145, 85),
            Text = editedLine.Reference.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceX);
         lbPointReferenceY = new Label
         {
            Location = new Point(200, 90),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceY);
         tbPointReferenceY = new TextBox
         {
            Location = new Point(230, 85),
            Text = editedLine.Reference.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceY);

         //для толщины линии
         lbPenThickness = new Label
         {
            Location = new Point(10, 120),
            Text = "Толщина линии",
            Size = new Size(89, 13)
         };
         pnFigureProps.Controls.Add(lbPenThickness);
         numPenThickness = new NumericUpDown
         {
            Location = new Point(115, 115),
            Size = new Size(80, 20),
            Value = new decimal(editedLine.PenThickness),
            Minimum = new decimal(0.5),
            Maximum = new decimal(300.0),
            Increment = new decimal(0.5),
            DecimalPlaces = 1
         };
         pnFigureProps.Controls.Add(numPenThickness);
         //для угла поворота
         lbAngle = new Label
         {
            Location = new Point(10, 150),
            Text = "Угол поворота",
            Size = new Size(89, 13)
         };
         pnFigureProps.Controls.Add(lbAngle);
         numAngle = new NumericUpDown
         {
            Location = new Point(115, 145),
            Size = new Size(80, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedLine.Angle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numAngle);
         //для цвета линии
         lbColorPen = new Label
         {
            Location = new Point(10, 180),
            Size = new Size(68, 13),
            Text = "Цвет линии"
         };
         pnFigureProps.Controls.Add(lbColorPen);

         BtColorContour = new Button
         {
            Location = new Point(115, 175),
            Size = new Size(82, 40),
            BackColor = editedLine.ColorContour,
         };
         pnFigureProps.Controls.Add(BtColorContour);
         //кнопка для сохранения в журнал
         btSaveToJournal = new Button
         {
            Location = new Point(50, 220),
            Size = new Size(200, 40),
            Text = "Сохранить изменения в журнал"
         };
         pnFigureProps.Controls.Add(btSaveToJournal);


         numAngle.ValueChanged += NumAngleValueChanged;
         numPenThickness.ValueChanged += NumPenThicknessValueChanged;
         tbPointEndX.TextChanged += TbPointEndXTextChanged;
         tbPointEndY.TextChanged += TbPointEndYTextChanged;
         BtColorContour.MouseUp += BtColorContourMouseUp;
         tbPointStartX.TextChanged += TbPointStartXTextChanged;
         tbPointStartY.TextChanged += TbPointStartYTextChanged;
         tbPointReferenceX.TextChanged += TbPointReferenceXTextChanged;
         tbPointReferenceY.TextChanged += TbPointReferenceYTextChanged;
         tbName.TextChanged += TbNameTextChanged;
         //для обработки нажатия (запрет на ввод ненужных здесь символов)
         tbPointStartX.KeyPress += TbPointKeyPress;
         tbPointStartY.KeyPress += TbPointKeyPress;
         tbPointEndX.KeyPress += TbPointKeyPress;
         tbPointEndY.KeyPress += TbPointKeyPress;
         tbPointReferenceX.KeyPress += TbPointKeyPress;
         tbPointReferenceY.KeyPress += TbPointKeyPress;
         //для всплывающей подсказки
         ttPanelProps = new ToolTip();

         pnFigureProps.VisibleChanged += PnFigurePropsVisibleChanged;

         //для кнопки удаления 
         btDelete.Click += BtDeleteClick;
         editedLine.ChangeEnd += RefreshEndPoint;
         //открытие/закрытие св-в
         btFigureName.Click += BtFigureNameClick;
      }


      private void RefreshEndPoint()
      {
         tbPointEndX.Text = editedFigure.End.X.ToString();
      }

      //для св-в прямоугольника
      public PanelProperties(FormMain lformMain, MyRectangle editedRectangle)
      {
         this.formMain = lformMain;
         this.editedFigure = editedRectangle;

         this.AutoSize = true;
         this.Dock = DockStyle.Top;
         this.BackColor = Color.White;
         this.Cursor = DefaultCursor;


         pnFigureProps = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            Visible = false,
            AutoSize = true
         };
         this.Controls.Add(pnFigureProps);

         pnForButtons = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            AutoSize = true
         };
         this.Controls.Add(pnForButtons);

         btFigureName = new Button
         {
            Size = new Size(303, 40),
            Dock = DockStyle.Top,
            BackColor = Color.MintCream,
            Text = editedRectangle.Name + "  " + editedRectangle.Start,
         };
         pnForButtons.Controls.Add(btFigureName);

         btDelete = new Button
         {
            BackgroundImage = Resources.delete,
            Size = new Size(40, 40),
            BackgroundImageLayout = ImageLayout.Stretch,
            Dock = DockStyle.Right,
         };
         pnForButtons.Controls.Add(btDelete);

         lbName = new Label
         {
            Location = new Point(10, 10),
            Text = "Название элемента"
         };
         pnFigureProps.Controls.Add(lbName);

         tbName = new TextBox
         {
            Location = new Point(115, 5),
            Text = editedRectangle.Name,
            Size = new Size(155, 20),
         };
         pnFigureProps.Controls.Add(tbName);

         //для точки начала
         lbPointStart = new Label
         {
            Location = new Point(10, 35),
            Text = "Точка начала",
         };
         pnFigureProps.Controls.Add(lbPointStart);

         lbPointStartX = new Label
         {
            Location = new Point(115, 35),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartX);

         tbPointStartX = new TextBox
         {
            Location = new Point(145, 30),
            Text = editedRectangle.Start.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartX);

         lbPointStartY = new Label
         {
            Location = new Point(200, 35),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartY);

         tbPointStartY = new TextBox
         {
            Location = new Point(230, 30),
            Text = editedRectangle.Start.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartY);

         //для точки конца
         lbPointEnd = new Label
         {
            Location = new Point(10, 65),
            Text = "Точка конца",
         };
         pnFigureProps.Controls.Add(lbPointEnd);

         lbPointEndX = new Label
         {
            Location = new Point(115, 65),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndX);

         tbPointEndX = new TextBox
         {
            Location = new Point(145, 60),
            Text = editedRectangle.End.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndX);

         lbPointEndY = new Label
         {
            Location = new Point(200, 65),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndY);

         tbPointEndY = new TextBox
         {
            Location = new Point(230, 60),
            Text = editedRectangle.End.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndY);

         //для точки опоры
         lbPointReference = new Label
         {
            Location = new Point(10, 90),
            Text = "Точка опоры",
         };
         pnFigureProps.Controls.Add(lbPointReference);
         lbPointReferenceX = new Label
         {
            Location = new Point(115, 90),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceX);
         tbPointReferenceX = new TextBox
         {
            Location = new Point(145, 85),
            Text = editedRectangle.Reference.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceX);
         lbPointReferenceY = new Label
         {
            Location = new Point(200, 90),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceY);
         tbPointReferenceY = new TextBox
         {
            Location = new Point(230, 85),
            Text = editedRectangle.Reference.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceY);

         //для толщины контура прямоугольника
         lbPenThickness = new Label
         {
            Location = new Point(10, 120),
            Text = "Толщина контура",
            Size = new Size(100, 13)
         };
         pnFigureProps.Controls.Add(lbPenThickness);

         numPenThickness = new NumericUpDown
         {
            Location = new Point(115, 115),
            Size = new Size(80, 20),
            Value = new decimal(editedRectangle.PenThickness),
            Minimum = new decimal(0.5),
            Maximum = new decimal(300.0),
            Increment = new decimal(0.5),
            DecimalPlaces = 1
         };
         pnFigureProps.Controls.Add(numPenThickness);

         //для угла поворота
         lbAngle = new Label
         {
            Location = new Point(10, 155),
            Text = "Угол поворота",
            Size = new Size(89, 13)
         };
         pnFigureProps.Controls.Add(lbAngle);
         numAngle = new NumericUpDown
         {
            Location = new Point(115, 150),
            Size = new Size(80, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedRectangle.Angle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numAngle);

         //цвет прямоугольника
         lbColorPen = new Label
         {
            Location = new Point(10, 190),
            Size = new Size(98, 13),
            Text = "Цвет прямоугольника"
         };
         pnFigureProps.Controls.Add(lbColorPen);

         BtColorPen = new Button
         {
            Location = new Point(115, 185),
            Size = new Size(82, 40),
            BackColor = editedRectangle.ColorFilling,
         };
         pnFigureProps.Controls.Add(BtColorPen);

         //цвет контура
         lbColorContour = new Label
         {
            Location = new Point(10, 240),
            Size = new Size(98, 13),
            Text = "Цвет контура"
         };
         pnFigureProps.Controls.Add(lbColorContour);
         BtColorContour = new Button
         {
            Location = new Point(115, 230),
            Size = new Size(82, 40),
            BackColor = editedRectangle.ColorContour,
         };
         pnFigureProps.Controls.Add(BtColorContour);

         //для чекбоксов
         cbFilling = new CheckBox
         {
            Location = new Point(13, 280),
            Size = new Size(97, 17),
            Text = "Закрашенный",
            Checked = editedRectangle.Filled
         };
         pnFigureProps.Controls.Add(cbFilling);

         cbContoured = new CheckBox
         {
            Location = new Point(120, 280),
            Size = new Size(84, 17),
            Text = "С контуром",
            Checked = editedRectangle.Contoured
         };
         pnFigureProps.Controls.Add(cbContoured);


         //кнопка для сохранения в журнал
         btSaveToJournal = new Button
         {
            Location = new Point(50, 310),
            Size = new Size(200, 40),
            Text = "Сохранить изменения в журнал"
         };
         pnFigureProps.Controls.Add(btSaveToJournal);

         numAngle.ValueChanged += NumAngleValueChanged;
         numPenThickness.ValueChanged += NumPenThicknessValueChanged;
         tbPointEndX.TextChanged += TbPointEndXTextChanged;
         tbPointEndY.TextChanged += TbPointEndYTextChanged;
         BtColorPen.MouseUp += BtColorPenMouseUp;
         cbFilling.CheckedChanged += CbFillingCheckedChanged;
         cbContoured.CheckedChanged += CbContouredCheckedChanged;
         BtColorContour.MouseUp += BtColorContourMouseUp;
         tbPointStartX.TextChanged += TbPointStartXTextChanged;
         tbPointStartY.TextChanged += TbPointStartYTextChanged;
         tbPointReferenceX.TextChanged += TbPointReferenceXTextChanged;
         tbPointReferenceY.TextChanged += TbPointReferenceYTextChanged;
         tbName.TextChanged += TbNameTextChanged;

         tbPointStartX.KeyPress += TbPointKeyPress;
         tbPointStartY.KeyPress += TbPointKeyPress;
         tbPointEndX.KeyPress += TbPointKeyPress;
         tbPointEndY.KeyPress += TbPointKeyPress;
         tbPointReferenceX.KeyPress += TbPointKeyPress;
         tbPointReferenceY.KeyPress += TbPointKeyPress;
         //для кнопки удаления 
         btDelete.Click += BtDeleteClick;
         //для всплывающей подсказки
         ttPanelProps = new ToolTip();
         pnFigureProps.VisibleChanged += PnFigurePropsVisibleChanged;

         btFigureName.Click += BtFigureNameClick;
      }

      //для сектора
      public PanelProperties(FormMain lformMain, MyPie editedPie)
      {
         this.formMain = lformMain;
         this.editedFigure = editedPie;

         this.AutoSize = true;
         this.Dock = DockStyle.Top;
         this.BackColor = Color.White;
         this.Cursor = DefaultCursor;


         pnFigureProps = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            Visible = false,
            AutoSize = true
         };
         this.Controls.Add(pnFigureProps);

         pnForButtons = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            AutoSize = true
         };
         this.Controls.Add(pnForButtons);

         btFigureName = new Button
         {
            Size = new Size(303, 40),
            Dock = DockStyle.Top,
            BackColor = Color.MintCream,
            Text = editedPie.Name + "  " + editedPie.Start,
         };
         pnForButtons.Controls.Add(btFigureName);

         btDelete = new Button
         {
            BackgroundImage = Resources.delete,
            Size = new Size(40, 40),
            BackgroundImageLayout = ImageLayout.Stretch,
            Dock = DockStyle.Right,
         };
         pnForButtons.Controls.Add(btDelete);

         lbName = new Label
         {
            Location = new Point(10, 10),
            Text = "Название элемента"
         };
         pnFigureProps.Controls.Add(lbName);

         tbName = new TextBox
         {
            Location = new Point(115, 5),
            Text = editedPie.Name,
            Size = new Size(155, 20),
         };
         pnFigureProps.Controls.Add(tbName);


         //для точки начала
         lbPointStart = new Label
         {
            Location = new Point(10, 35),
            Text = "Точка начала",
         };
         pnFigureProps.Controls.Add(lbPointStart);

         lbPointStartX = new Label
         {
            Location = new Point(115, 35),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartX);

         tbPointStartX = new TextBox
         {
            Location = new Point(145, 30),
            Text = editedPie.Start.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartX);

         lbPointStartY = new Label
         {
            Location = new Point(200, 35),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartY);

         tbPointStartY = new TextBox
         {
            Location = new Point(230, 30),
            Text = editedPie.Start.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartY);

         //для точки конца
         lbPointEnd = new Label
         {
            Location = new Point(10, 65),
            Text = "Точка конца",
         };
         pnFigureProps.Controls.Add(lbPointEnd);

         lbPointEndX = new Label
         {
            Location = new Point(115, 65),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndX);

         tbPointEndX = new TextBox
         {
            Location = new Point(145, 60),
            Text = editedPie.End.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndX);

         lbPointEndY = new Label
         {
            Location = new Point(200, 65),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndY);

         tbPointEndY = new TextBox
         {
            Location = new Point(230, 60),
            Text = editedPie.End.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndY);

         //для точки опоры
         lbPointReference = new Label
         {
            Location = new Point(10, 90),
            Text = "Точка опоры",
         };
         pnFigureProps.Controls.Add(lbPointReference);
         lbPointReferenceX = new Label
         {
            Location = new Point(115, 90),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceX);
         tbPointReferenceX = new TextBox
         {
            Location = new Point(145, 85),
            Text = editedPie.Reference.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceX);
         lbPointReferenceY = new Label
         {
            Location = new Point(200, 90),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceY);
         tbPointReferenceY = new TextBox
         {
            Location = new Point(230, 85),
            Text = editedPie.Reference.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceY);

         //для толщины контура сектора
         lbPenThickness = new Label
         {
            Location = new Point(10, 120),
            Text = "Толщина контура",
            Size = new Size(100, 13)
         };
         pnFigureProps.Controls.Add(lbPenThickness);

         numPenThickness = new NumericUpDown
         {
            Location = new Point(115, 115),
            Size = new Size(80, 20),
            Value = new decimal(editedPie.PenThickness),
            Minimum = new decimal(0.5),
            Maximum = new decimal(300.0),
            Increment = new decimal(0.5),
            DecimalPlaces = 1
         };
         pnFigureProps.Controls.Add(numPenThickness);

         //для угла поворота
         lbAngle = new Label
         {
            Location = new Point(10, 160),
            Text = "Угол поворота",
            Size = new Size(89, 13)
         };
         pnFigureProps.Controls.Add(lbAngle);
         numAngle = new NumericUpDown
         {
            Location = new Point(115, 155),
            Size = new Size(80, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedPie.Angle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numAngle);
         //цвет сектора
         lbColorPen = new Label
         {
            Location = new Point(10, 190),
            Size = new Size(98, 13),
            Text = "Цвет сектора"
         };
         pnFigureProps.Controls.Add(lbColorPen);

         BtColorPen = new Button
         {
            Location = new Point(115, 185),
            Size = new Size(82, 40),
            BackColor = editedPie.ColorFilling,
         };
         pnFigureProps.Controls.Add(BtColorPen);

         //цвет контура
         lbColorContour = new Label
         {
            Location = new Point(10, 240),
            Size = new Size(98, 13),
            Text = "Цвет контура"
         };
         pnFigureProps.Controls.Add(lbColorContour);
         BtColorContour = new Button
         {
            Location = new Point(115, 240),
            Size = new Size(82, 40),
            BackColor = editedPie.ColorContour,
         };
         pnFigureProps.Controls.Add(BtColorContour);

         //для чекбоксов
         cbFilling = new CheckBox
         {
            Location = new Point(13, 285),
            Size = new Size(97, 17),
            Text = "Закрашенный",
            Checked = editedPie.Filled
         };
         pnFigureProps.Controls.Add(cbFilling);

         cbContoured = new CheckBox
         {
            Location = new Point(120, 285),
            Size = new Size(84, 17),
            Text = "С контуром",
            Checked = editedPie.Contoured
         };
         pnFigureProps.Controls.Add(cbContoured);

         //начальный угол
         lbStartAngle = new Label
         {
            Location = new Point(10, 320),
            Size = new Size(92, 13),
            Text = "Начальный угол"
         };
         pnFigureProps.Controls.Add(lbStartAngle);

         numStartAngle = new NumericUpDown
         {
            Location = new Point(115, 320),
            Size = new Size(85, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedPie.StartAngle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numStartAngle);

         //угол вращения
         lbSweepAngle = new Label
         {
            Location = new Point(10, 350),
            Size = new Size(89, 13),
            Text = "Угол вращения"
         };
         pnFigureProps.Controls.Add(lbSweepAngle);

         numSweepAngle = new NumericUpDown
         {
            Location = new Point(115, 350),
            Size = new Size(85, 20),
            Minimum = -360,
            Maximum = 360,
            Value = (decimal) editedPie.SweepAngle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numSweepAngle);


         //кнопка для сохранения в журнал
         btSaveToJournal = new Button
         {
            Location = new Point(50, 380),
            Size = new Size(200, 40),
            Text = "Сохранить изменения в журнал"
         };
         pnFigureProps.Controls.Add(btSaveToJournal);

         numAngle.ValueChanged += NumAngleValueChanged;
         numPenThickness.ValueChanged += NumPenThicknessValueChanged;
         tbPointEndX.TextChanged += TbPointEndXTextChanged;
         tbPointEndY.TextChanged += TbPointEndYTextChanged;
         BtColorPen.MouseUp += BtColorPenMouseUp;
         tbPointStartX.TextChanged += TbPointStartXTextChanged;
         tbPointStartY.TextChanged += TbPointStartYTextChanged;
         cbFilling.CheckedChanged += CbFillingCheckedChanged;
         cbContoured.CheckedChanged += CbContouredCheckedChanged;
         BtColorContour.MouseUp += BtColorContourMouseUp;
         tbPointReferenceX.TextChanged += TbPointReferenceXTextChanged;
         tbPointReferenceY.TextChanged += TbPointReferenceYTextChanged;
         numStartAngle.ValueChanged += NumStartAngleValueChanged;
         numSweepAngle.ValueChanged += NumSweepAngleValueChanged;
         tbName.TextChanged += TbNameTextChanged;

         tbPointStartX.KeyPress += TbPointKeyPress;
         tbPointStartY.KeyPress += TbPointKeyPress;
         tbPointEndX.KeyPress += TbPointKeyPress;
         tbPointEndY.KeyPress += TbPointKeyPress;
         tbPointReferenceX.KeyPress += TbPointKeyPress;
         tbPointReferenceY.KeyPress += TbPointKeyPress;
         //для кнопки удаления 
         btDelete.Click += BtDeleteClick;
         //для всплывающей подсказки
         ttPanelProps = new ToolTip();
         pnFigureProps.VisibleChanged += PnFigurePropsVisibleChanged;

         btFigureName.Click += BtFigureNameClick;
      }

      //для эллипса
      public PanelProperties(FormMain lformMain, MyEllipse editedEllipse)
      {
         this.editedFigure = editedEllipse;
         this.formMain = lformMain;

         this.AutoSize = true;
         this.Dock = DockStyle.Top;
         this.BackColor = Color.White;
         this.Cursor = DefaultCursor;


         pnFigureProps = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            Visible = false,
            AutoSize = true
         };
         this.Controls.Add(pnFigureProps);

         pnForButtons = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            AutoSize = true
         };
         this.Controls.Add(pnForButtons);

         btFigureName = new Button
         {
            Size = new Size(303, 40),
            Dock = DockStyle.Top,
            BackColor = Color.MintCream,
            Text = editedEllipse.Name + "  " + editedEllipse.Start,
         };
         pnForButtons.Controls.Add(btFigureName);

         btDelete = new Button
         {
            BackgroundImage = Resources.delete,
            Size = new Size(40, 40),
            BackgroundImageLayout = ImageLayout.Stretch,
            Dock = DockStyle.Right,
         };
         pnForButtons.Controls.Add(btDelete);

         lbName = new Label
         {
            Location = new Point(10, 10),
            Text = "Название элемента"
         };
         pnFigureProps.Controls.Add(lbName);

         tbName = new TextBox
         {
            Location = new Point(115, 5),
            Text = editedEllipse.Name,
            Size = new Size(155, 20),
         };
         pnFigureProps.Controls.Add(tbName);

         //для точки начала
         lbPointStart = new Label
         {
            Location = new Point(10, 35),
            Text = "Точка начала",
         };
         pnFigureProps.Controls.Add(lbPointStart);

         lbPointStartX = new Label
         {
            Location = new Point(115, 35),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartX);

         tbPointStartX = new TextBox
         {
            Location = new Point(145, 30),
            Text = editedEllipse.Start.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartX);

         lbPointStartY = new Label
         {
            Location = new Point(200, 35),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartY);

         tbPointStartY = new TextBox
         {
            Location = new Point(230, 30),
            Text = editedEllipse.Start.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartY);

         //для точки конца
         lbPointEnd = new Label
         {
            Location = new Point(10, 65),
            Text = "Точка конца",
         };
         pnFigureProps.Controls.Add(lbPointEnd);

         lbPointEndX = new Label
         {
            Location = new Point(115, 65),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndX);

         tbPointEndX = new TextBox
         {
            Location = new Point(145, 60),
            Text = editedEllipse.End.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndX);

         lbPointEndY = new Label
         {
            Location = new Point(200, 65),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndY);

         tbPointEndY = new TextBox
         {
            Location = new Point(230, 60),
            Text = editedEllipse.End.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndY);

         //для толщины контура окружности
         lbPenThickness = new Label
         {
            Location = new Point(10, 90),
            Text = "Толщина контура",
            Size = new Size(100, 13)
         };
         pnFigureProps.Controls.Add(lbPenThickness);

         numPenThickness = new NumericUpDown
         {
            Location = new Point(115, 85),
            Size = new Size(80, 20),
            Value = new decimal(editedEllipse.PenThickness),
            Minimum = new decimal(0.5),
            Maximum = new decimal(300.0),
            Increment = new decimal(0.5),
            DecimalPlaces = 1
         };
         pnFigureProps.Controls.Add(numPenThickness);


         //цвет сектора
         lbColorPen = new Label
         {
            Location = new Point(10, 125),
            Size = new Size(98, 13),
            Text = "Цвет окружности"
         };
         pnFigureProps.Controls.Add(lbColorPen);

         BtColorPen = new Button
         {
            Location = new Point(115, 115),
            Size = new Size(82, 40),
            BackColor = editedEllipse.ColorFilling,
         };
         pnFigureProps.Controls.Add(BtColorPen);

         //цвет контура
         lbColorContour = new Label
         {
            Location = new Point(10, 175),
            Size = new Size(98, 13),
            Text = "Цвет контура"
         };
         pnFigureProps.Controls.Add(lbColorContour);
         BtColorContour = new Button
         {
            Location = new Point(115, 165),
            Size = new Size(82, 40),
            BackColor = editedEllipse.ColorContour,
         };
         pnFigureProps.Controls.Add(BtColorContour);

         //для чекбоксов
         cbFilling = new CheckBox
         {
            Location = new Point(13, 220),
            Size = new Size(97, 17),
            Text = "Закрашенный",
            Checked = editedEllipse.Filled
         };
         pnFigureProps.Controls.Add(cbFilling);

         cbContoured = new CheckBox
         {
            Location = new Point(120, 220),
            Size = new Size(84, 17),
            Text = "С контуром",
            Checked = editedEllipse.Contoured
         };
         pnFigureProps.Controls.Add(cbContoured);

         //кнопка для сохранения в журнал
         btSaveToJournal = new Button
         {
            Location = new Point(50, 240),
            Size = new Size(200, 40),
            Text = "Сохранить изменения в журнал"
         };
         pnFigureProps.Controls.Add(btSaveToJournal);

         //_numAngle.ValueChanged += NumAngleValueChanged;
         numPenThickness.ValueChanged += NumPenThicknessValueChanged;
         tbPointEndX.TextChanged += TbPointEndXTextChanged;
         tbPointEndY.TextChanged += TbPointEndYTextChanged;
         BtColorPen.MouseUp += BtColorPenMouseUp;
         cbFilling.CheckedChanged += CbFillingCheckedChanged;
         cbContoured.CheckedChanged += CbContouredCheckedChanged;
         BtColorContour.MouseUp += BtColorContourMouseUp;
         tbPointStartX.TextChanged += TbPointStartXTextChanged;
         tbPointStartY.TextChanged += TbPointStartYTextChanged;
         tbName.TextChanged += TbNameTextChanged;

         tbPointStartX.KeyPress += TbPointKeyPress;
         tbPointStartY.KeyPress += TbPointKeyPress;
         tbPointEndX.KeyPress += TbPointKeyPress;
         tbPointEndY.KeyPress += TbPointKeyPress;

         //для всплывающей подсказки
         ttPanelProps = new ToolTip();
         pnFigureProps.VisibleChanged += PnFigurePropsVisibleChanged;
         //this.FormClosing += Window_properties_FormClosing;
         //для кнопки удаления 
         btDelete.Click += BtDeleteClick;

         btFigureName.Click += BtFigureNameClick;
      }

      //для дуги
      public PanelProperties(FormMain lformMain, MyArc editedArc)
      {
         this.formMain = lformMain;
         this.editedFigure = editedArc;

         this.AutoSize = true;
         this.Dock = DockStyle.Top;
         this.BackColor = Color.White;
         this.Cursor = DefaultCursor;


         pnFigureProps = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            Visible = false,
            AutoSize = true
         };
         this.Controls.Add(pnFigureProps);

         pnForButtons = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            AutoSize = true
         };
         this.Controls.Add(pnForButtons);

         btFigureName = new Button
         {
            Size = new Size(303, 40),
            Dock = DockStyle.Top,
            BackColor = Color.MintCream,
            Text = editedArc.Name + "  " + editedArc.Start,
         };
         pnForButtons.Controls.Add(btFigureName);

         btDelete = new Button
         {
            BackgroundImage = Resources.delete,
            Size = new Size(40, 40),
            BackgroundImageLayout = ImageLayout.Stretch,
            Dock = DockStyle.Right,
         };
         pnForButtons.Controls.Add(btDelete);

         lbName = new Label
         {
            Location = new Point(10, 10),
            Text = "Название элемента"
         };
         pnFigureProps.Controls.Add(lbName);

         tbName = new TextBox
         {
            Location = new Point(115, 5),
            Text = editedArc.Name,
            Size = new Size(155, 20),
         };
         pnFigureProps.Controls.Add(tbName);


         //для точки начала
         lbPointStart = new Label
         {
            Location = new Point(10, 35),
            Text = "Точка начала",
         };
         pnFigureProps.Controls.Add(lbPointStart);

         lbPointStartX = new Label
         {
            Location = new Point(115, 35),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartX);

         tbPointStartX = new TextBox
         {
            Location = new Point(145, 30),
            Text = editedArc.Start.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartX);

         lbPointStartY = new Label
         {
            Location = new Point(200, 35),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointStartY);

         tbPointStartY = new TextBox
         {
            Location = new Point(230, 30),
            Text = editedArc.Start.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointStartY);

         //для точки конца
         lbPointEnd = new Label
         {
            Location = new Point(10, 65),
            Text = "Точка конца",
         };
         pnFigureProps.Controls.Add(lbPointEnd);

         lbPointEndX = new Label
         {
            Location = new Point(115, 65),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndX);

         tbPointEndX = new TextBox
         {
            Location = new Point(145, 60),
            Text = editedArc.End.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndX);

         lbPointEndY = new Label
         {
            Location = new Point(200, 65),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointEndY);

         tbPointEndY = new TextBox
         {
            Location = new Point(230, 60),
            Text = editedArc.End.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointEndY);

         //для точки опоры
         lbPointReference = new Label
         {
            Location = new Point(10, 90),
            Text = "Точка опоры",
         };
         pnFigureProps.Controls.Add(lbPointReference);
         lbPointReferenceX = new Label
         {
            Location = new Point(115, 90),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceX);
         tbPointReferenceX = new TextBox
         {
            Location = new Point(145, 85),
            Text = editedArc.Reference.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceX);
         lbPointReferenceY = new Label
         {
            Location = new Point(200, 90),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceY);
         tbPointReferenceY = new TextBox
         {
            Location = new Point(230, 85),
            Text = editedArc.Reference.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceY);

         //для толщины контура сектора
         lbPenThickness = new Label
         {
            Location = new Point(10, 120),
            Text = "Толщина контура",
            Size = new Size(100, 13)
         };
         pnFigureProps.Controls.Add(lbPenThickness);

         numPenThickness = new NumericUpDown
         {
            Location = new Point(115, 115),
            Size = new Size(80, 20),
            Value = new decimal(editedArc.PenThickness),
            Minimum = new decimal(0.5),
            Maximum = new decimal(300.0),
            Increment = new decimal(0.5),
            DecimalPlaces = 1
         };
         pnFigureProps.Controls.Add(numPenThickness);

         //для угла поворота
         lbAngle = new Label
         {
            Location = new Point(10, 160),
            Text = "Угол поворота",
            Size = new Size(89, 13)
         };
         pnFigureProps.Controls.Add(lbAngle);
         numAngle = new NumericUpDown
         {
            Location = new Point(115, 155),
            Size = new Size(80, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedArc.Angle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numAngle);
         //цвет сектора
         lbColorPen = new Label
         {
            Location = new Point(10, 195),
            Size = new Size(98, 13),
            Text = "Цвет дуги"
         };
         pnFigureProps.Controls.Add(lbColorPen);

         BtColorContour = new Button
         {
            Location = new Point(115, 185),
            Size = new Size(82, 40),
            BackColor = editedArc.ColorContour,
         };
         pnFigureProps.Controls.Add(BtColorContour);

         //начальный угол
         lbStartAngle = new Label
         {
            Location = new Point(10, 235),
            Size = new Size(92, 13),
            Text = "Начальный угол"
         };
         pnFigureProps.Controls.Add(lbStartAngle);

         numStartAngle = new NumericUpDown
         {
            Location = new Point(115, 235),
            Size = new Size(85, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedArc.StartAngle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numStartAngle);

         //угол вращения
         lbSweepAngle = new Label
         {
            Location = new Point(10, 265),
            Size = new Size(89, 13),
            Text = "Угол вращения"
         };
         pnFigureProps.Controls.Add(lbSweepAngle);

         numSweepAngle = new NumericUpDown
         {
            Location = new Point(115, 265),
            Size = new Size(85, 20),
            Minimum = -360,
            Maximum = 360,
            Value = (decimal) editedArc.SweepAngle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numSweepAngle);


         //кнопка для сохранения в журнал
         btSaveToJournal = new Button
         {
            Location = new Point(50, 295),
            Size = new Size(200, 40),
            Text = "Сохранить изменения в журнал"
         };
         pnFigureProps.Controls.Add(btSaveToJournal);

         numAngle.ValueChanged += NumAngleValueChanged;
         numPenThickness.ValueChanged += NumPenThicknessValueChanged;
         tbPointEndX.TextChanged += TbPointEndXTextChanged;
         tbPointEndY.TextChanged += TbPointEndYTextChanged;
         BtColorContour.MouseUp += BtColorContourMouseUp;
         tbPointStartX.TextChanged += TbPointStartXTextChanged;
         tbPointStartY.TextChanged += TbPointStartYTextChanged;
         tbPointReferenceX.TextChanged += TbPointReferenceXTextChanged;
         tbPointReferenceY.TextChanged += TbPointReferenceYTextChanged;
         numStartAngle.ValueChanged += NumStartAngleValueChanged;
         numSweepAngle.ValueChanged += NumSweepAngleValueChanged;
         tbName.TextChanged += TbNameTextChanged;

         tbPointStartX.KeyPress += TbPointKeyPress;
         tbPointStartY.KeyPress += TbPointKeyPress;
         tbPointEndX.KeyPress += TbPointKeyPress;
         tbPointEndY.KeyPress += TbPointKeyPress;
         tbPointReferenceX.KeyPress += TbPointKeyPress;
         tbPointReferenceY.KeyPress += TbPointKeyPress;
         //для кнопки удаления 
         btDelete.Click += BtDeleteClick;
         //для всплывающей подсказки
         ttPanelProps = new ToolTip();
         pnFigureProps.VisibleChanged += PnFigurePropsVisibleChanged;
         //this.FormClosing += Window_properties_FormClosing;

         btFigureName.Click += BtFigureNameClick;
      }

      //для многоугольника
      public PanelProperties(FormMain lformMain, MyPolygon editedPolygon)
      {
         this.formMain = lformMain;
         this.editedFigure = editedPolygon;

         this.AutoSize = true;
         this.Dock = DockStyle.Top;
         this.BackColor = Color.White;
         this.Cursor = DefaultCursor;


         pnFigureProps = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            Visible = false,
            AutoSize = true
         };
         this.Controls.Add(pnFigureProps);

         pnForButtons = new Panel
         {
            Dock = DockStyle.Top,
            Location = new Point(0, 0),
            BackColor = Color.White,
            AutoSize = true
         };
         this.Controls.Add(pnForButtons);

         btFigureName = new Button
         {
            Size = new Size(303, 40),
            Dock = DockStyle.Top,
            BackColor = Color.MintCream,
            Text = editedPolygon.Name + "  " + editedPolygon.Start,
         };
         pnForButtons.Controls.Add(btFigureName);

         btDelete = new Button
         {
            BackgroundImage = Resources.delete,
            Size = new Size(40, 40),
            BackgroundImageLayout = ImageLayout.Stretch,
            Dock = DockStyle.Right,
         };
         pnForButtons.Controls.Add(btDelete);

         lbName = new Label
         {
            Location = new Point(10, 10),
            Text = "Название элемента"
         };
         pnFigureProps.Controls.Add(lbName);

         tbName = new TextBox
         {
            Location = new Point(115, 5),
            Text = editedPolygon.Name,
            Size = new Size(155, 20),
         };
         pnFigureProps.Controls.Add(tbName);

         //для точек многоугольника
         cbForpolygonPoints = new ComboBox
         {
            Location = new Point(10, 35),
            Size = new Size(100, 21),
            DataSource = editedPolygon.EnumPolygonPoints(),
            DropDownStyle = ComboBoxStyle.DropDownList
         };
         pnFigureProps.Controls.Add(cbForpolygonPoints);

         lbPointPolygonX = new Label
         {
            Location = new Point(115, 40),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointPolygonX);
         tbPointPolygonX = new TextBox
         {
            Location = new Point(145, 35),
            Text = editedPolygon.Points[cbForpolygonPoints.SelectedIndex + 1].X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointPolygonX);
         lbPointPolygonY = new Label
         {
            Location = new Point(200, 40),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointPolygonY);
         tbPointPolygonY = new TextBox
         {
            Location = new Point(230, 35),
            Text = editedPolygon.Points[cbForpolygonPoints.SelectedIndex + 1].Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointPolygonY);

         //для точки опоры
         lbPointReference = new Label
         {
            Location = new Point(10, 70),
            Text = "Точка опоры",
         };
         pnFigureProps.Controls.Add(lbPointReference);
         lbPointReferenceX = new Label
         {
            Location = new Point(115, 70),
            Text = "X:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceX);
         tbPointReferenceX = new TextBox
         {
            Location = new Point(145, 65),
            Text = editedPolygon.Reference.X.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceX);
         lbPointReferenceY = new Label
         {
            Location = new Point(200, 70),
            Text = "Y:",
            Size = new Size(17, 13)
         };
         pnFigureProps.Controls.Add(lbPointReferenceY);
         tbPointReferenceY = new TextBox
         {
            Location = new Point(230, 65),
            Text = editedPolygon.Reference.Y.ToString("0.0"),
            Size = new Size(40, 20)
         };
         pnFigureProps.Controls.Add(tbPointReferenceY);

         //для толщины линии
         lbPenThickness = new Label
         {
            Location = new Point(10, 105),
            Text = "Толщина контура",
            Size = new Size(100, 13)
         };
         pnFigureProps.Controls.Add(lbPenThickness);

         numPenThickness = new NumericUpDown
         {
            Location = new Point(115, 100),
            Size = new Size(80, 20),
            Value = new decimal(editedPolygon.PenThickness),
            Minimum = new decimal(0.5),
            Maximum = new decimal(300.0),
            Increment = new decimal(0.5),
            DecimalPlaces = 1
         };
         pnFigureProps.Controls.Add(numPenThickness);

         //для угла поворота
         lbAngle = new Label
         {
            Location = new Point(10, 140),
            Text = "Угол поворота",
            Size = new Size(89, 13)
         };
         pnFigureProps.Controls.Add(lbAngle);
         numAngle = new NumericUpDown
         {
            Location = new Point(115, 135),
            Size = new Size(80, 20),
            Minimum = decimal.MinValue,
            Maximum = decimal.MaxValue,
            Value = (decimal) editedPolygon.Angle,
            Increment = (decimal) 5.0,
            DecimalPlaces = 2
         };
         pnFigureProps.Controls.Add(numAngle);
         //цвет прямоугольника
         lbColorPen = new Label
         {
            Location = new Point(10, 170),
            Size = new Size(98, 13),
            Text = "Цвет многоугольника"
         };
         pnFigureProps.Controls.Add(lbColorPen);

         BtColorPen = new Button
         {
            Location = new Point(115, 165),
            Size = new Size(82, 40),
            BackColor = editedPolygon.ColorFilling,
         };
         pnFigureProps.Controls.Add(BtColorPen);

         //цвет контура
         lbColorContour = new Label
         {
            Location = new Point(10, 220),
            Size = new Size(98, 13),
            Text = "Цвет контура"
         };
         pnFigureProps.Controls.Add(lbColorContour);
         BtColorContour = new Button
         {
            Location = new Point(115, 210),
            Size = new Size(82, 40),
            BackColor = editedPolygon.ColorContour,
         };
         pnFigureProps.Controls.Add(BtColorContour);

         //для чекбоксов
         cbFilling = new CheckBox
         {
            Location = new Point(13, 260),
            Size = new Size(97, 17),
            Text = "Закрашенный",
            Checked = editedPolygon.Filled
         };
         pnFigureProps.Controls.Add(cbFilling);

         cbContoured = new CheckBox
         {
            Location = new Point(120, 260),
            Size = new Size(84, 17),
            Text = "С контуром",
            Checked = editedPolygon.Contoured
         };
         pnFigureProps.Controls.Add(cbContoured);


         //кнопка для сохранения в журнал
         btSaveToJournal = new Button
         {
            Location = new Point(50, 290),
            Size = new Size(200, 40),
            Text = "Сохранить изменения в журнал"
         };
         pnFigureProps.Controls.Add(btSaveToJournal);

         numAngle.ValueChanged += NumAngleValueChanged;
         numPenThickness.ValueChanged += NumPenThicknessValueChanged;
         BtColorPen.MouseUp += BtColorPenMouseUp;
         cbFilling.CheckedChanged += CbFillingCheckedChanged;
         cbContoured.CheckedChanged += CbContouredCheckedChanged;
         BtColorContour.MouseUp += BtColorContourMouseUp;
         tbPointReferenceX.TextChanged += TbPointReferenceXTextChanged;
         tbPointReferenceY.TextChanged += TbPointReferenceYTextChanged;
         cbForpolygonPoints.SelectedIndexChanged += CbForpolygonPointsSelectedIndexChanged;
         tbPointPolygonX.TextChanged += TbPointPolygonXTextChanged;
         tbPointPolygonY.TextChanged += TbPointPolygonYTextChanged;
         tbName.TextChanged += TbNameTextChanged;

         tbPointPolygonX.KeyPress += TbPointKeyPress;
         tbPointPolygonY.KeyPress += TbPointKeyPress;
         tbPointReferenceX.KeyPress += TbPointKeyPress;
         tbPointReferenceY.KeyPress += TbPointKeyPress;
         //для кнопки удаления 
         btDelete.Click += BtDeleteClick;
         //для всплывающей подсказки
         ttPanelProps = new ToolTip();
         pnFigureProps.VisibleChanged += PnFigurePropsVisibleChanged;
         //this.FormClosing += Window_properties_FormClosing;

         btFigureName.Click += BtFigureNameClick;
      }

      #endregion
   }
}
