﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace EVGR
{
   public delegate void Refresh();

   [Serializable]
   public class MyLines : Figures
   {
      //угол для правильного поворота линии 
      private float angleToX;
      private PointF[] points;
      
      public override PointF Start
      {
         get { return start; }
         set { start = value; }
      }
      public event Refresh ChangeEnd;
      
      public override PointF End
      {
         get { return end; }
         set {
            end = value;
            ChangeEnd?.Invoke();
         }
      }

      public PointF[] Points
      {
         get { return points; }
         set { points = value; }
      }

      public override float Angle
      {
         get { return angle; }
         set { angle = value; }
      }

      public float AngleToX
      {
         get { return (float) (Math.Atan2(end.Y - start.Y, end.X - start.X)*Constants.RAD_TO_GRAD); }
         set { angleToX = value; }
      }

      
      public MyLines(string name, PointF start, PointF reference, float angle, float penThickness,
         Color colorContour, PointF end, float angleToX, PointF[] points, bool selected, DashStyle dashStyle)
      {
         this.points = points;
         this.angleToX = angleToX;
         this.name = name;
         this.start = start;
         this.reference = reference;
         this.end = end;
         this.angle = 0;
         this.colorContour = colorContour;
         this.penThickness = penThickness;
         this.isSelected = selected;
         this.dashStyle = dashStyle;
      }

      public bool InsideOfLine(PointF location)
      {
         return CommonMethods.InsideOfPolygonLineRectangle(Points, location);
      }



      public string WriteToJournal()
      {
         string str = Name + " Толщина линии: " + PenThickness + " Точка начала: " +
                      Start + " Координата точки опоры: " + Reference + " Цвет: " +
                      ColorFilling + " Точка конца: " + End + " Угол опоры: " +
                      Angle + " Длина: " + (int) Math.Abs(Math.Sqrt((
                         End.Y - Start.Y)*(End.Y - Start.Y) + (End.X - Start.X)*(End.X - Start.X)));
         return str;
      }

      //(для рисования) метод для перемещения точек линии в соответствии с заданным углом (начальный = 0) 
      public void ConvertPointsOfLineWhileDrawing()
      {
         PointF[] ConvertedPoints = new PointF[points.Count()];
         ConvertedPoints[0].X = (float) (-(-penThickness/2)*Math.Sin(AngleToX/180.0*Math.PI)) + start.X;
         ConvertedPoints[0].Y = (float) ((-penThickness/2)*Math.Cos(AngleToX/180.0*Math.PI)) + start.Y;
         ConvertedPoints[1].X = (float) (-(penThickness/2)*Math.Sin(AngleToX/180.0*Math.PI)) + start.X;
         ConvertedPoints[1].Y = (float) ((penThickness/2)*Math.Cos(AngleToX/180.0*Math.PI)) + start.Y;
         ConvertedPoints[2].X = (float) (-(penThickness/2)*Math.Sin(AngleToX/180.0*Math.PI)) + end.X;
         ConvertedPoints[2].Y = (float) ((penThickness/2)*Math.Cos(AngleToX/180.0*Math.PI)) + end.Y;
         ConvertedPoints[3].X = (float) (-(-penThickness/2)*Math.Sin(AngleToX/180.0*Math.PI)) + end.X;
         ConvertedPoints[3].Y = (float) ((-penThickness/2)*Math.Cos(AngleToX/180.0*Math.PI)) + end.Y;
         Points = ConvertedPoints;
      }

      //(для изменения) метод для перемещения точек линии в соответствии с заданным углом (начальный = 0) 
      public void ConvertPointsOfLineWithAngle()
      {
        CommonMethods.RotatePolygonOrLine
            (ref points, ref prevAngle, reference, angle);
      }
      
      //изменение точек начала и конца в соотв с углом
      public PointF ModifyPointsLine(PointF f)
      {
         float x = f.X - reference.X;
         float y = f.Y - reference.Y;
         f.X = (float) (x*Math.Cos(angle/180.0*Math.PI) -
                        (y)*Math.Sin(angle/180.0*Math.PI)) + reference.X;
         f.Y = (float) (x*Math.Sin(angle/180.0*Math.PI) +
                        (y)*Math.Cos(angle/180.0*Math.PI)) + reference.Y;
         return new PointF(f.X, f.Y);
      }

      /// <summary>
      /// Перемещение прямоугольника по горизонтали и по вертикали
      /// </summary>
      /// <returns></returns>
      public void LineHorVerMoving(float x, float y)
      {
         PointF[] NewPointsArray = new PointF[points.Length];
         for (int i = 0; i < Points.Length; i++)
         {
            NewPointsArray[i] = new PointF(Points[i].X + x, Points[i].Y + y);
         }
         Points = NewPointsArray;
         Start = new PointF(Start.X + x, Start.Y + y);
         End = new PointF(End.X + x, End.Y + y);
      }

      public void DrawSelectLines(PaintEventArgs e)
      {
         //объект, определяющий цвет линий и ширину
         Pen pen = new Pen(colorContour, PenThickness);
         pen.DashStyle = DashStyle;
         e.Graphics.DrawLine(pen, Start, End);

         if (IsSelected) //выделение линии
         { 
            Markers.DrawMarkersForLine(e, this);
         }
      }
      

   }
}
