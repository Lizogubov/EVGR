﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace EVGR
{
   [Serializable]
   public class MyPie : Figures
   {
      private float startAngle;
      private float sweepAngle;
      private float prevStartAngle;
      private float prevSweepAngle;

      public float PrevSweepAngle
      {
         get { return prevSweepAngle; }
         set { prevSweepAngle = value; }
      }

      public float PrevStartAngle
      {
         get { return prevStartAngle; }
         set { prevStartAngle = value; }
      }

      public override PointF Start
      {
         get { return start; }
         set { start = value; }
      }

      public override PointF End
      {
         get { return end; }
         set { end = value; }
      }

      public float StartAngle
      {
         get { return (startAngle); }
         set { startAngle = value; }
      }

      public float SweepAngle
      {
         get { return sweepAngle; }
         set { sweepAngle = value; }
      }

      public MyPie(string name, PointF start, PointF reference, double angle, float PenThickness, Color colorPen,
         PointF end, float sweepAngle,float startAngle, float width, float height, bool filling, bool contoured,
         Color colorContour, bool selected, DashStyle dashStyle, bool hatchStyleOn, HatchStyle hatchStyle)

      {
         this.colorContour = colorContour;
         this.contoured = contoured;
         this.filled = filling;
         this.startAngle = startAngle;
         this.sweepAngle = sweepAngle;
         this.width = width;
         this.height = height;
         this.name = name;
         this.start = start;
         this.reference = reference;
         this.angle = 0;
         this.colorFilling = colorPen;
         this.penThickness = PenThickness;
         this.end = end;
         this.isSelected = selected;
         this.dashStyle = dashStyle;
         this.hatchStyleOn = hatchStyleOn;
         this.hatchStyle = hatchStyle;
      }

      public string WriteToJournal()
      {
         string str = Name;
         if (!Filled)
            str += " Толщина линии: " + PenThickness;

         str += " Точка начала: " + Start + " Точка опоры: " + Reference +
                " Цвет: " + ColorFilling + " Точка конца: " + End + " Угол опоры: " + Angle +
                " Радиус: " + (float) Math.Abs(Math.Sqrt((End.Y - Start.Y)*(End.Y - Start.Y) +
                                                         (End.X - Start.X)*(End.X - Start.X))) +
                " Начальный угол: " + StartAngle + " Угол вращения: " + SweepAngle +
                " Закрашен: " + Filled + " Контур: " + Contoured;
         if (Contoured)
            str += " Цвет контура: " + ColorContour;

         return str;
      }

      public bool InPie(PointF location)
      {
         //смещаем координаты в начало (0,0)
         float x = End.X - Start.X;
         float y = End.Y - Start.Y;
         float x0 = location.X - Start.X;
         float y0 = location.Y - Start.Y;
         //если радиус от проверяемой точки меньше радиуса сектора и точка находится в пределах угла вращения
         if (x0*x0 + y0*y0 <= x*x + y*y &&
             (x*x0 + y*y0)/(Math.Sqrt(x*x + y*y)*Math.Sqrt(x0*x0 + y0*y0)) >=
             Math.Cos((double) SweepAngle/2/180.0*3.14))
            return true;

         return false;
      }

      public void ModifyPointsPieArcWithAngle()
      {
         CommonMethods.ModifyPieArcPointsWithAngle
            (ref start, ref end, ref prevAngle , reference, angle);
      }

      public void ModifyPointsPieArcWithStartAngle()
      {
         CommonMethods.ModifyPieArcEndWithStartAngle
            (ref end, ref prevStartAngle, start, startAngle);
      }

      public void ModifyPointsPieArcWithSweepAngle()
      {
         CommonMethods.ModifyPieArcEndWithSweepAngle
            (ref end, ref prevSweepAngle, start,  sweepAngle);
      }

      public float GetStartAngle()
      {
         return CommonMethods.GetStartAngle
            (start, end, sweepAngle);
      }

      public float GetDiametr()
      {
         return CommonMethods.GetDiametrOfPieArc(start, end);
      }

      /// <summary>
      /// Перемещение сектора по горизонтали и по вертикали
      /// </summary>
      /// <returns></returns>
      public void PieHorVerMoving(float x, float y)
      {
         Start = new PointF(Start.X + x, Start.Y + y);
         End = new PointF(End.X + x, End.Y + y);
      }

      public void DrawSelectPies(PaintEventArgs e)
      {
         // объединяем высоту и ширину,равную теперь диаметру, тк для рисования сектора используется круг
         float wh = GetDiametr();
         if (wh > 0.0) // должна быть больше 0,тк метод DrawPie не подразумевает такую проверку
         {
            if (!Filled && !Contoured)
            {
               Pen penColorPie = new Pen(ColorContour, PenThickness);
               penColorPie.DashStyle = DashStyle;
                  e.Graphics.DrawPie(penColorPie, Start.X - wh/2, Start.Y - wh/2,
                     wh, wh, (StartAngle + Angle), SweepAngle);
            }
            if (Filled)
            {
               if (HatchStyleOn)
                  using (Brush brushColorPie = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                     e.Graphics.FillPie(brushColorPie, Start.X - wh / 2, Start.Y - wh / 2,
                     wh, wh, (StartAngle + Angle), SweepAngle);
               else
                  using (Brush brushColorPie = new SolidBrush(ColorFilling))
                     e.Graphics.FillPie(brushColorPie, Start.X - wh / 2, Start.Y - wh / 2,
                        wh, wh, (StartAngle + Angle), SweepAngle);
            }
            else if (Contoured)
            {
               if(HatchStyleOn)
                  using (Brush brushColorPie = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                     e.Graphics.FillPie(brushColorPie, Start.X - wh/2, Start.Y - wh/2,
                     wh, wh, (StartAngle + Angle), SweepAngle);
               else
                  using (Brush brushColorPie = new SolidBrush(ColorFilling))
                     e.Graphics.FillPie(brushColorPie, Start.X - wh / 2, Start.Y - wh / 2,
                        wh, wh, (StartAngle + Angle), SweepAngle);

               Pen contourColorPie = new Pen(ColorContour, PenThickness);
               contourColorPie.DashStyle = DashStyle;
                  e.Graphics.DrawPie(contourColorPie, Start.X - wh/2, Start.Y - wh/2,
                     wh, wh, (StartAngle + Angle), SweepAngle);
            }
         }

         if (IsSelected)
         {
            Pen penColorPie2 = new Pen(Color.Gold, PenThickness);
            penColorPie2.DashStyle = DashStyle.Dot;
            //e.Graphics.DrawPie(penColorPie2, Start.X - wh/2, Start.Y - wh/2,wh, wh, (StartAngle + Angle), SweepAngle);
            Markers.DrawMarkersForPie(e, this);
         }
      }

   }
}
