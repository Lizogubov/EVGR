﻿ using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace EVGR
{
   [Serializable]
   public class MyPolygon : Figures
   {
      private PointF[] points;


      public PointF[] Points
      {
         get { return points; }
         set { points = value; }
      }

      public override float Angle
      {
         get { return angle; }
         set { angle = value; }
      }

      public MyPolygon(string name, PointF start, PointF reference, float angle, float PenThickness, Color colorPen,
         PointF[] points, bool filling, bool contoured, Color colorContour, bool selected, DashStyle dashStyle,
         bool hatchStyleOn, HatchStyle hatchStyle)

      {
         this.colorContour = colorContour;
         this.contoured = contoured;
         this.filled = filling;
         this.points = points;
         this.name = name;
         this.start = start;
         this.reference = reference;
         this.angle = angle;
         this.colorFilling = colorPen;
         this.penThickness = PenThickness;
         this.isSelected = selected;
         this.dashStyle = dashStyle;
         this.hatchStyleOn = hatchStyleOn;
         this.hatchStyle = hatchStyle;
      }

      public string WriteToJournal()
      {
         string str = Name;
         if (!Filled)
            str += " Толщина линии: " + PenThickness;

         str += " Координата точки опоры: " + Reference +
                " Цвет: " + ColorFilling + " Угол опоры: " + Angle +
                " Закрашен: " + Filled + " Контур: " + Contoured;
         if (Contoured)
            str += " Цвет контура: " + ColorContour;
         return str;
      }


        protected override void SetRefPointToCenter()
        {
            PointF CenterMass= new Point(0,0);
            for (int i = 0; i < points.Count(); i++)
            {
                //находим сумму координат между точками многоугольника, последовательно
                CenterMass.X += points[i].X;
                CenterMass.Y += points[i].Y;
            }
            Reference =  new PointF(CenterMass.X/points.Count(), CenterMass.Y/points.Count());
        }

      /// <summary>
      /// Для каждой вершины A[n] - вычисляем булевский флажок, находится точка над или под лучом,
      /// то есть просто сравниваем координату .y вершины с нулем, flag[n] = A[n]
      /// Затем идем по парам точек (A[n-1],A[n]) и считаем пересечения когда этот флажок меняется с true на false и наоборот.
      ///  При этом проверяем, что отрезок проходит справа от точки O, для этого сравниваем знак определителя |A[n-1]-A[n], A[n]-O| с 0.
      /// </summary>
      public bool InsideOfPolygon(PointF location)
      {
         return CommonMethods.InsideOfPolygonLineRectangle(points, location);
      }

      public Array EnumPolygonPoints()
      {
         String[] array = new string[Points.Count()];
         for (int i = 0; i < Points.Count(); i++)
         {
            array[i] = ("Точка №" + (i + 1));
         }
         return array;
      }

      public void RotatePolygon()
      {
         CommonMethods.RotatePolygonOrLine (ref points, ref prevAngle, reference, angle);
      }

      public void BuildPolygonWhileMoving(PointF elocation, PointF previewElocation)
      {
         PointF[] NewPointsArray = new PointF[Points.Length];
         for (int i = 0; i < Points.Length; i++)
         {
            NewPointsArray[i] = new PointF(Points[i].X - (previewElocation.X - elocation.X),
               Points[i].Y - (previewElocation.Y - elocation.Y));
         }
         Points = NewPointsArray;
      }

      //перемещение многоугольника по горизонтали и по вертикали
      public void PolygonHorVerMoving(float x, float y)
      {
         PointF[] NewPointsArray = new PointF[points.Length];
         for (int i = 0; i < Points.Length; i++)
         {
            NewPointsArray[i] = new PointF(Points[i].X + x, Points[i].Y + y);
         }
         Points = NewPointsArray;
      }

      
      public void DrawSelectPolygons(PaintEventArgs e)
      {
         if (Points.Length > 1) //точек должно быть больше 1
         {
            if (!Filled && !Contoured)
            {
               Pen penColorPolygon = new Pen(ColorContour, PenThickness);
               penColorPolygon.DashStyle = DashStyle;
                  e.Graphics.DrawPolygon(penColorPolygon, Points);
            }
            else if (Filled)
            {
               if (HatchStyleOn)
                  using (Brush brushColorPolygon = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                     e.Graphics.FillPolygon(brushColorPolygon, Points);
               else
                  using (Brush brushColorPolygon = new SolidBrush(ColorFilling))
                     e.Graphics.FillPolygon(brushColorPolygon, Points);

               if (Points.Count() <= 3 && SwitchConditions.PolygonPressed == true)
               {
                  using (Pen penColorPolygon = new Pen(ColorContour, PenThickness))
                     e.Graphics.DrawLine(penColorPolygon, Points[0], Points[1]);
               }
            }
            else if (Contoured)
            {
               if(HatchStyleOn)
                  using (Brush brushColorPolygon = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                     e.Graphics.FillPolygon(brushColorPolygon, Points);
               else
                  using (Brush brushColorPolygon = new SolidBrush(ColorFilling))
                     e.Graphics.FillPolygon(brushColorPolygon, Points);

               Pen contourColorPolygon = new Pen(ColorContour, PenThickness);
               contourColorPolygon.DashStyle = DashStyle;
                  e.Graphics.DrawPolygon(contourColorPolygon, Points);
            }
         }

         if (IsSelected)
         {
            Markers.DrawMarkersForPolygon(e, this);
         }
      }

   }
}
