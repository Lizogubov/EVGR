﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace EVGR
{
   [Serializable]
   public class MyEllipse : Figures
   {
      public override PointF Start
      {
         get { return start; }
         set { start = value; }
      }

      public override PointF End
      {
         get { return end; }
         set { end = value; }
      }
      
      public MyEllipse(string name, PointF start, PointF reference,
         float penThickness, Color colorPen, PointF end, float width, float height,
         bool filling, bool contoured, Color colorContour, bool selected, DashStyle dashStyle,
         bool hatchStyleOn, HatchStyle hatchStyle)

      {
         this.colorContour = colorContour;
         this.contoured = contoured;
         this.filled = filling;
         this.width = width;
         this.height = height;
         this.name = name;
         this.start = start;
         this.reference = reference;
         this.angle = 0;
         this.colorFilling = colorPen;
         this.penThickness = penThickness;
         this.end = end;
         this.isSelected = selected;
         this.dashStyle = dashStyle;
         this.hatchStyleOn = hatchStyleOn;
         this.hatchStyle = hatchStyle;
      }

      public string WriteToJournal()
      {
         string str = Name;
         if (!Filled)
            str += " Толщина линии: " + PenThickness;

         str += " Точка начала: " + Start + " Цвет: " + ColorFilling +
                " Точка конца: " + End + " Радиус: " + Math.Abs(Width) +
                " Закрашен: " + Filled + " Контур: " + Contoured;
         if (Contoured)
            str += " Цвет контура: " + ColorContour;
         return str;
      }

      public bool InEllipse(PointF location)
      {
         float x = location.X;
         float y = location.Y;
         //большая и малая полуоси
         float a = (float) Math.Abs(Width/2.0);
         float b = (float) Math.Abs(Height/2.0);
         //для вытянутых вверх/вниз эллипсов
         if (a < b)
         {
            b = (float) Math.Abs(Width/2.0);
            a = (float) Math.Abs(Height/2.0);
         }

         //центр эллипса
         float centerx = (Start.X + End.X)/2.0F;
         float centery = (Start.Y + End.Y)/2.0F;

         //Эксцентриситет
         float ecs = (float) Math.Sqrt(1 - (b*b)/(a*a));
         //фокальное расстояние
         float f = a*ecs;
         //Фокусы
         PointF f1 = new PointF(centerx - f, centery);
         PointF f2 = new PointF(centerx + f, centery);
         //Расстояния от точки до фокусов
         double d1 = Math.Sqrt(Math.Pow(f1.X - x, 2) + Math.Pow(f1.Y - y, 2));
         double d2 = Math.Sqrt(Math.Pow(f2.X - x, 2) + Math.Pow(f2.Y - y, 2));
         //для вытянутых вверх/вниз эллипсов
         if (Math.Abs(Width) < Math.Abs(Height))
         {
            f1 = new PointF(centerx, centery - f);
            f2 = new PointF(centerx, centery + f);
            d1 = Math.Sqrt(Math.Pow(f1.X - x, 2) + Math.Pow(f1.Y - y, 2));
            d2 = Math.Sqrt(Math.Pow(f2.X - x, 2) + Math.Pow(f2.Y - y, 2));
         }
         if (Math.Round(d1 + d2, 5) <= Math.Round(2*a, 5))
            return true;

         return false;
      }

      /// <summary>
      /// Движение по горизонтали и по вертикали
      /// </summary>
      public void EllipseHorVerMoving(float x, float y)
      {
         Start = new PointF(Start.X + x, Start.Y + y);
         End = new PointF(End.X + x, End.Y + y);
      }


      public void DrawSelectEllipses(PaintEventArgs e)
      {
            //float y = Start.Y + End.X - Start.X; // округление эллипса
            //End = new PointF(End.X, y);
            //Width = Height;
            if (!Filled && !Contoured)
            {
               Pen penColorEllipses = new Pen(ColorContour, PenThickness);
               penColorEllipses.DashStyle = DashStyle;
               e.Graphics.DrawEllipse(penColorEllipses, Start.X, Start.Y, Width, Height);
            }
            else if (Filled)
            {
               if(HatchStyleOn)
                  using(Brush brushColorEllipses = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                     e.Graphics.FillEllipse(brushColorEllipses, Start.X, Start.Y, Width, Height);
               else
                  using (Brush brushColorEllipses = new SolidBrush(ColorFilling))
                     e.Graphics.FillEllipse(brushColorEllipses, Start.X, Start.Y, Width, Height);
            }
            else if (Contoured)  
            {
               if (HatchStyleOn)
                  using (Brush brushColorEllipses = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                     e.Graphics.FillEllipse(brushColorEllipses, Start.X, Start.Y, Width, Height);
               else
                  using (Brush brushColorEllipses = new SolidBrush(ColorFilling))
                     e.Graphics.FillEllipse(brushColorEllipses, Start.X, Start.Y, Width, Height);

               Pen contourColorEllipses = new Pen(ColorContour, PenThickness);
               contourColorEllipses.DashStyle = DashStyle;
               e.Graphics.DrawEllipse(contourColorEllipses, Start.X, Start.Y, Width, Height);
            }
            if (IsSelected)
            {
               Markers.DrawMarkersForEllipse(e, this);
            }
       }



    }
}

