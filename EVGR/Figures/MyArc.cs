﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace EVGR
{
   [Serializable]
   public class MyArc : Figures
   {
      private float startAngle;
      private float sweepAngle;
      private float prevStartAngle;
      private float prevSweepAngle;

      public float PrevSweepAngle
      {
         get { return prevSweepAngle; }
         set { prevSweepAngle = value; }
      }

      public float PrevStartAngle
      {
         get { return prevStartAngle; }
         set { prevStartAngle = value; }
      }

      public float StartAngle
      {
         get { return (startAngle); }
         set { startAngle = value; }
      }

      public float SweepAngle
      {
         get { return sweepAngle; }
         set { sweepAngle = value; }
      }

      public override PointF Start
      {
         get { return start; }
         set { start = value; }
      }

      public override PointF End
      {
         get { return end; }
         set { end = value; }
      }

      public MyArc(string name, PointF start, PointF reference, double angle, float PenThickness, Color colorContour,
         PointF end, float sweepAngle,float startAngle, bool selected,DashStyle dashStyle)

      {
         this.startAngle = startAngle;
         this.sweepAngle = sweepAngle;
         this.name = name;
         this.start = start;
         this.reference = reference;
         this.angle = 0;
         this.ColorContour = colorContour;
         this.penThickness = PenThickness;
         this.end = end;
         this.isSelected = selected;
         this.dashStyle = dashStyle;
      }

      public string WriteToJournal()
      {
         string str = Name;
         str += " Толщина линии: " + PenThickness;
         str += " Точка начала: " + Start + " Точка опоры: " + Reference +
                " Цвет: " + ColorFilling + " Точка конца: " + End + " Угол опоры: " + Angle +
                " Радиус: " + (float) Math.Abs(Math.Sqrt((End.Y - Start.Y)*(End.Y - Start.Y) +
                                                         (End.X - Start.X)*(End.X - Start.X))) + " Начальный угол: " +
                StartAngle +
                " Угол вращения: " + SweepAngle;

         return str;
      }

      public bool InArc(PointF location)
      {
         float x = End.X - Start.X;
         float y = End.Y - Start.Y;
         float x0 = location.X - Start.X;
         float y0 = location.Y - Start.Y;
         float R = (float) Math.Sqrt(x*x + y*y);

         //если разность радиуса проверяемой точки и радиуса сектора меньше числа, равному толщине линии,умноженной на радиус дуги
         //и их же разность больше 0 и точка находится в пределах угла вращения
         if (Math.Abs((x0*x0 + y0*y0) - (x*x + y*y)) < PenThickness*R &&
             Math.Abs((x0*x0 + y0*y0) - (x*x + y*y)) > 0 &&
             (x*x0 + y*y0)/(Math.Sqrt(x*x + y*y)*Math.Sqrt(x0*x0 + y0*y0)) >=
             Math.Cos((double) SweepAngle/2/180.0*3.14))
            return true;

         return false;
      }

      public void ModifyPointsPieArcWithAngle()
      {
         CommonMethods.ModifyPieArcPointsWithAngle
            (ref start,  ref end, ref prevAngle, reference, angle);
      }

      public void ModifyPointsPieArcWithStartAngle()
      {
         CommonMethods.ModifyPieArcEndWithStartAngle
            (  ref end, ref prevStartAngle, start, startAngle);
      }

      public void ModifyPointsPieArcWithSweepAngle()
      {
         CommonMethods.ModifyPieArcEndWithSweepAngle
            (ref end , ref prevSweepAngle, start, sweepAngle);
      }

      public float GetStartAngle()
      {
         return CommonMethods.GetStartAngle
            (start,  end, sweepAngle);
      }

      public float GetDiametr()
      {
         return CommonMethods.GetDiametrOfPieArc( start,  end);
      }

      /// <summary>
      /// Перемещение дуги по горизонтали и по вертикали
      /// </summary>
      /// <returns></returns>
      public void ArcHorVerMoving(float x, float y)
      {
         Start = new PointF(Start.X + x, Start.Y + y);
         End = new PointF(End.X + x, End.Y + y);
      }

      public void DrawSelectArcs(PaintEventArgs e)
      {
         float Wh = GetDiametr();
         if (Wh > 0.0)
         {
            Pen penColorArc = new Pen(ColorContour, PenThickness);
            penColorArc.DashStyle = DashStyle;
            e.Graphics.DrawArc(penColorArc, Start.X - Wh/2, Start.Y - Wh/2,
               Wh, Wh, (StartAngle + Angle), SweepAngle);
         }
         if (this.IsSelected)
            {
               Markers.DrawMarkersForArc(e, this);
            }
       }
         
   }
}