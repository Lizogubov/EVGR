﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace EVGR
{
   [Serializable]
   public class Figures
   {
      //параметры для фигур
      protected float penThickness;
      protected string name;
      protected PointF start;
      protected Color colorFilling;
      protected PointF reference;
      protected PointF end;
      protected float angle;
      protected float width;
      protected float height;
      protected bool filled;
      protected bool contoured;
      protected Color colorContour;
      [NonSerialized]
      protected bool isSelected;
      protected float prevAngle;
      protected DashStyle dashStyle;
      protected bool hatchStyleOn;
      protected HatchStyle hatchStyle;

      //коллекция для всех объектов по порядку
      private List<Figures> poryadok;
      //коллекция для журнала изменений на форме 
      private List<string> Journal;
      //создание коллекций линий
      private List<MyLines> CollectionLines;
      //коллекция для прямоугольника
      private List<MyRectangle> CollectionRectangles;
      //для многоугольника
      private List<MyPolygon> CollectionPolygons;
      //для сектора
      private List<MyPie> CollectionPies;
      //для овала
      private List<MyEllipse> CollectionEllipses;
      //для дуги
      private List<MyArc> CollectionArcs;

      // Свойство для удаления фигуры
      private Figures currentObj;
      // Точка для запоминания координат в момент до начала перемещения фигуры
      private PointF pointToMove;
      [NonSerialized]
      private FormMain formMain;

      //Прилепляет полосу прокрутки вниз - для журнала
      [DllImport("user32.dll")]
      private static extern IntPtr SendMessage(IntPtr handle, Int32 msg, Int32 wParam, Int32 lParam);
      //После добавления в текстбокс 
      // SendMessage(richTextBox_Jornal.Handle, 0x115/*WM_VSCROLL*/, 0x07/*SB_BOTTOM*/, 0);
      
      public Figures()
      {
      }

      public Figures(FormMain formMain)
      {
         this.formMain = formMain;
         poryadok = new List<Figures>();
         Journal = new List<string>();
         CollectionLines = new List<MyLines>();
         CollectionRectangles = new List<MyRectangle>();
         CollectionPolygons = new List<MyPolygon>();
         CollectionPies = new List<MyPie>();
         CollectionEllipses = new List<MyEllipse>();
         CollectionArcs = new List<MyArc>();
      }

      public List<Figures> Poryadok
      {
         get { return poryadok; }
         set { poryadok = value; }
      }

      public float PrevAngle
      {
         get { return prevAngle; }
         set { prevAngle = value; }
      }

      public bool IsSelected
      {
         get { return isSelected; }
         set { isSelected = value; }
      }

      public Color ColorContour
      {
         get { return colorContour; }
         set { colorContour = value; }
      }

      public bool Contoured
      {
         get { return contoured; }
         set { contoured = value; }
      }

      public bool Filled
      {
         get { return filled; }
         set { filled = value; }
      }

      public virtual float Height
      {
         get { return end.Y - start.Y; }
         set { height = value; }
      }

      public virtual float Width
      {
         get { return end.X - start.X; }
         set { width = value; }
      }

      public virtual float Angle
      {
         get { return angle; }
         set { angle = value; }
      }

      public virtual PointF End
      {
         get
         {
            //свойство для перемещения точки в соответствии с заданным углом (начальный = 0)
            PointF ConvertedPoint = new PointF();
            float x = (end.X - reference.X);
            float y = (end.Y - reference.Y);
            float X = (float) (x*Math.Cos(angle/180.0*Math.PI) -
                               y*Math.Sin(angle/180.0*Math.PI)) + reference.X;
            float Y = (float) (x*Math.Sin(angle/180.0*Math.PI) +
                               y*Math.Cos(angle/180.0*Math.PI)) + reference.Y;
            ConvertedPoint = new PointF(X, Y);
            return ConvertedPoint;
         }
         set { end = value; }
      }

      public virtual PointF Start
      {
         get
         {
            //свойство для перемещения точки в соответствии с заданным углом (начальный = 0)
            PointF ConvertedPoint = new PointF();
            float x = (start.X - reference.X);
            float y = (start.Y - reference.Y);
            float X = (float) (x*Math.Cos(angle/180.0*Math.PI) -
                               y*Math.Sin(angle/180.0*Math.PI)) + reference.X;
            float Y = (float) (x*Math.Sin(angle/180.0*Math.PI) +
                               y*Math.Cos(angle/180.0*Math.PI)) + reference.Y;
            ConvertedPoint = new PointF(X, Y);
            return ConvertedPoint;
         }
         set { end = value; }
      }

      public Color ColorFilling
      {
         get { return colorFilling; }
         set { colorFilling = value; }
      }

      public PointF Reference
      {
         get { return new PointF(reference.X, reference.Y); }
         set { reference = value; }
      }

      public float PenThickness
      {
         get { return penThickness; }
         set { penThickness = value; }
      }

      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      public DashStyle DashStyle
      {
         get { return dashStyle; }
         set { dashStyle = value; }
      }

      public bool HatchStyleOn
      {
         get { return hatchStyleOn; }
         set { hatchStyleOn = value; }
      }

      public HatchStyle HatchStyle
      {
         get { return hatchStyle; }
         set { hatchStyle = value; }
      }

      public Figures CurrentObj
      {
         get { return currentObj; }
         set { currentObj = value; }
      }

      public static bool AngleChanging
      {
         get { return angleChanging; }
         set { angleChanging = value; }
      }

      protected virtual void SetRefPointToCenter()
      {
         Reference = new PointF((End.X + Start.X) / 2, (End.Y + Start.Y) / 2);
      }

      #region Основные методы
      
      /// <summary>
      /// Цикл для отрисовки элементов  по порядку их слоев
      /// </summary>
      public void Paint(PaintEventArgs e)
      {
         foreach (var item in Poryadok)
         {
            if (item is MyLines)
            {
               if ((MyLines) item != currentObj)
               {
                  ((MyLines) item).DrawSelectLines(e);
               }
               else
               {
                  CollectionLines.RemoveAt(CollectionLines.FindIndex(x => x == (MyLines) item));
                  Poryadok.RemoveAt(Poryadok.FindIndex(x => x == (MyLines) item));
                  break;
               }
            }
            else if (item is MyRectangle)
            {
               if ((MyRectangle)item != currentObj)
               {
                  ((MyRectangle)item).DrawSelectRectangles(e);
               }
               else
               {
                  CollectionRectangles.RemoveAt(CollectionRectangles.FindIndex(x => x == (MyRectangle)item));
                  Poryadok.RemoveAt(Poryadok.FindIndex(x => x == (MyRectangle)item));
                  break;
               }
            }
            else if (item is MyEllipse)
            {
               if ((MyEllipse)item != currentObj)
               {
                  ((MyEllipse)item).DrawSelectEllipses(e);
               }
               else
               {
                  CollectionEllipses.RemoveAt(CollectionEllipses.FindIndex(x => x == (MyEllipse)item));
                  Poryadok.RemoveAt(Poryadok.FindIndex(x => x == (MyEllipse)item));
                  break;
               }
            }
            else if (item is MyPolygon)
            {
               if ((MyPolygon)item != currentObj)
               {
                  ((MyPolygon)item).DrawSelectPolygons(e);
               }
               else
               {
                  CollectionPolygons.RemoveAt(CollectionPolygons.FindIndex(x => x == (MyPolygon)item));
                  Poryadok.RemoveAt(Poryadok.FindIndex(x => x == (MyPolygon)item));
                  break;
               }
            }
            else if (item is MyPie)
            {
               if ((MyPie)item != currentObj)
               {
                  ((MyPie)item).DrawSelectPies(e);
               }
               else
               {
                  CollectionPies.RemoveAt(CollectionPies.FindIndex(x => x == (MyPie)item));
                  Poryadok.RemoveAt(Poryadok.FindIndex(x => x == (MyPie)item));
                  break;
               }
            }
            else if (item is MyArc)
            {
               if ((MyArc)item != currentObj)
               {
                  ((MyArc)item).DrawSelectArcs(e);
               }
               else
               {
                  CollectionArcs.RemoveAt(CollectionArcs.FindIndex(x => x == (MyArc)item));
                  Poryadok.RemoveAt(Poryadok.FindIndex(x => x == (MyArc)item));
                  break;
               }
            }
         }
      }

      /// <summary>
      /// Начинаем рисование фигур - строим заготовочную модель для каждой фигуры
      /// </summary>
      public void StartDrawingFigures(PointF elocation)
      {
         UnselectAllItems();
         if (SwitchConditions.LineDrawing && SwitchConditions.LinePressed == false)
         {
            StartAddingNewLine(elocation);
         }
         else if (SwitchConditions.RectangleDrawing && SwitchConditions.RectanglePressed == false)
         {
            StartAddingNewRectangle(elocation);
         }
         else if (SwitchConditions.EllipseDrawing)
         {
            StartAddingNewEllipse(elocation);
         }
         else if (SwitchConditions.PolygonDrawing && SwitchConditions.PolygonPressed == false)
         {
            StartAddingNewPolygon(elocation);
         }
         else if (SwitchConditions.PieDrawing)
         {
            StartAddingNewPie(elocation);
         }
         else if (SwitchConditions.ArcDrawing)
         {
            StartAddingNewArc(elocation);
         }
      }

      

      /// <summary>
      /// Рисуем фигуры - получаем координаты от мыши во время движения
      /// </summary>
      public void DrawingFigures(PointF elocation)
      {
         if (SwitchConditions.LineDrawing && SwitchConditions.LinePressed)
         {
            CollectionLines.Last().End = elocation;
            CollectionLines.Last().ConvertPointsOfLineWhileDrawing();
         }
         else if (SwitchConditions.RectangleDrawing && SwitchConditions.RectanglePressed)
         {
            CollectionRectangles.Last().End = elocation;
            CollectionRectangles.Last().BuildRectangle();
         }
         else if (SwitchConditions.PieDrawing)
         {
            CollectionPies.Last().End = elocation;
            //устанавливаем угол поворота, равный 45'
            CollectionPies.Last().SweepAngle = 45;
            CollectionPies.Last().StartAngle = CollectionPies.Last().GetStartAngle();
         }
         else if (SwitchConditions.ArcDrawing)
         {
            CollectionArcs[CollectionArcs.Count - 1].End = elocation;
            //устанавливаем угол поворота, равный 45'
            CollectionArcs.Last().SweepAngle = 45;
            CollectionArcs.Last().StartAngle = CollectionArcs.Last().GetStartAngle();
         }
         else if (SwitchConditions.EllipseDrawing)
         {
            CollectionEllipses.Last().End = elocation;
         }
         else if (SwitchConditions.PolygonDrawing)
         {
            if (CollectionPolygons.Count > 0 && CollectionPolygons.Last().Points.Count() > 1)
                  CollectionPolygons.Last().Points[CollectionPolygons.Last().Points.Length - 1] = elocation;
         }
      }
      
      /// <summary>
      /// Заканчиваем рисование фигур при отпускании кнопки мыши ( если это многоугольник - точки вносятся последовательно,
      /// пока последняя точка не будет равняться первой
      /// </summary>
      public void EndDrawingFigures(PointF elocation)
      {
         bool drawn = false;
         if (SwitchConditions.LineDrawing)
         {
            if (EndAddingNewLine())
               drawn = true;
         }
         else if (SwitchConditions.RectangleDrawing)
         {
            if (EndAddingNewRectangle())
               drawn = true;
         }
         else if (SwitchConditions.EllipseDrawing)
         {
            if (EndAddingNewEllipse())
               drawn = true;
         }
         else if (SwitchConditions.PolygonDrawing)
         {
            if (EndAddingNewPolygon(elocation))
               drawn = true;
         }
         else if (SwitchConditions.PieDrawing)
         {
            if (EndAddingNewPie())
               drawn = true;
         }
         else if (SwitchConditions.ArcDrawing)
         {
            if (EndAddingNewArc())
               drawn = true;
         }

         if (Poryadok.Count > 0 && drawn)
         if (SwitchConditions.PolygonPressed == false)
         {
            SwitchConditions.SomeFigureSelected = true;
            Poryadok.Last().IsSelected = true;
            WriteAllFiguresToJournal();
            SetAllFiguresRefToCenter();
         }
      }
      
      private bool startChanging;
      private bool endChanging;
      private bool referenceChanging;
      private bool pointsChanging;
      private bool sidesChanging;
      private static bool angleChanging;
      static int pointIndex = 0;
      private PointF refPointCoords;
      private float angleWhileRotating;

      

      /// <summary>
      /// Подготовка к перемещению фигур
      /// </summary>
      public void DefineParamToChange(PointF elocation)
      {
         startChanging = false;
         endChanging = false;
         referenceChanging = false;
         pointsChanging = false;
         sidesChanging = false;
         AngleChanging = false;
         
         foreach (var item in Poryadok)
         {
            if (item.IsSelected)
            {
               if (item is MyLines){
                  if (isLineParamToChangeDefined(elocation, item)) 
                     break;
               }
               else if (item is MyRectangle){
                  if (isRectangleParamToChangeDefined(elocation, item))
                     break;
               }
               else if (item is MyPolygon){
                  if (isPolygonParamToChangeDefined(elocation, item))
                     break;
               }
               else if (item is MyPie){
                  if (isPieParamToChangeDefined(elocation, item))
                     break;
               }
               else if (item is MyArc){
                  if (isArcParamToChangeDefined(elocation, item))
                     break;
               }
               else if (item is MyEllipse){
                  if (isEllipseParamToChangeDefined(elocation, item))
                     break;
               }
            }
         }
      }

      private bool isEllipseParamToChangeDefined(PointF elocation, Figures item)
      {
         if (IsInsidePoint(elocation, item.End))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            endChanging = true;
            return true;
         }
         else if (IsInsidePoint(elocation, item.Start))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            startChanging = true;
            return true;
         }
         else if (((MyEllipse) item).InEllipse(elocation))
         {
            SwitchConditions.ChangeMoveCondition("moving");
            return true;
         }
         return false;
      }

      private bool isArcParamToChangeDefined(PointF elocation, Figures item)
      {
         if (IsInsidePoint(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            referenceChanging = true;
            return true;
         }
         else if (IsInsidePoint(elocation, item.Start))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            startChanging = true;
            return true;
         }
         else if (IsInsidePoint(elocation, item.End))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            endChanging = true;
            return true;
         }
         else if (InArrowArea(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            AngleChanging = true;
            refPointCoords = item.Reference;
            return true;
         }
         else if (((MyArc) item).InArc(elocation))
         {
            SwitchConditions.ChangeMoveCondition("moving");
            return true;
         }
         return false;
      }

      private bool isPieParamToChangeDefined(PointF elocation, Figures item)
      {
         if (IsInsidePoint(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            referenceChanging = true;
            return true;
         }
         //если попадаем в точку начала
         else if (IsInsidePoint(elocation, item.Start))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            startChanging = true;
            return true;
         }
         //если попадаем в точку конца
         else if (IsInsidePoint(elocation, item.End))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            endChanging = true;
            return true;
         }
         else if (InArrowArea(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            AngleChanging = true;
            refPointCoords = item.Reference;
            return true;
         }
         else if (((MyPie) item).InPie(elocation))
         {
            SwitchConditions.ChangeMoveCondition("moving");
            return true;
         }
         return false;
      }

      private bool isPolygonParamToChangeDefined(PointF elocation, Figures item)
      {
         var itemPol = ((MyPolygon) item);
         if (IsInsidePoint(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            referenceChanging = true;
            return true;
         }
         else if (GetOneOfPoints(elocation, item, itemPol.Points.Count()) != PointF.Empty)
         {
            SwitchConditions.ChangeMoveCondition("changing");
            pointsChanging = true;
            return true;
         }
         else if (InArrowArea(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            AngleChanging = true;
            refPointCoords = item.Reference;
            return true;
         }
         else if (((MyPolygon) item).InsideOfPolygon(elocation))
         {
            SwitchConditions.ChangeMoveCondition("moving");
            return true;
         }
         return false;
      }

      private bool isRectangleParamToChangeDefined(PointF elocation, Figures item)
      {
         var itemRec = ((MyRectangle) item);
         if (IsInsidePoint(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            referenceChanging = true;
            return true;
         }
         else if (GetOneOfPoints(elocation, item, itemRec.Points.Count()) != PointF.Empty)
         {
            SwitchConditions.ChangeMoveCondition("changing");
            pointsChanging = true;
            return true;
         }
         else if (GetOneOfSidePoints(elocation, item, itemRec.Points.Count()) != PointF.Empty)
         {
            SwitchConditions.ChangeMoveCondition("changing");
            sidesChanging = true;
            return true;
         }
         else if (InArrowArea(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            AngleChanging = true;
            refPointCoords = item.Reference;
            return true;
         }
         else if (itemRec.InsideOfRectangle(elocation))
         {
            SwitchConditions.ChangeMoveCondition("moving");
            return true;
         }
         return false;
      }

      private bool isLineParamToChangeDefined(PointF elocation, Figures item)
      {
         if (IsInsidePoint(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            referenceChanging = true;
            return true;
         }
         //если попадаем в точку начала
         else if (IsInsidePoint(elocation, item.Start))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            startChanging = true;
            return true;
         }
         //если попадаем в точку конца
         else if (IsInsidePoint(elocation, item.End))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            endChanging = true;
            return true;
         }
         //если попадаем внутрь линии
         else if (((MyLines) item).InsideOfLine(elocation))
         {
            SwitchConditions.ChangeMoveCondition("moving");
            return true;
         }
         else if (InArrowArea(elocation, item.Reference))
         {
            SwitchConditions.ChangeMoveCondition("changing");
            AngleChanging = true;
            refPointCoords = item.Reference;
            return true;
         }
         return false;
      }

      /// <summary>
      /// Сохраняем точку  для перемещения и угол 
      /// </summary>
      public void SaveParamsToChangeMove(PointF elocation )
      {
         pointToMove = elocation;
         float x1 = refPointCoords.X, y1 = refPointCoords.Y;
         float x2 = elocation.X, y2 = elocation.Y;
         angleWhileRotating = (float)(Math.Atan2(y1 - y2, x1 - x2) / Math.PI * 180);
         // Без этого диапазон от 0...180 и -1...-180
         angleWhileRotating = (angleWhileRotating < 0) ? angleWhileRotating + 360 : angleWhileRotating;   

      }
      
      /// <summary>
      /// Перемещение/изменение выделенной фигуры зажатой мышкой 
      /// </summary>
      public void MovingChangingFigures(PointF elocation)
      {
        
         for (int i = Poryadok.Count - 1; i >= 0; --i)
         {
            Figures item = Poryadok[i];
            if (item.IsSelected)
            {
               if (item is MyLines)
               {
                  if (SwitchConditions.ChangingFigure)
                  {
                     if (referenceChanging)
                        GetNewReferencePoint(elocation, item);
                     else if (startChanging)
                        GetNewStartPoint(elocation, item);
                     else if (endChanging)
                        GetNewEndPoint(elocation, item);
                     else if (AngleChanging)
                     {
                        SetNewAngle(elocation, item);
                        item.End = ((MyLines)item).ModifyPointsLine(((MyLines)item).End);
                        item.Start = ((MyLines)item).ModifyPointsLine(((MyLines)item).Start);
                     }
                     pointToMove = elocation;
                  }
                  else if (SwitchConditions.Moving)
                  {
                     GetNewPointsWhileMoving(item, elocation);
                  }
                  ((MyLines) item).ConvertPointsOfLineWhileDrawing();
               }
               else if (item is MyRectangle)
               {
                  var itemRec = (MyRectangle) item;
                  if (SwitchConditions.ChangingFigure)
                  {
                     if (referenceChanging)
                        GetNewReferencePoint(elocation, itemRec);
                     if (pointsChanging)
                     {
                        float lAngle = itemRec.Angle;
                        itemRec.Angle = 0;
                        itemRec.RotateRectangle();
                        itemRec.GetNewRectanglePoints(elocation, pointIndex);
                        itemRec.Angle = lAngle;
                        itemRec.RotateRectangle();
                     }
                     else if (sidesChanging)
                     {
                        float lAngle = itemRec.Angle;
                        itemRec.Angle = 0;
                        itemRec.RotateRectangle();
                        itemRec.ChangeRectangleSides(elocation, pointIndex, lAngle);
                        itemRec.Angle = lAngle;
                        itemRec.RotateRectangle();
                     }
                     else if (AngleChanging)
                     {
                        //item.PrevAngle = 0;
                        SetNewAngle(elocation, item);
                        ((MyRectangle)item).RotateRectangle();
                     }
                  }
                  else if (SwitchConditions.Moving)
                  {
                     GetNewPointsWhileMoving(item, elocation);
                     itemRec.BuildRectangleWhileMoving(elocation, pointToMove);
                  }
                  pointToMove = elocation;
               }
               else if (item is MyPolygon)
               {
                  var itemPol = ((MyPolygon)item);
                  if (SwitchConditions.ChangingFigure)
                  {
                     if (referenceChanging)
                        GetNewReferencePoint(elocation, item);
                     else if (pointsChanging)
                     {
                        itemPol.Points[pointIndex] = elocation;
                     }
                     else if (AngleChanging)
                     {
                        SetNewAngle(elocation, item);
                        ((MyPolygon) item).PrevAngle = 0;
                        ((MyPolygon)item).RotatePolygon();
                     }
                  }
                  else if (SwitchConditions.Moving)
                  {
                     GetNewPointsWhileMoving(item, elocation);
                     itemPol.BuildPolygonWhileMoving(elocation, pointToMove);
                  }
                  pointToMove = elocation;
               }
               else if (item is MyPie)
               {
                  if (SwitchConditions.ChangingFigure)
                  {
                     if (referenceChanging)
                        GetNewReferencePoint(elocation, item);
                     else if (startChanging)
                     {
                        GetNewStartPoint(elocation, item);
                        ((MyPie)item).StartAngle = ((MyPie)item).GetStartAngle();
                     }
                     else if (endChanging)
                     {
                        GetNewEndPoint(elocation, item);
                        ((MyPie)item).StartAngle = ((MyPie)item).GetStartAngle();
                     }
                     else if (AngleChanging)
                     {
                        SetNewAngle(elocation, item);
                        ((MyPie)item).PrevAngle = 0;
                        ((MyPie)item).ModifyPointsPieArcWithAngle();
                        ((MyPie)item).StartAngle = ((MyPie)item).GetStartAngle();
                     }
                     pointToMove = elocation;
                  }
                  else if (SwitchConditions.Moving)
                  {
                     GetNewPointsWhileMoving(item, elocation);
                  }
               }
               else if (item is MyArc)
               {
                  if (SwitchConditions.ChangingFigure)
                  {
                     if (referenceChanging)
                        GetNewReferencePoint(elocation, item);
                     else if (startChanging)
                     {
                        GetNewStartPoint(elocation, item);
                        ((MyArc)item).StartAngle = ((MyArc)item).GetStartAngle();
                     }
                     else if (endChanging)
                     {
                        GetNewEndPoint(elocation, item);
                        ((MyArc)item).StartAngle = ((MyArc)item).GetStartAngle();
                     }
                     else if (AngleChanging)
                     {
                        SetNewAngle(elocation, item);
                        ((MyArc) item).PrevAngle = 0;
                        ((MyArc)item).ModifyPointsPieArcWithAngle();
                        ((MyArc)item).StartAngle = ((MyArc)item).GetStartAngle();
                     }
                     pointToMove = elocation;
                  }
                  else if (SwitchConditions.Moving)
                  {
                     GetNewPointsWhileMoving(item, elocation);
                  }
               }
               else if (item is MyEllipse)
               {
                  if (SwitchConditions.ChangingFigure)
                  {
                     if (endChanging)
                        GetNewEndPoint(elocation, item);
                     if (startChanging)
                        GetNewStartPoint(elocation, item); 
                     pointToMove = elocation;
                  }
                  else if (SwitchConditions.Moving)
                  {
                     GetNewPointsWhileMoving(item, elocation);
                  }
               }

            }
         }
      }

      private void SetNewAngle(PointF elocation, Figures item)
      {
         float x1 = item.Reference.X, y1 = item.Reference.Y;
         float x2 = elocation.X, y2 = elocation.Y;
         float newAngleWhileRotating = (float) (Math.Atan2(y1 - y2, x1 - x2)/Constants.GRAD_TO_RAD);
         float difference = newAngleWhileRotating - angleWhileRotating;
         // Без этого диапазон от 0...180 и -1...-180
         angleWhileRotating = (newAngleWhileRotating < 0) ? newAngleWhileRotating + 360 : newAngleWhileRotating;
         if (!(item is MyRectangle))
            item.Angle = difference;
         else if (item is MyRectangle)
         {
            item.Angle += difference;
         }
      }

      /// <summary>
      /// Заканчиваем перемещение фигуры мышкой
      /// </summary>
      public void EndMovingChangingFigures()
      {
         //SwitchConditions.SomeFigureSelected = (!SwitchConditions.SomeFigureSelected);
         SwitchConditions.Moving = false;
         SwitchConditions.ChangingFigure = false;
      }


      /// <summary>
      /// Выделение фигуры щелчком мыши
      /// </summary>
      public bool SelectFigure(PointF elocation)
      {
         //цикл для прохода по всем нарисованным фигурам, при каждой итерации цикла просматривается последний элемент коллекции
         //среди всех фигур, если в точке нажатия находится фигура, добавленная в коллекцию не последней, 
         //цикл продолжает просматривать далее коллекцию фигур на предмет обнаружения фигуры в данной точке
         for (int i = Poryadok.Count - 1; i >= 0; --i)
         {
            Figures item = Poryadok[i];

            if (item is MyLines){
               if (((MyLines)item).InsideOfLine(elocation)){
                  UnselectAllItems();
                  item.IsSelected = true;
                  return true;
               }
            }
            else if (item is MyPie){
               if (((MyPie)item).InPie(elocation)){
                  UnselectAllItems();
                  item.IsSelected = true;
                  return true; 
               }
            }
            else if (item is MyArc){
               if (((MyArc)item).InArc(elocation)){
                  UnselectAllItems();
                  item.IsSelected = true;
                  return true; 
               }
            }
            else if (item is MyEllipse){
               if (((MyEllipse)item).InEllipse(elocation)){
                  UnselectAllItems();
                  item.IsSelected = true;
                  return true; 
               }
            }
            else if (item is MyRectangle){
               if (((MyRectangle)item).InsideOfRectangle(elocation)){
                  UnselectAllItems();
                  item.IsSelected = true;
                  return true; 
               }
            }
            else if (item is MyPolygon){
               if (((MyPolygon)item).InsideOfPolygon(elocation)){
                  UnselectAllItems();
                  item.IsSelected = true;
                  return true; 
               }
            }
         }
         return false;
      }


      /// <summary>
      /// Заполнение коллекций загруженными элементами
      /// </summary>
      public void FillingCollectionsWithLoaded(dynamic allFigures)
      {
         poryadok.AddRange(allFigures);

         foreach (var item in poryadok)
         {
            if (item is MyLines)
            {
               CollectionLines.Add((MyLines) item);
               PanelProperties panel_lines = new PanelProperties(formMain,
                  CollectionLines.Last());
               formMain.GetPanel_properties_lines.Controls.Add(panel_lines);
            }
            else if (item is MyRectangle)
            {
               CollectionRectangles.Add((MyRectangle) item);
               PanelProperties panel_rectangles = new PanelProperties(formMain,
                  CollectionRectangles.Last());
               formMain.GetPanel_properties_rectangles.Controls.Add(panel_rectangles);
            }
            else if (item is MyArc)
            {
               CollectionArcs.Add((MyArc) item);
               PanelProperties panel_arcs = new PanelProperties(formMain,
                  CollectionArcs.Last());
               formMain.GetPanel_properties_arcs.Controls.Add(panel_arcs);
            }
            else if (item is MyEllipse)
            {
               CollectionEllipses.Add((MyEllipse) item);
               PanelProperties panel_ellipses = new PanelProperties(formMain,
                  CollectionEllipses.Last());
               formMain.GetPanel_properties_ellipses.Controls.Add(panel_ellipses);
            }
            else if (item is MyPie)
            {
               CollectionPies.Add((MyPie) item);
               PanelProperties panel_pies = new PanelProperties(formMain,
                  CollectionPies.Last());
               formMain.GetPanel_properties_pies.Controls.Add(panel_pies);
            }
            else if (item is MyPolygon)
            {
               CollectionPolygons.Add((MyPolygon) item);
               PanelProperties panel_polygons = new PanelProperties(formMain,
                  CollectionPolygons.Last());
               formMain.GetPanel_properties_polygons.Controls.Add(panel_polygons);
            }
         }
      }
      
      /// <summary>
      /// Очистка коллекций
      /// </summary>
      public void ClearAllCollections()
      {
         Poryadok.Clear();
         CollectionEllipses.Clear();
         CollectionRectangles.Clear();
         CollectionArcs.Clear();
         CollectionLines.Clear();
         CollectionPies.Clear();
         CollectionPolygons.Clear();
      }

      /// <summary>
      /// Запись событий в журнал
      /// </summary>
      private string ReportForJournal()
      {
         string report = "";
         report = "\r\n" + DateTime.Now + "\r\n" + Journal.Last() + "\r\n";
         return report;
      }

#endregion


      #region Вспомогательные методы

      

      
      /// <summary>
      /// Новая конечная точка после изменения
      /// </summary>
      private void GetNewEndPoint(PointF elocation, Figures item)
      {
         item.End = new PointF(item.End.X - (pointToMove.X - elocation.X),
            item.End.Y - (pointToMove.Y - elocation.Y));
      }

      /// <summary>
      /// Новая начальная точка после изменения
      /// </summary>
      private void GetNewStartPoint(PointF elocation, Figures item)
      {
         item.Start = new PointF(item.Start.X - (pointToMove.X - elocation.X),
            item.Start.Y - (pointToMove.Y - elocation.Y));
      }

      /// <summary>
      /// Новая точка опоры после изменения
      /// </summary>
      private void GetNewReferencePoint(PointF elocation, Figures item)
      {
         item.Reference = new PointF(item.Reference.X - (pointToMove.X - elocation.X),
            item.Reference.Y - (pointToMove.Y - elocation.Y));
      }
      
      /// <summary>
      /// Метод проверяет попадание указателем мыши в область маркера той или иной точки фигуры
      /// </summary>
      private static bool IsInsidePoint(PointF elocation, PointF point)
      {
         return elocation.X > point.X - 5 && elocation.X < point.X + 5 &&
                elocation.Y > point.Y - 5 && elocation.Y < point.Y + 5;
      }

      /// <summary>
      /// Метод проверяет попадание указателем мыши в область маркера одной из точек 
      /// образующий прямоугольник\многоугольник
      /// </summary>
      private static PointF GetOneOfPoints(PointF elocation, dynamic item, int pointsCount)
      {
         PointF point = new PointF();
         for (int i = 0; i < pointsCount; i++)
         {
            if (elocation.X > item.Points[i].X - 5 && elocation.X < item.Points[i].X + 5 &&
            elocation.Y > item.Points[i].Y - 5 && elocation.Y < item.Points[i].Y + 5)
            {
               point = item.Points[i];
               pointIndex = i;
               break;
            }
         }
         return point;
      }

      /// <summary>
      /// Метод возвращает точку, лежащую между двумя вершинами прямоугольника
      /// </summary>
      private static PointF GetOneOfSidePoints(PointF elocation, dynamic item, int pointsCount)
      {
         PointF point = new PointF();
         for (int i = 0; i < pointsCount; i++)
         {
            PointF sidePoint;
            if (i < 3)
            {
               sidePoint = new PointF((item.Points[i].X + item.Points[i + 1].X) / 2,
                  (item.Points[i].Y + item.Points[i + 1].Y) / 2);
            }
            else
            {
               sidePoint = new PointF((item.Points[i].X + item.Points[i - 3].X) / 2,
                  (item.Points[i].Y + item.Points[i - 3].Y) / 2);
            }

            if (elocation.X > sidePoint.X - 5 && elocation.X < sidePoint.X + 5 &&
                elocation.Y > sidePoint.Y - 5 && elocation.Y < sidePoint.Y + 5)
            {
               point = item.Points[i];
               pointIndex = i;
               break;
            }
         }
         return point;
      }

      /// <summary>
      /// Добавление записи в журнал о последней нарисованной фигуре
      /// </summary>
      private void WriteAllFiguresToJournal()
      {
         if (SwitchConditions.ArcDrawing)
            Journal.Add(CollectionArcs.Last().WriteToJournal());
         else if (SwitchConditions.PieDrawing)
            Journal.Add(CollectionPies.Last().WriteToJournal());
         else if (SwitchConditions.PolygonDrawing)
            Journal.Add(CollectionPolygons.Last().WriteToJournal());
         else if (SwitchConditions.EllipseDrawing) 
            Journal.Add(CollectionEllipses.Last().WriteToJournal());
         else if (SwitchConditions.RectangleDrawing)
            Journal.Add(CollectionRectangles.Last().WriteToJournal());
         else if (SwitchConditions.LineDrawing)
            Journal.Add(CollectionLines.Last().WriteToJournal());

         formMain.GetJournalRichTextBox.Text += ReportForJournal();
         SendMessage(formMain.GetJournalRichTextBox.Handle, 0x115 /*WM_VSCROLL*/, 0x07 /*SB_BOTTOM*/, 0);
      }

      private void StartAddingNewArc(PointF elocation)
      {
         CollectionArcs.Add(new MyArc("Дуга", elocation, elocation, 0.0,
            (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeContourColor.BackColor,
            elocation, 10, 10, false, SwitchConditions.MyDashStyle));
         Poryadok.Add(CollectionArcs.Last());
      }

      private void StartAddingNewPie(PointF elocation)
      {
         if (SwitchConditions.Filled)
            CollectionPies.Add(new MyPie("Сектор", elocation, elocation, 0.0,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               elocation, 10, 10, 0, 0, true, false, formMain.GetButtonChangeContourColor.BackColor, false, SwitchConditions.MyDashStyle,
                SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (!SwitchConditions.Filled && !SwitchConditions.Contoured)
            CollectionPies.Add(new MyPie("Сектор", elocation, elocation, 0.0,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               elocation, 10, 10, 0, 0, false, false, formMain.GetButtonChangeContourColor.BackColor,
               false, SwitchConditions.MyDashStyle, SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (SwitchConditions.Contoured)
            CollectionPies.Add(new MyPie("Сектор", elocation, elocation, 0.0,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               elocation, 10, 10, 0, 0, false, true, formMain.GetButtonChangeContourColor.BackColor,
               false, SwitchConditions.MyDashStyle, SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));

         Poryadok.Add(CollectionPies.Last());
      }

      private void StartAddingNewPolygon(PointF elocation)
      {
         if (SwitchConditions.Filled)
            CollectionPolygons.Add(new MyPolygon("Многоугольник", elocation, elocation, 0.0f,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               new PointF[] { elocation, }, true, false, formMain.GetButtonChangeContourColor.BackColor, false, SwitchConditions.MyDashStyle,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (!SwitchConditions.Filled && !SwitchConditions.Contoured)
            CollectionPolygons.Add(new MyPolygon("Многоугольник", elocation, elocation, 0.0f,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               new PointF[] { elocation, }, false, false, formMain.GetButtonChangeContourColor.BackColor,
               false, SwitchConditions.MyDashStyle, SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (SwitchConditions.Contoured)
            CollectionPolygons.Add(new MyPolygon("Многоугольник", elocation, elocation, 0.0f,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               new PointF[] { elocation, }, false, true, formMain.GetButtonChangeContourColor.BackColor,
               false, SwitchConditions.MyDashStyle, SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));

         Poryadok.Add(CollectionPolygons.Last());
         SwitchConditions.PolygonPressed = true;

         if (CollectionPolygons.Last().Points.Count() == 1)
         {
            PointF[] PrevPointsArray = CollectionPolygons.Last().Points;
            PointF[] NewPointsArray = new PointF[PrevPointsArray.Length + 1];
            for (int j = 0; j < PrevPointsArray.Length; ++j)
               NewPointsArray[j] = PrevPointsArray[j];
            NewPointsArray[NewPointsArray.Length - 1] = elocation;
            CollectionPolygons.Last().Points = NewPointsArray;
            CollectionPolygons.Last().Points[CollectionPolygons.Last().Points.Length - 1] = elocation;
         }
      }

      private void StartAddingNewEllipse(PointF elocation)
      {
         if (SwitchConditions.Filled)
            CollectionEllipses.Add(new MyEllipse("Окружность", elocation,
               elocation, (float)formMain.GetNumericUpDown_Width.Value,
               formMain.GetButtonChangeColor.BackColor,
               elocation, 0, 0, true, false, formMain.GetButtonChangeContourColor.BackColor,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyDashStyle, SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (!SwitchConditions.Filled && !SwitchConditions.Contoured)
            CollectionEllipses.Add(new MyEllipse("Окружность",
               elocation, elocation, (float)formMain.GetNumericUpDown_Width.Value,
               formMain.GetButtonChangeColor.BackColor, elocation, 0,
               0, false, false, formMain.GetButtonChangeContourColor.BackColor, false, SwitchConditions.MyDashStyle,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (SwitchConditions.Contoured)
            CollectionEllipses.Add(new MyEllipse("Окружность", elocation, elocation,
               (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeColor.BackColor,
               elocation, 0, 0, false, true, formMain.GetButtonChangeContourColor.BackColor, false, SwitchConditions.MyDashStyle,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));

         Poryadok.Add(CollectionEllipses.Last());
      }

      private void StartAddingNewRectangle(PointF elocation)
      {
         if (SwitchConditions.Filled)
            CollectionRectangles.Add(new MyRectangle("Прямоугольник",
               elocation, elocation, 0.0f, (float)formMain.GetNumericUpDown_Width.Value,
               formMain.GetButtonChangeColor.BackColor, elocation,
               0, 0, true, false, formMain.GetButtonChangeContourColor.BackColor,
               new PointF[] { elocation, elocation, elocation, elocation }, false, SwitchConditions.MyDashStyle,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (!SwitchConditions.Filled && !SwitchConditions.Contoured)
            CollectionRectangles.Add(new MyRectangle("Прямоугольник",
               elocation, elocation, 0.0f, (float)formMain.GetNumericUpDown_Width.Value,
               formMain.GetButtonChangeColor.BackColor, elocation, 0, 0, false, false,
               formMain.GetButtonChangeContourColor.BackColor,
               new PointF[] { elocation, elocation, elocation, elocation }, false, SwitchConditions.MyDashStyle,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));
         else if (SwitchConditions.Contoured)
            CollectionRectangles.Add(new MyRectangle("Прямоугольник",
               elocation, elocation, 0.0f, (float)formMain.GetNumericUpDown_Width.Value,
               formMain.GetButtonChangeColor.BackColor, elocation, 0, 0, false, true,
               formMain.GetButtonChangeContourColor.BackColor,
               new PointF[] { elocation, elocation, elocation, elocation }, false, SwitchConditions.MyDashStyle,
               SwitchConditions.IsHatchStyleOn, SwitchConditions.MyHatchStyle));

         Poryadok.Add(CollectionRectangles.Last());
         SwitchConditions.RectanglePressed = true;
      }

      private void StartAddingNewLine(PointF elocation)
      {
         //добавляем в коллекцию новую линию
         CollectionLines.Add(new MyLines("Линия", elocation, elocation, 0,
            (float)formMain.GetNumericUpDown_Width.Value, formMain.GetButtonChangeContourColor.BackColor,
            elocation, 0, new PointF[] { elocation, elocation, elocation, elocation }, false, SwitchConditions.MyDashStyle));
         //Добавляем в коллекцию фигур новую линию
         Poryadok.Add(CollectionLines.Last());
         SwitchConditions.LinePressed = true;
      }

      private void SetAllFiguresRefToCenter()
      {
         Poryadok.Last().SetRefPointToCenter();
         if (CollectionPolygons.Count != 0)
            CollectionPolygons.Last().SetRefPointToCenter();
         if (CollectionRectangles.Count != 0)
            CollectionRectangles.Last().SetRefPointToCenter();
      }

      private bool EndAddingNewArc()
      {
         if (Poryadok.Last().End != Poryadok.Last().Start)
         {
            PanelProperties panel_arcs = new PanelProperties(formMain, CollectionArcs.Last());
            formMain.GetPanel_properties_arcs.Controls.Add(panel_arcs);
            return true;
         }
         else
         {
            CollectionArcs.RemoveAt(CollectionArcs.Count - 1);
            Poryadok.RemoveAt(Poryadok.Count - 1);
            return false;
         }
      }

      private bool EndAddingNewPie()
      {
         if (Poryadok.Last().End != Poryadok.Last().Start)
         {
            PanelProperties panel_pies = new PanelProperties(formMain,
               CollectionPies.Last());
            formMain.GetPanel_properties_pies.Controls.Add(panel_pies);
            return true;
         }
         else
         {
            CollectionPies.RemoveAt(CollectionPies.Count - 1);
            Poryadok.RemoveAt(Poryadok.Count - 1);
            return false;
         }
      }

      private bool EndAddingNewPolygon(PointF elocation)
      {
         if (SwitchConditions.PolygonPressed)
         {
            //инициализируем новые массивы точек для координат многоугольника
            PointF[] PrevPointsArray = CollectionPolygons.Last().Points;

            if (Math.Abs(CollectionPolygons.Last().Points[0].X - elocation.X) >= 10 ||
                Math.Abs(CollectionPolygons.Last().Points[0].Y - elocation.Y) >= 10)
            {
               PointF[] NewPointsArray = new PointF[PrevPointsArray.Length + 1];
               for (int j = 0; j < PrevPointsArray.Length; ++j)
                  NewPointsArray[j] = PrevPointsArray[j];
               NewPointsArray[NewPointsArray.Length - 1] = elocation;
               CollectionPolygons.Last().Points = NewPointsArray;

               if (CollectionPolygons.Last().Points[CollectionPolygons.Last().Points.Length - 1] !=
                   CollectionPolygons.Last().Points[CollectionPolygons.Last().Points.Length - 2])
               {
                  //удаляем, если равна предыдущей
                  PointF[] PrevPointsArray3 = CollectionPolygons.Last().Points;
                  PointF[] NewPointsArray3 = new PointF[PrevPointsArray3.Length - 1];
                  for (int j = 0; j < PrevPointsArray3.Length - 1; ++j)
                     NewPointsArray3[j] = PrevPointsArray3[j];
                  CollectionPolygons.Last().Points = NewPointsArray3;
               }
            }

            else if (CollectionPolygons.Last().Points.Length > 1)
            {
               if (CollectionPolygons.Last().Points.Length == 2)
               {
                  SwitchConditions.PolygonPressed = false;
                  CollectionPolygons.RemoveAt(CollectionPolygons.Count - 1);
                  Poryadok.RemoveAt(Poryadok.Count - 1);
                  return false;
               }
               else
               {
                  SwitchConditions.PolygonPressed = false;
                  //удаляем последнюю точку
                  PointF[] PrevPointsArray2 = CollectionPolygons.Last().Points;
                  PointF[] NewPointsArray2 = new PointF[PrevPointsArray2.Length - 1];
                  for (int j = 0; j < PrevPointsArray2.Length - 1; ++j)
                     NewPointsArray2[j] = PrevPointsArray2[j];
                  CollectionPolygons.Last().Points = NewPointsArray2;

                  PanelProperties panel_polygons = new PanelProperties(formMain,
                     CollectionPolygons.Last());
                  formMain.GetPanel_properties_polygons.Controls.Add(panel_polygons);
               }
            }
            return true;
         }
         return false;
      }

      private bool EndAddingNewEllipse()
      {
         if (Poryadok.Last().End != Poryadok.Last().Start)
         {
            PanelProperties panel_ellipses = new PanelProperties(formMain,
               CollectionEllipses.Last());
            formMain.GetPanel_properties_ellipses.Controls.Add(panel_ellipses);
            return true;
         }
         else
         {
            CollectionEllipses.RemoveAt(CollectionEllipses.Count - 1);
            Poryadok.RemoveAt(Poryadok.Count - 1);
            return false;
         }
      }

      private bool EndAddingNewRectangle()
      {
         if (Poryadok.Last().End != Poryadok.Last().Start)
         {
            SwitchConditions.RectanglePressed = false;
            PanelProperties panel_rectangles = new PanelProperties(formMain,
               CollectionRectangles.Last());
            formMain.GetPanel_properties_rectangles.Controls.Add(panel_rectangles);
            return true;
         }
         else
         {
            CollectionRectangles.RemoveAt(CollectionRectangles.Count - 1);
            Poryadok.RemoveAt(Poryadok.Count - 1);
            SwitchConditions.RectanglePressed = false;
            return false;
         }
      }

      private bool EndAddingNewLine()
      {
         //если точки начала и конца не совпадают  - линия счиатется нарисованной
         if ((Poryadok.Last().Start != Poryadok.Last().End) && CollectionLines.Count != 0)
         {
            SwitchConditions.LinePressed = false;
            // добавляем на панель св-в линий панель с параметрами этой линии
            PanelProperties panel_lines = new PanelProperties(formMain, CollectionLines.Last());
            formMain.GetPanel_properties_lines.Controls.Add(panel_lines);
            return true;
         }
         //если это был случайный клик - просто не заносим никуда
         else
         {
            CollectionLines.RemoveAt(CollectionLines.Count - 1);
            Poryadok.RemoveAt(Poryadok.Count - 1);
            SwitchConditions.LinePressed = false;
            return false;
         }
      }

      /// <summary>
      /// Перемещение фигуры ( для многоугольника и прямоугольника точка, относительно которой фигуры перемещаются
      /// меняется после их перестроения)
      /// </summary>
      private void GetNewPointsWhileMoving(Figures item, PointF elocation)
      { 
         item.Start = new PointF(item.Start.X - (pointToMove.X - elocation.X),
            item.Start.Y - (pointToMove.Y - elocation.Y));
         if (!(item is MyPolygon || item is MyRectangle))
         {
            item.End = new PointF(item.End.X - (pointToMove.X - elocation.X),
               item.End.Y - (pointToMove.Y - elocation.Y));
            pointToMove = elocation;
         }
      }
      
      /// <summary>
      /// Снятие выделения со всех фигур
      /// </summary>
      public void UnselectAllItems()
      {
         foreach (var item in Poryadok)
         {
            item.IsSelected = false;
         }
      }


      /// <summary>
      /// Метод определяет, находится ли мышь в области, в которой должна появляться стрелка для вращения фигуры
      /// </summary>
      public bool InArrowArea(PointF elocation, PointF pointRef)
      {
         if (Math.Abs(pointRef.X - elocation.X) < 40 && Math.Abs(pointRef.Y - elocation.Y) < 40)
            return true;
         else return false;
      }


      /// <summary>
      /// Перемещение фигуры стрелками, когда она выделена
      /// </summary>
      public void MoveFiguresByArrows(int x, int y)
      {
         foreach (var figure in Poryadok)
         {
            if (!figure.IsSelected)
               continue;
            
            if (figure is MyLines)
            {
               ((MyLines)figure).LineHorVerMoving(x, y);
               break;
            }
            else if (figure is MyRectangle)
            {
               ((MyRectangle) figure).RectangleHorVerMoving(x, y);
               break;
            }
            else if (figure is MyPolygon)
            {
               ((MyPolygon)figure).PolygonHorVerMoving(x, y);
               break;
            }
            else if (figure is MyArc)
            {
               ((MyArc)figure).ArcHorVerMoving(x, y);
               break;
            }
            else if (figure is MyPie)
            {
               ((MyPie)figure).PieHorVerMoving(x, y);
               break;
            }
            else if (figure is MyEllipse)
            {
               ((MyEllipse)figure).EllipseHorVerMoving(x, y);
               break;
            }
            
         }
      }

#endregion



   }
}
   