﻿using System;
using System.Drawing;
using System.Linq;

namespace EVGR
{
   internal static class CommonMethods
   {

      public static void ModifyPieArcPointsWithAngle
         (ref PointF start,  ref PointF end, ref float prevAngle, PointF reference, float angle)
      {
         float x = start.X - reference.X;
         float y = start.Y - reference.Y;
         start.X = (float) (x*Math.Cos((angle - prevAngle)/180.0*Math.PI) -
                             y*Math.Sin((angle - prevAngle)/180.0*Math.PI)) + reference.X;
         start.Y = (float) (x*Math.Sin((angle - prevAngle)/180.0*Math.PI) +
                             y*Math.Cos((angle - prevAngle)/180.0*Math.PI)) + reference.Y;
         float x2 = end.X - reference.X;
         float y2 = end.Y - reference.Y;
         end.X = (float) (x2*Math.Cos((angle - prevAngle)/180.0*Math.PI) -
                           y2*Math.Sin((angle - prevAngle)/180.0*Math.PI)) + reference.X;
         end.Y = (float) (x2*Math.Sin((angle - prevAngle)/180.0*Math.PI) +
                           y2*Math.Cos((angle - prevAngle)/180.0*Math.PI)) + reference.Y;
         prevAngle = angle;
      }

      public static void ModifyPieArcEndWithStartAngle
         (ref PointF end, ref float prevStartAngle, PointF start, float startAngle)
      {
         float x2 = end.X - start.X;
         float y2 = end.Y - start.Y;
         end.X = (float) (x2*Math.Cos((startAngle - prevStartAngle)/180.0*Math.PI) -
                           y2*Math.Sin((startAngle - prevStartAngle)/180.0*Math.PI)) + start.X;
         end.Y = (float) (x2*Math.Sin((startAngle - prevStartAngle)/180.0*Math.PI) +
                           y2*Math.Cos((startAngle - prevStartAngle)/180.0*Math.PI)) + start.Y;
         prevStartAngle = startAngle;
      }

      public static void ModifyPieArcEndWithSweepAngle
         (  ref PointF end, ref float prevSweepAngle, PointF start, float sweepAngle)
      {
         float x2 = end.X - start.X;
         float y2 = end.Y - start.Y;
         end.X = (float) (x2*Math.Cos((sweepAngle - prevSweepAngle)/2/180.0*Math.PI) -
                           y2*Math.Sin((sweepAngle - prevSweepAngle)/2/180.0*Math.PI)) + start.X;
         end.Y = (float) (x2*Math.Sin((sweepAngle - prevSweepAngle)/2/180.0*Math.PI) +
                           y2*Math.Cos((sweepAngle - prevSweepAngle)/2/180.0*Math.PI)) + start.Y;
         prevSweepAngle = sweepAngle;
      }

      public static float GetStartAngle
         (PointF start,  PointF end, float sweepAngle)
      {
         double x1 = start.X;
         double y1 = start.Y;
         double x2 = end.X;
         double y2 = end.Y;
         return (float) (((Math.Atan2(y2 - y1, x2 - x1)*180)/Math.PI) - sweepAngle/2);
      }

      public static void RotatePolygonOrLine
         (ref PointF[] points, ref float prevAngle,  PointF reference,  float angle)
      {
         PointF[] ConvertedPoints = new PointF[points.Count()];
         for (int i = 0; i < points.Count(); i++)
         {
            float x = (points[i].X - reference.X);
            float y = (points[i].Y - reference.Y);
            ConvertedPoints[i].X = (float) (x*Math.Cos((angle - prevAngle)/180.0*Math.PI) -
                                            y*Math.Sin((angle - prevAngle)/180.0*Math.PI)) + reference.X;
            ConvertedPoints[i].Y = (float) (x*Math.Sin((angle - prevAngle)/180.0*Math.PI) +
                                            y*Math.Cos((angle - prevAngle)/180.0*Math.PI)) + reference.Y;
         }
         prevAngle = angle;
         points = ConvertedPoints;
      }

      public static bool InsideOfPolygonLineRectangle
         (PointF[] points, PointF location)
      {
         int intersectionsNum = 0;
         int prevPoint = points.Count() - 1;
         bool prevUnder = points[prevPoint].Y < location.Y;
         for (int u = 0; u < points.Count(); ++u)
         {
            bool cur_under = points[u].Y < location.Y;
            float ax = points[prevPoint].X - location.X;
            float ay = points[prevPoint].Y - location.Y;
            float bx = points[u].X - location.X;
            float by = points[u].Y - location.Y;

            float t = (ax*(by - ay) - ay*(bx - ax));

            if (cur_under && !prevUnder)
            {
               if (t > 0)
                  intersectionsNum++;
            }
            if (!cur_under && prevUnder)
            {
               if (t < 0)
                  intersectionsNum++;
            }
            prevPoint = u;
            prevUnder = cur_under;
         }
         if ((intersectionsNum & 1) != 0)
            return true;
         else return false;
      }

      public static float GetDiametrOfPieArc(PointF start,  PointF end)
      {
         return (float) (2.0*Math.Sqrt((start.X - end.X)*(start.X - end.X) +
                                       (start.Y - end.Y)*(start.Y - end.Y)));
      }


   }
}
