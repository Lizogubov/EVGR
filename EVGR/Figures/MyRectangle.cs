﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace EVGR
{
   [Serializable]
   public class MyRectangle : Figures
   {
      private PointF[] points;

      public PointF[] Points
      {
         get { return points; }
         set { points = value; }
      }

      public override PointF Start
      {
         get { return new PointF(start.X, start.Y); }
         set { start = value; }
      }

      public override PointF End
      {
         get { return new PointF(end.X, end.Y); }
         set { end = value; }
      }

      public MyRectangle(string name, PointF start, PointF reference, float angle, float penThickness,
         Color colorPen, PointF end, int width, int height, bool filling, bool contoured, Color colorContour,
         PointF[] points, bool selected, DashStyle dashStyle, bool hatchStyleOn, HatchStyle hatchStyle)
      {
         this.points = points;
         this.colorContour = colorContour;
         this.contoured = contoured;
         this.filled = filling;
         this.width = width;
         this.height = height;
         this.name = name;
         this.start = start;
         this.reference = reference;
         this.angle = angle;
         this.colorFilling = colorPen;
         this.penThickness = penThickness;
         this.end = end;
         this.isSelected = selected;
         this.dashStyle = dashStyle;
         this.hatchStyleOn = hatchStyleOn;
         this.hatchStyle = hatchStyle;
      }

      protected override void SetRefPointToCenter()
      {
         PointF CenterMass = new Point(0, 0);
         for (int i = 0; i < points.Count(); i++)
         {
            //находим сумму координат между точками многоугольника, последовательно
            CenterMass.X += points[i].X;
            CenterMass.Y += points[i].Y;
         }
         Reference = new PointF(CenterMass.X / points.Count(), CenterMass.Y / points.Count());
      }

      public bool InsideOfRectangle(PointF location)
      {
         return CommonMethods.InsideOfPolygonLineRectangle(points, location);
      }

      public string WriteToJournal()
      {
         string str = Name;
         if (!Filled)
            str += " Толщина линии: " + PenThickness;

         str += " Точка начала: " + Start + " Точка опоры: " + Reference + " Цвет: " +
                ColorFilling + " Точка конца: " + End + " Угол опоры: " +
                Angle + " Ширина: " + Math.Abs(Width) + " Высота: " +
                Math.Abs(Height) + " Закрашен: " + Filled + " Контур: " +
                Contoured;
         if (Contoured)
            str += " Цвет контура: " + ColorContour;
         return str;
      }

      /// <summary>
      /// Построение прямоугольника при рисовании по точке начала и конца
      /// </summary>
      public void BuildRectangle()
      {
         PointF[] NewPointsArray = new PointF[Points.Length];
         NewPointsArray[0] = new PointF(Start.X, End.Y);
         NewPointsArray[1] = Start;
         NewPointsArray[2] = new PointF(End.X, Start.Y);
         NewPointsArray[3] = End;
         Points = NewPointsArray;
      }
      
      public void BuildRectangleWhileMoving(PointF elocation, PointF previewElocation)
      {
         PointF[] NewPointsArray = new PointF[Points.Length];
         NewPointsArray[0] = new PointF(Points[0].X - (previewElocation.X - elocation.X),
            Points[0].Y - (previewElocation.Y - elocation.Y));
         NewPointsArray[1] = new PointF(Points[1].X - (previewElocation.X - elocation.X),
            Points[1].Y - (previewElocation.Y - elocation.Y));
         NewPointsArray[2] = new PointF(Points[2].X - (previewElocation.X - elocation.X),
            Points[2].Y - (previewElocation.Y - elocation.Y));
         NewPointsArray[3] = new PointF(Points[3].X - (previewElocation.X - elocation.X),
            Points[3].Y - (previewElocation.Y - elocation.Y));
         Points = NewPointsArray;
      }

      public float AngleToX()
      {
        return (float)(Math.Atan2(end.Y - start.Y, end.X - start.X) * Constants.RAD_TO_GRAD); 
      }
      
      public void RotateRectangle()
      {
         PointF[] ConvertedPoints = new PointF[points.Count()];
         for (int i = 0; i < points.Count(); i++)
         {
            ConvertedPoints[i] = GetConvertedPoint(points[i]);
         }
         Start = GetConvertedPoint(start);
         End = GetConvertedPoint(end);
         prevAngle = angle;
         Points = ConvertedPoints;
      }

      private PointF GetConvertedPoint(PointF point)
      {
         float x = point.X - reference.X;
         float y = point.Y - reference.Y;
         point.X = (float)(x * Math.Cos((angle - prevAngle) / 180.0 * Math.PI) -
                   y * Math.Sin((angle - prevAngle) / 180.0 * Math.PI)) + reference.X;
         point.Y = (float)(x * Math.Sin((angle - prevAngle) / 180.0 * Math.PI) +
                   y * Math.Cos((angle - prevAngle) / 180.0 * Math.PI)) + reference.Y;
         return point;
      }
      
      /// <summary>
      /// Растягивание прямоугольника за его боковые грани
      /// </summary>
      public void ChangeRectangleSides(PointF elocation, int pointIndex, float pAngle)
       {
         if (pointIndex == 0)
         {
            Points[pointIndex].X = elocation.X * (float)(Math.Sin((pAngle) / Constants.GRAD_TO_RAD));
            Points[pointIndex + 1].X = elocation.X * (float)(Math.Sin((pAngle) / Constants.GRAD_TO_RAD));
         }
         else if (pointIndex == 1)
         {
            Points[pointIndex].Y = elocation.Y;
            Points[pointIndex + 1].Y = elocation.Y;
         }
         else if (pointIndex == 2)
         {
            Points[pointIndex].X = elocation.X;
            Points[pointIndex + 1].X = elocation.X;
         }
         else if (pointIndex == 3)
         {
            Points[pointIndex].Y = elocation.Y;
            Points[pointIndex - 3].Y = elocation.Y;
         }
         Start = Points[1];
         End = Points[3];
      }

      /// <summary>
      /// Новые точки для прямоугольника после его изменения
      /// </summary>
      public void GetNewRectanglePoints(PointF elocation, int pointIndex)
      {
         Points[pointIndex] = elocation;
         if (pointIndex == 0)
         {
            Points[pointIndex + 3].Y = Points[pointIndex].Y;
            Points[pointIndex + 1].X = Points[pointIndex].X;
         }
         else if (pointIndex == 1)
         {
            Points[pointIndex + 1].Y = Points[pointIndex].Y;
            Points[pointIndex - 1].X = Points[pointIndex].X;
         }
         else if (pointIndex == 2)
         {
            Points[pointIndex - 1].Y = Points[pointIndex].Y;
            Points[pointIndex + 1].X = Points[pointIndex].X;
         }
         else if (pointIndex == 3)
         {
            Points[pointIndex - 3].Y = Points[pointIndex].Y;
            Points[pointIndex - 1].X = Points[pointIndex].X;
         }
         Start = Points[1];
         End = Points[3];
      }

      
      
      /// <summary>
      /// Перемещение прямоугольника по горизонтали и по вертикали
      /// </summary>
      /// <returns></returns>
      public void RectangleHorVerMoving(float x, float y)
      {
         PointF[] NewPointsArray = new PointF[points.Length];
         for (int i = 0; i < Points.Length; i++)
         {
            NewPointsArray[i] = new PointF(Points[i].X + x, Points[i].Y + y);
         }
         Points = NewPointsArray;
         Start = new PointF(Start.X+x, Start.Y+y);
         End = new PointF(End.X + x, End.Y + y);
      }
      
      public void DrawSelectRectangles(PaintEventArgs e)
      {
         if (!Filled && !Contoured)
         {
            Pen penColorRectangle = new Pen(ColorContour, PenThickness);
            penColorRectangle.DashStyle = DashStyle;
            e.Graphics.DrawPolygon(penColorRectangle, Points);
         }
         else if (Filled)
         {
            if(HatchStyleOn)
               using (Brush BrushColorRectangle = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                  e.Graphics.FillPolygon(BrushColorRectangle, Points);
            else
               using (Brush BrushColorRectangle = new SolidBrush(ColorFilling))
                  e.Graphics.FillPolygon(BrushColorRectangle, Points);
         }
         else if (Contoured)
         {
            if (HatchStyleOn)
               using (Brush BrushColorRectangle = new HatchBrush(HatchStyle, ColorContour, ColorFilling))
                  e.Graphics.FillPolygon(BrushColorRectangle, Points);
            else
               using (Brush BrushColorRectangle = new SolidBrush(ColorFilling))
                  e.Graphics.FillPolygon(BrushColorRectangle, Points);

            Pen contourColorRectangle = new Pen(ColorContour, PenThickness);
            contourColorRectangle.DashStyle = DashStyle;
               e.Graphics.DrawPolygon(contourColorRectangle, Points);
         }

         if (IsSelected)
         {
            Markers.DrawMarkersForRectangle(e, this);
         }
      }

   }
}
