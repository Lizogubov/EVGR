﻿using System.Drawing;

namespace EVGR
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
         System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Размер");
         System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Палитра");
         System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Фон");
         System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Основные параметры", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
         System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Геометрические фигуры");
         System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Проектирование", new System.Windows.Forms.TreeNode[] {
            treeNode5});
         this.pnFullOptionsOnLeft = new System.Windows.Forms.Panel();
         this.pnFiguresToDraw = new System.Windows.Forms.Panel();
         this.cbHatchStyle = new System.Windows.Forms.ComboBox();
         this.lbChooseHatchStyle = new System.Windows.Forms.Label();
         this.cbContourStyle = new System.Windows.Forms.ComboBox();
         this.lbChooseContourStyle = new System.Windows.Forms.Label();
         this.cbCountoring = new System.Windows.Forms.CheckBox();
         this.cbFilling = new System.Windows.Forms.CheckBox();
         this.lbColorContour = new System.Windows.Forms.Label();
         this.btColorContour = new System.Windows.Forms.Button();
         this.btColorFilling = new System.Windows.Forms.Button();
         this.numWidth = new System.Windows.Forms.NumericUpDown();
         this.lbColorFilling = new System.Windows.Forms.Label();
         this.lbWidth = new System.Windows.Forms.Label();
         this.lbFigures = new System.Windows.Forms.Label();
         this.btRectangleDrawing = new System.Windows.Forms.Button();
         this.btPolygonDrawing = new System.Windows.Forms.Button();
         this.btPieDrawing = new System.Windows.Forms.Button();
         this.btEllipseDrawing = new System.Windows.Forms.Button();
         this.btArcDrawing = new System.Windows.Forms.Button();
         this.btLineDrawing = new System.Windows.Forms.Button();
         this.ssMain = new System.Windows.Forms.StatusStrip();
         this.lbCoordinates = new System.Windows.Forms.ToolStripStatusLabel();
         this.lbSize = new System.Windows.Forms.ToolStripStatusLabel();
         this.pnBackground = new System.Windows.Forms.Panel();
         this.lbChooseBackgroundColor = new System.Windows.Forms.Label();
         this.cbPanelDrawingBackground = new System.Windows.Forms.ComboBox();
         this.pnForPallette = new System.Windows.Forms.Panel();
         this.btColor2 = new System.Windows.Forms.Button();
         this.lbPallette = new System.Windows.Forms.Label();
         this.btColor1 = new System.Windows.Forms.Button();
         this.btColor20 = new System.Windows.Forms.Button();
         this.btColor13 = new System.Windows.Forms.Button();
         this.btColor12 = new System.Windows.Forms.Button();
         this.btColor19 = new System.Windows.Forms.Button();
         this.btColor11 = new System.Windows.Forms.Button();
         this.btColor16 = new System.Windows.Forms.Button();
         this.btColor10 = new System.Windows.Forms.Button();
         this.btColor17 = new System.Windows.Forms.Button();
         this.btColor9 = new System.Windows.Forms.Button();
         this.btColor18 = new System.Windows.Forms.Button();
         this.btColor8 = new System.Windows.Forms.Button();
         this.btColor7 = new System.Windows.Forms.Button();
         this.btColor14 = new System.Windows.Forms.Button();
         this.btColor6 = new System.Windows.Forms.Button();
         this.btColor15 = new System.Windows.Forms.Button();
         this.btColor5 = new System.Windows.Forms.Button();
         this.btColor4 = new System.Windows.Forms.Button();
         this.btColor3 = new System.Windows.Forms.Button();
         this.pnExtension = new System.Windows.Forms.Panel();
         this.label2 = new System.Windows.Forms.Label();
         this.tbPanelDrawingY = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.tbPanelDrawingX = new System.Windows.Forms.TextBox();
         this.btSetPanelDrawingSize = new System.Windows.Forms.Button();
         this.lbСhooseResolution = new System.Windows.Forms.Label();
         this.twMain = new System.Windows.Forms.TreeView();
         this.pnOptions = new System.Windows.Forms.Panel();
         this.btScaling = new System.Windows.Forms.Button();
         this.btSelect = new System.Windows.Forms.Button();
         this.btJournal = new System.Windows.Forms.Button();
         this.pnPropertiesAndJournal = new System.Windows.Forms.Panel();
         this.pnPropertiesFiguresDUBLICATE = new System.Windows.Forms.Panel();
         this.pnFiguresProps = new System.Windows.Forms.Panel();
         this.pnPolygonsProps = new System.Windows.Forms.Panel();
         this.btPolygonsPropsOnOff = new System.Windows.Forms.Button();
         this.pnArcsProps = new System.Windows.Forms.Panel();
         this.btArcsPropsOnOff = new System.Windows.Forms.Button();
         this.pnEllipsesProps = new System.Windows.Forms.Panel();
         this.btEllipsesPropsOnOff = new System.Windows.Forms.Button();
         this.pnPiesProps = new System.Windows.Forms.Panel();
         this.btPiesPropsOnOff = new System.Windows.Forms.Button();
         this.pnRectanglesProps = new System.Windows.Forms.Panel();
         this.btRectanglesPropsOnOff = new System.Windows.Forms.Button();
         this.pnLinesProps = new System.Windows.Forms.Panel();
         this.btLinesPropsOnOff = new System.Windows.Forms.Button();
         this.lbFiguresProps = new System.Windows.Forms.Label();
         this.pnForJournal = new System.Windows.Forms.Panel();
         this.tbJornal = new System.Windows.Forms.RichTextBox();
         this.msMain = new System.Windows.Forms.MenuStrip();
         this.miFile = new System.Windows.Forms.ToolStripMenuItem();
         this.miNew = new System.Windows.Forms.ToolStripMenuItem();
         this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
         this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
         this.miSave = new System.Windows.Forms.ToolStripMenuItem();
         this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
         this.miExit = new System.Windows.Forms.ToolStripMenuItem();
         this.miPictureConfigures = new System.Windows.Forms.ToolStripMenuItem();
         this.miAntialiasing = new System.Windows.Forms.ToolStripMenuItem();
         this.miAntialiasing_on = new System.Windows.Forms.ToolStripMenuItem();
         this.miAntialiasing_off = new System.Windows.Forms.ToolStripMenuItem();
         this.miAboutProgram = new System.Windows.Forms.ToolStripMenuItem();
         this.miAboutCreator = new System.Windows.Forms.ToolStripMenuItem();
         this.fdOpen = new System.Windows.Forms.OpenFileDialog();
         this.fdSave = new System.Windows.Forms.SaveFileDialog();
         this.pnUnderPanelDrawing = new EVGR.PanelUnderDrawingPanel();
         this.pnPanelDrawing = new EVGR.DrawingPanel();
         this.pnFullOptionsOnLeft.SuspendLayout();
         this.pnFiguresToDraw.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numWidth)).BeginInit();
         this.ssMain.SuspendLayout();
         this.pnBackground.SuspendLayout();
         this.pnForPallette.SuspendLayout();
         this.pnExtension.SuspendLayout();
         this.pnOptions.SuspendLayout();
         this.pnPropertiesAndJournal.SuspendLayout();
         this.pnPropertiesFiguresDUBLICATE.SuspendLayout();
         this.pnFiguresProps.SuspendLayout();
         this.pnForJournal.SuspendLayout();
         this.msMain.SuspendLayout();
         this.pnUnderPanelDrawing.SuspendLayout();
         this.SuspendLayout();
         // 
         // pnFullOptionsOnLeft
         // 
         this.pnFullOptionsOnLeft.AutoScroll = true;
         this.pnFullOptionsOnLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnFullOptionsOnLeft.BackColor = System.Drawing.SystemColors.ActiveCaption;
         this.pnFullOptionsOnLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
         this.pnFullOptionsOnLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.pnFullOptionsOnLeft.Controls.Add(this.pnFiguresToDraw);
         this.pnFullOptionsOnLeft.Controls.Add(this.ssMain);
         this.pnFullOptionsOnLeft.Controls.Add(this.pnBackground);
         this.pnFullOptionsOnLeft.Controls.Add(this.pnForPallette);
         this.pnFullOptionsOnLeft.Controls.Add(this.pnExtension);
         this.pnFullOptionsOnLeft.Controls.Add(this.twMain);
         this.pnFullOptionsOnLeft.Dock = System.Windows.Forms.DockStyle.Left;
         this.pnFullOptionsOnLeft.Location = new System.Drawing.Point(0, 24);
         this.pnFullOptionsOnLeft.Margin = new System.Windows.Forms.Padding(3, 3, 13, 3);
         this.pnFullOptionsOnLeft.Name = "pnFullOptionsOnLeft";
         this.pnFullOptionsOnLeft.Size = new System.Drawing.Size(239, 938);
         this.pnFullOptionsOnLeft.TabIndex = 0;
         // 
         // pnFiguresToDraw
         // 
         this.pnFiguresToDraw.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pnFiguresToDraw.Controls.Add(this.cbHatchStyle);
         this.pnFiguresToDraw.Controls.Add(this.lbChooseHatchStyle);
         this.pnFiguresToDraw.Controls.Add(this.cbContourStyle);
         this.pnFiguresToDraw.Controls.Add(this.lbChooseContourStyle);
         this.pnFiguresToDraw.Controls.Add(this.cbCountoring);
         this.pnFiguresToDraw.Controls.Add(this.cbFilling);
         this.pnFiguresToDraw.Controls.Add(this.lbColorContour);
         this.pnFiguresToDraw.Controls.Add(this.btColorContour);
         this.pnFiguresToDraw.Controls.Add(this.btColorFilling);
         this.pnFiguresToDraw.Controls.Add(this.numWidth);
         this.pnFiguresToDraw.Controls.Add(this.lbColorFilling);
         this.pnFiguresToDraw.Controls.Add(this.lbWidth);
         this.pnFiguresToDraw.Controls.Add(this.lbFigures);
         this.pnFiguresToDraw.Controls.Add(this.btRectangleDrawing);
         this.pnFiguresToDraw.Controls.Add(this.btPolygonDrawing);
         this.pnFiguresToDraw.Controls.Add(this.btPieDrawing);
         this.pnFiguresToDraw.Controls.Add(this.btEllipseDrawing);
         this.pnFiguresToDraw.Controls.Add(this.btArcDrawing);
         this.pnFiguresToDraw.Controls.Add(this.btLineDrawing);
         this.pnFiguresToDraw.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnFiguresToDraw.Location = new System.Drawing.Point(0, 487);
         this.pnFiguresToDraw.Name = "pnFiguresToDraw";
         this.pnFiguresToDraw.Size = new System.Drawing.Size(237, 264);
         this.pnFiguresToDraw.TabIndex = 39;
         this.pnFiguresToDraw.Visible = false;
         // 
         // cbHatchStyle
         // 
         this.cbHatchStyle.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
         this.cbHatchStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cbHatchStyle.DropDownWidth = 190;
         this.cbHatchStyle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.cbHatchStyle.FormattingEnabled = true;
         this.cbHatchStyle.Items.AddRange(new object[] {
            "Сплошной цвет",
            "Горизонтальная",
            "Вертикальная",
            "Диагональная вниз",
            "Диагональная вверх",
            "Крупная клетка",
            "Диагональная клетка",
            "05 процентов",
            "10 процентов",
            "20 процентов",
            "25 процентов",
            "30 процентов",
            "40 процентов",
            "50 процентов",
            "60 процентов",
            "70 процентов",
            "75 процентов",
            "80 процентов",
            "90 процентов",
            "Легкая диагональная вниз",
            "Легкая диагональная вверх",
            "Плотная диагональная вниз",
            "Плотная диагональная вверх",
            "Широкая диагональная вниз",
            "Широкая диагональная вверх",
            "Легкая вевртикальная ",
            "Легкая горизонтальная",
            "Узкая вертикальная",
            "Узкая горизонтальная",
            "Плотная вертикальная",
            "Плотная горизонтальная",
            "Штриховая диагональная вниз",
            "Штриховая диагональная вверх",
            "Штриховая горизонтальная",
            "Штриховая вертикальная",
            "Мелкие конфетти",
            "Крупные конфетти",
            "Зигзаг",
            "Волна",
            "Диагональный кирпич",
            "Горизонтальный кирпич",
            "Плетенка",
            "Шотландка",
            "Уголки",
            "Точечная сетка",
            "Точечные ромбики ",
            "Дранка",
            "Шпалера",
            "Сферы",
            "Мелкая клетка",
            "Мелкая шахматная доска",
            "Крупная шахматная доска",
            "Контурные ромбики",
            "Ромбики"});
         this.cbHatchStyle.Location = new System.Drawing.Point(114, 219);
         this.cbHatchStyle.MaxDropDownItems = 20;
         this.cbHatchStyle.Name = "cbHatchStyle";
         this.cbHatchStyle.Size = new System.Drawing.Size(116, 21);
         this.cbHatchStyle.TabIndex = 48;
         this.cbHatchStyle.SelectedIndexChanged += new System.EventHandler(this.cbHatchStyle_SelectedIndexChanged);
         this.cbHatchStyle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbHatchStyle_MouseClick);
         // 
         // lbChooseHatchStyle
         // 
         this.lbChooseHatchStyle.AutoSize = true;
         this.lbChooseHatchStyle.Location = new System.Drawing.Point(11, 222);
         this.lbChooseHatchStyle.Name = "lbChooseHatchStyle";
         this.lbChooseHatchStyle.Size = new System.Drawing.Size(97, 13);
         this.lbChooseHatchStyle.TabIndex = 47;
         this.lbChooseHatchStyle.Text = "Стиль штриховки:";
         // 
         // cbContourStyle
         // 
         this.cbContourStyle.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
         this.cbContourStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cbContourStyle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.cbContourStyle.Location = new System.Drawing.Point(114, 179);
         this.cbContourStyle.MaxDropDownItems = 30;
         this.cbContourStyle.Name = "cbContourStyle";
         this.cbContourStyle.Size = new System.Drawing.Size(116, 21);
         this.cbContourStyle.TabIndex = 46;
         this.cbContourStyle.SelectedIndexChanged += new System.EventHandler(this.cbContourStyle_SelectedIndexChanged);
         this.cbContourStyle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbContourStyle_MouseClick);
         // 
         // lbChooseContourStyle
         // 
         this.lbChooseContourStyle.AutoSize = true;
         this.lbChooseContourStyle.Location = new System.Drawing.Point(11, 182);
         this.lbChooseContourStyle.Name = "lbChooseContourStyle";
         this.lbChooseContourStyle.Size = new System.Drawing.Size(83, 13);
         this.lbChooseContourStyle.TabIndex = 45;
         this.lbChooseContourStyle.Text = "Стиль контура:";
         // 
         // cbCountoring
         // 
         this.cbCountoring.AutoSize = true;
         this.cbCountoring.Location = new System.Drawing.Point(11, 142);
         this.cbCountoring.Name = "cbCountoring";
         this.cbCountoring.Size = new System.Drawing.Size(121, 17);
         this.cbCountoring.TabIndex = 44;
         this.cbCountoring.Text = "Контур c заливкой";
         this.cbCountoring.UseVisualStyleBackColor = true;
         this.cbCountoring.CheckedChanged += new System.EventHandler(this.checkBox_Countoring_CheckedChanged);
         // 
         // cbFilling
         // 
         this.cbFilling.AutoSize = true;
         this.cbFilling.Location = new System.Drawing.Point(130, 142);
         this.cbFilling.Name = "cbFilling";
         this.cbFilling.Size = new System.Drawing.Size(108, 17);
         this.cbFilling.TabIndex = 43;
         this.cbFilling.Text = "Только заливка";
         this.cbFilling.UseVisualStyleBackColor = true;
         this.cbFilling.CheckedChanged += new System.EventHandler(this.checkBox_Filling_CheckedChanged);
         // 
         // lbColorContour
         // 
         this.lbColorContour.AutoSize = true;
         this.lbColorContour.Location = new System.Drawing.Point(3, 76);
         this.lbColorContour.Name = "lbColorContour";
         this.lbColorContour.Size = new System.Drawing.Size(50, 26);
         this.lbColorContour.TabIndex = 41;
         this.lbColorContour.Text = "  Цвет\r\nконтура:";
         // 
         // btColorContour
         // 
         this.btColorContour.BackColor = System.Drawing.Color.Black;
         this.btColorContour.Location = new System.Drawing.Point(12, 102);
         this.btColorContour.Name = "btColorContour";
         this.btColorContour.Size = new System.Drawing.Size(27, 22);
         this.btColorContour.TabIndex = 42;
         this.btColorContour.UseVisualStyleBackColor = false;
         this.btColorContour.BackColorChanged += new System.EventHandler(this.btColorContour_BackColorChanged);
         this.btColorContour.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_figures_colorContour_MouseUp);
         // 
         // btColorFilling
         // 
         this.btColorFilling.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
         this.btColorFilling.Location = new System.Drawing.Point(78, 103);
         this.btColorFilling.Name = "btColorFilling";
         this.btColorFilling.Size = new System.Drawing.Size(27, 22);
         this.btColorFilling.TabIndex = 34;
         this.btColorFilling.UseVisualStyleBackColor = false;
         this.btColorFilling.BackColorChanged += new System.EventHandler(this.btColorFilling_BackColorChanged);
         this.btColorFilling.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_figures_color_MouseUp);
         // 
         // numWidth
         // 
         this.numWidth.DecimalPlaces = 1;
         this.numWidth.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
         this.numWidth.Location = new System.Drawing.Point(146, 105);
         this.numWidth.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
         this.numWidth.Name = "numWidth";
         this.numWidth.Size = new System.Drawing.Size(57, 20);
         this.numWidth.TabIndex = 30;
         this.numWidth.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
         this.numWidth.ValueChanged += new System.EventHandler(this.numWidth_ValueChanged);
         // 
         // lbColorFilling
         // 
         this.lbColorFilling.AutoSize = true;
         this.lbColorFilling.Location = new System.Drawing.Point(68, 76);
         this.lbColorFilling.Name = "lbColorFilling";
         this.lbColorFilling.Size = new System.Drawing.Size(52, 26);
         this.lbColorFilling.TabIndex = 33;
         this.lbColorFilling.Text = "  Цвет\r\nзаливки:";
         // 
         // lbWidth
         // 
         this.lbWidth.AutoSize = true;
         this.lbWidth.Location = new System.Drawing.Point(133, 89);
         this.lbWidth.Name = "lbWidth";
         this.lbWidth.Size = new System.Drawing.Size(89, 13);
         this.lbWidth.TabIndex = 29;
         this.lbWidth.Text = "Толщина линии:";
         // 
         // lbFigures
         // 
         this.lbFigures.AutoSize = true;
         this.lbFigures.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
         this.lbFigures.Location = new System.Drawing.Point(78, -1);
         this.lbFigures.Name = "lbFigures";
         this.lbFigures.Size = new System.Drawing.Size(79, 24);
         this.lbFigures.TabIndex = 32;
         this.lbFigures.Text = "Фигуры";
         // 
         // btRectangleDrawing
         // 
         this.btRectangleDrawing.Image = ((System.Drawing.Image)(resources.GetObject("btRectangleDrawing.Image")));
         this.btRectangleDrawing.Location = new System.Drawing.Point(50, 34);
         this.btRectangleDrawing.Name = "btRectangleDrawing";
         this.btRectangleDrawing.Size = new System.Drawing.Size(30, 30);
         this.btRectangleDrawing.TabIndex = 5;
         this.btRectangleDrawing.UseVisualStyleBackColor = true;
         this.btRectangleDrawing.Click += new System.EventHandler(this.button_RectangleDrawing_Click);
         // 
         // btPolygonDrawing
         // 
         this.btPolygonDrawing.Image = global::EVGR.Properties.Resources.Polygon;
         this.btPolygonDrawing.Location = new System.Drawing.Point(86, 34);
         this.btPolygonDrawing.Name = "btPolygonDrawing";
         this.btPolygonDrawing.Size = new System.Drawing.Size(30, 30);
         this.btPolygonDrawing.TabIndex = 4;
         this.btPolygonDrawing.UseVisualStyleBackColor = true;
         this.btPolygonDrawing.Click += new System.EventHandler(this.button_PolygonDrawing_Click);
         // 
         // btPieDrawing
         // 
         this.btPieDrawing.Image = global::EVGR.Properties.Resources.Pie;
         this.btPieDrawing.Location = new System.Drawing.Point(122, 34);
         this.btPieDrawing.Name = "btPieDrawing";
         this.btPieDrawing.Size = new System.Drawing.Size(30, 30);
         this.btPieDrawing.TabIndex = 3;
         this.btPieDrawing.UseVisualStyleBackColor = true;
         this.btPieDrawing.Click += new System.EventHandler(this.button_PieDrawing_Click);
         // 
         // btEllipseDrawing
         // 
         this.btEllipseDrawing.Image = global::EVGR.Properties.Resources.Ellipse;
         this.btEllipseDrawing.Location = new System.Drawing.Point(158, 34);
         this.btEllipseDrawing.Name = "btEllipseDrawing";
         this.btEllipseDrawing.Size = new System.Drawing.Size(30, 30);
         this.btEllipseDrawing.TabIndex = 2;
         this.btEllipseDrawing.UseVisualStyleBackColor = true;
         this.btEllipseDrawing.Click += new System.EventHandler(this.button_EllipseDrawing_Click);
         // 
         // btArcDrawing
         // 
         this.btArcDrawing.Image = global::EVGR.Properties.Resources.Arc;
         this.btArcDrawing.Location = new System.Drawing.Point(194, 34);
         this.btArcDrawing.Name = "btArcDrawing";
         this.btArcDrawing.Size = new System.Drawing.Size(30, 30);
         this.btArcDrawing.TabIndex = 1;
         this.btArcDrawing.UseVisualStyleBackColor = true;
         this.btArcDrawing.Click += new System.EventHandler(this.button_ArcDrawing_Click);
         // 
         // btLineDrawing
         // 
         this.btLineDrawing.BackColor = System.Drawing.SystemColors.ActiveCaption;
         this.btLineDrawing.ForeColor = System.Drawing.SystemColors.ControlText;
         this.btLineDrawing.Image = ((System.Drawing.Image)(resources.GetObject("btLineDrawing.Image")));
         this.btLineDrawing.Location = new System.Drawing.Point(14, 34);
         this.btLineDrawing.Name = "btLineDrawing";
         this.btLineDrawing.Size = new System.Drawing.Size(30, 30);
         this.btLineDrawing.TabIndex = 0;
         this.btLineDrawing.UseVisualStyleBackColor = false;
         this.btLineDrawing.Click += new System.EventHandler(this.button_LineDrawing_Click);
         // 
         // ssMain
         // 
         this.ssMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbCoordinates,
            this.lbSize});
         this.ssMain.Location = new System.Drawing.Point(0, 914);
         this.ssMain.Name = "ssMain";
         this.ssMain.Size = new System.Drawing.Size(237, 22);
         this.ssMain.TabIndex = 33;
         // 
         // lbCoordinates
         // 
         this.lbCoordinates.Image = global::EVGR.Properties.Resources.CursorXYIcon;
         this.lbCoordinates.Name = "lbCoordinates";
         this.lbCoordinates.Size = new System.Drawing.Size(16, 17);
         // 
         // lbSize
         // 
         this.lbSize.Image = global::EVGR.Properties.Resources.ImageSizeIcon;
         this.lbSize.Name = "lbSize";
         this.lbSize.Size = new System.Drawing.Size(16, 17);
         // 
         // pnBackground
         // 
         this.pnBackground.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pnBackground.Controls.Add(this.lbChooseBackgroundColor);
         this.pnBackground.Controls.Add(this.cbPanelDrawingBackground);
         this.pnBackground.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnBackground.Location = new System.Drawing.Point(0, 454);
         this.pnBackground.Name = "pnBackground";
         this.pnBackground.Size = new System.Drawing.Size(237, 33);
         this.pnBackground.TabIndex = 31;
         this.pnBackground.Visible = false;
         // 
         // lbChooseBackgroundColor
         // 
         this.lbChooseBackgroundColor.AutoSize = true;
         this.lbChooseBackgroundColor.Location = new System.Drawing.Point(3, 11);
         this.lbChooseBackgroundColor.Name = "lbChooseBackgroundColor";
         this.lbChooseBackgroundColor.Size = new System.Drawing.Size(86, 13);
         this.lbChooseBackgroundColor.TabIndex = 20;
         this.lbChooseBackgroundColor.Text = "Выберите фон: ";
         // 
         // cbPanelDrawingBackground
         // 
         this.cbPanelDrawingBackground.BackColor = System.Drawing.SystemColors.Window;
         this.cbPanelDrawingBackground.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
         this.cbPanelDrawingBackground.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cbPanelDrawingBackground.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.cbPanelDrawingBackground.FormattingEnabled = true;
         this.cbPanelDrawingBackground.Items.AddRange(new object[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
         this.cbPanelDrawingBackground.Location = new System.Drawing.Point(113, 5);
         this.cbPanelDrawingBackground.MaxDropDownItems = 20;
         this.cbPanelDrawingBackground.Name = "cbPanelDrawingBackground";
         this.cbPanelDrawingBackground.Size = new System.Drawing.Size(102, 21);
         this.cbPanelDrawingBackground.TabIndex = 26;
         this.cbPanelDrawingBackground.SelectedIndexChanged += new System.EventHandler(this.cbPanelDrawingBackground_SelectedIndexChanged);
         this.cbPanelDrawingBackground.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbPanelDrawingBackground_MouseClick);
         // 
         // pnForPallette
         // 
         this.pnForPallette.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pnForPallette.Controls.Add(this.btColor2);
         this.pnForPallette.Controls.Add(this.lbPallette);
         this.pnForPallette.Controls.Add(this.btColor1);
         this.pnForPallette.Controls.Add(this.btColor20);
         this.pnForPallette.Controls.Add(this.btColor13);
         this.pnForPallette.Controls.Add(this.btColor12);
         this.pnForPallette.Controls.Add(this.btColor19);
         this.pnForPallette.Controls.Add(this.btColor11);
         this.pnForPallette.Controls.Add(this.btColor16);
         this.pnForPallette.Controls.Add(this.btColor10);
         this.pnForPallette.Controls.Add(this.btColor17);
         this.pnForPallette.Controls.Add(this.btColor9);
         this.pnForPallette.Controls.Add(this.btColor18);
         this.pnForPallette.Controls.Add(this.btColor8);
         this.pnForPallette.Controls.Add(this.btColor7);
         this.pnForPallette.Controls.Add(this.btColor14);
         this.pnForPallette.Controls.Add(this.btColor6);
         this.pnForPallette.Controls.Add(this.btColor15);
         this.pnForPallette.Controls.Add(this.btColor5);
         this.pnForPallette.Controls.Add(this.btColor4);
         this.pnForPallette.Controls.Add(this.btColor3);
         this.pnForPallette.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnForPallette.Location = new System.Drawing.Point(0, 325);
         this.pnForPallette.Name = "pnForPallette";
         this.pnForPallette.Size = new System.Drawing.Size(237, 129);
         this.pnForPallette.TabIndex = 30;
         this.pnForPallette.Visible = false;
         // 
         // btColor2
         // 
         this.btColor2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(151)))), ((int)(((byte)(151)))));
         this.btColor2.Location = new System.Drawing.Point(58, 19);
         this.btColor2.Name = "btColor2";
         this.btColor2.Size = new System.Drawing.Size(31, 22);
         this.btColor2.TabIndex = 17;
         this.btColor2.UseVisualStyleBackColor = false;
         this.btColor2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // lbPallette
         // 
         this.lbPallette.AutoSize = true;
         this.lbPallette.Location = new System.Drawing.Point(11, 1);
         this.lbPallette.Name = "lbPallette";
         this.lbPallette.Size = new System.Drawing.Size(211, 13);
         this.lbPallette.TabIndex = 4;
         this.lbPallette.Text = "Задайте нужные Вам для работы цвета:";
         // 
         // btColor1
         // 
         this.btColor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
         this.btColor1.Location = new System.Drawing.Point(12, 19);
         this.btColor1.Name = "btColor1";
         this.btColor1.Size = new System.Drawing.Size(31, 22);
         this.btColor1.TabIndex = 5;
         this.btColor1.UseVisualStyleBackColor = false;
         this.btColor1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor20
         // 
         this.btColor20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
         this.btColor20.Location = new System.Drawing.Point(191, 103);
         this.btColor20.Name = "btColor20";
         this.btColor20.Size = new System.Drawing.Size(31, 22);
         this.btColor20.TabIndex = 27;
         this.btColor20.UseVisualStyleBackColor = false;
         this.btColor20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor13
         // 
         this.btColor13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
         this.btColor13.Location = new System.Drawing.Point(102, 75);
         this.btColor13.Name = "btColor13";
         this.btColor13.Size = new System.Drawing.Size(31, 22);
         this.btColor13.TabIndex = 6;
         this.btColor13.UseVisualStyleBackColor = false;
         this.btColor13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor12
         // 
         this.btColor12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
         this.btColor12.Location = new System.Drawing.Point(58, 75);
         this.btColor12.Name = "btColor12";
         this.btColor12.Size = new System.Drawing.Size(31, 22);
         this.btColor12.TabIndex = 7;
         this.btColor12.UseVisualStyleBackColor = false;
         this.btColor12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor19
         // 
         this.btColor19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
         this.btColor19.Location = new System.Drawing.Point(148, 103);
         this.btColor19.Name = "btColor19";
         this.btColor19.Size = new System.Drawing.Size(31, 22);
         this.btColor19.TabIndex = 25;
         this.btColor19.UseVisualStyleBackColor = false;
         this.btColor19.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor11
         // 
         this.btColor11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
         this.btColor11.Location = new System.Drawing.Point(12, 75);
         this.btColor11.Name = "btColor11";
         this.btColor11.Size = new System.Drawing.Size(31, 22);
         this.btColor11.TabIndex = 8;
         this.btColor11.UseVisualStyleBackColor = false;
         this.btColor11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor16
         // 
         this.btColor16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
         this.btColor16.Location = new System.Drawing.Point(12, 103);
         this.btColor16.Name = "btColor16";
         this.btColor16.Size = new System.Drawing.Size(31, 22);
         this.btColor16.TabIndex = 24;
         this.btColor16.UseVisualStyleBackColor = false;
         this.btColor16.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor10
         // 
         this.btColor10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(203)))), ((int)(((byte)(255)))));
         this.btColor10.Location = new System.Drawing.Point(191, 47);
         this.btColor10.Name = "btColor10";
         this.btColor10.Size = new System.Drawing.Size(31, 22);
         this.btColor10.TabIndex = 9;
         this.btColor10.UseVisualStyleBackColor = false;
         this.btColor10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor17
         // 
         this.btColor17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
         this.btColor17.Location = new System.Drawing.Point(58, 103);
         this.btColor17.Name = "btColor17";
         this.btColor17.Size = new System.Drawing.Size(31, 22);
         this.btColor17.TabIndex = 23;
         this.btColor17.UseVisualStyleBackColor = false;
         this.btColor17.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor9
         // 
         this.btColor9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
         this.btColor9.Location = new System.Drawing.Point(148, 47);
         this.btColor9.Name = "btColor9";
         this.btColor9.Size = new System.Drawing.Size(31, 22);
         this.btColor9.TabIndex = 10;
         this.btColor9.UseVisualStyleBackColor = false;
         this.btColor9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor18
         // 
         this.btColor18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(160)))), ((int)(((byte)(200)))));
         this.btColor18.Location = new System.Drawing.Point(102, 103);
         this.btColor18.Name = "btColor18";
         this.btColor18.Size = new System.Drawing.Size(31, 22);
         this.btColor18.TabIndex = 22;
         this.btColor18.UseVisualStyleBackColor = false;
         this.btColor18.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor8
         // 
         this.btColor8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
         this.btColor8.Location = new System.Drawing.Point(102, 47);
         this.btColor8.Name = "btColor8";
         this.btColor8.Size = new System.Drawing.Size(31, 22);
         this.btColor8.TabIndex = 11;
         this.btColor8.UseVisualStyleBackColor = false;
         this.btColor8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor7
         // 
         this.btColor7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
         this.btColor7.Location = new System.Drawing.Point(58, 47);
         this.btColor7.Name = "btColor7";
         this.btColor7.Size = new System.Drawing.Size(31, 22);
         this.btColor7.TabIndex = 12;
         this.btColor7.UseVisualStyleBackColor = false;
         this.btColor7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor14
         // 
         this.btColor14.BackColor = System.Drawing.Color.Aqua;
         this.btColor14.Location = new System.Drawing.Point(148, 75);
         this.btColor14.Name = "btColor14";
         this.btColor14.Size = new System.Drawing.Size(31, 22);
         this.btColor14.TabIndex = 19;
         this.btColor14.UseVisualStyleBackColor = false;
         this.btColor14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor6
         // 
         this.btColor6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(63)))));
         this.btColor6.Location = new System.Drawing.Point(12, 47);
         this.btColor6.Name = "btColor6";
         this.btColor6.Size = new System.Drawing.Size(31, 22);
         this.btColor6.TabIndex = 13;
         this.btColor6.UseVisualStyleBackColor = false;
         this.btColor6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor15
         // 
         this.btColor15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(51)))), ((int)(((byte)(0)))));
         this.btColor15.Location = new System.Drawing.Point(191, 75);
         this.btColor15.Name = "btColor15";
         this.btColor15.Size = new System.Drawing.Size(31, 22);
         this.btColor15.TabIndex = 18;
         this.btColor15.UseVisualStyleBackColor = false;
         this.btColor15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor5
         // 
         this.btColor5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
         this.btColor5.Location = new System.Drawing.Point(191, 19);
         this.btColor5.Name = "btColor5";
         this.btColor5.Size = new System.Drawing.Size(31, 22);
         this.btColor5.TabIndex = 14;
         this.btColor5.UseVisualStyleBackColor = false;
         this.btColor5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor4
         // 
         this.btColor4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
         this.btColor4.Location = new System.Drawing.Point(148, 19);
         this.btColor4.Name = "btColor4";
         this.btColor4.Size = new System.Drawing.Size(31, 22);
         this.btColor4.TabIndex = 15;
         this.btColor4.UseVisualStyleBackColor = false;
         this.btColor4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // btColor3
         // 
         this.btColor3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
         this.btColor3.Location = new System.Drawing.Point(102, 19);
         this.btColor3.Name = "btColor3";
         this.btColor3.Size = new System.Drawing.Size(31, 22);
         this.btColor3.TabIndex = 16;
         this.btColor3.UseVisualStyleBackColor = false;
         this.btColor3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_color_pallete_MouseUp);
         // 
         // pnExtension
         // 
         this.pnExtension.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pnExtension.Controls.Add(this.label2);
         this.pnExtension.Controls.Add(this.tbPanelDrawingY);
         this.pnExtension.Controls.Add(this.label1);
         this.pnExtension.Controls.Add(this.tbPanelDrawingX);
         this.pnExtension.Controls.Add(this.btSetPanelDrawingSize);
         this.pnExtension.Controls.Add(this.lbСhooseResolution);
         this.pnExtension.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnExtension.Location = new System.Drawing.Point(0, 199);
         this.pnExtension.Name = "pnExtension";
         this.pnExtension.Size = new System.Drawing.Size(237, 126);
         this.pnExtension.TabIndex = 29;
         this.pnExtension.Visible = false;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(18, 67);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(80, 13);
         this.label2.TabIndex = 6;
         this.label2.Text = "По вертикали:";
         // 
         // tbPanelDrawingY
         // 
         this.tbPanelDrawingY.Location = new System.Drawing.Point(113, 64);
         this.tbPanelDrawingY.Name = "tbPanelDrawingY";
         this.tbPanelDrawingY.Size = new System.Drawing.Size(100, 20);
         this.tbPanelDrawingY.TabIndex = 5;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(18, 40);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(91, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "По горизонтали:";
         // 
         // tbPanelDrawingX
         // 
         this.tbPanelDrawingX.Location = new System.Drawing.Point(113, 37);
         this.tbPanelDrawingX.Name = "tbPanelDrawingX";
         this.tbPanelDrawingX.Size = new System.Drawing.Size(100, 20);
         this.tbPanelDrawingX.TabIndex = 3;
         // 
         // btSetPanelDrawingSize
         // 
         this.btSetPanelDrawingSize.Location = new System.Drawing.Point(77, 95);
         this.btSetPanelDrawingSize.Name = "btSetPanelDrawingSize";
         this.btSetPanelDrawingSize.Size = new System.Drawing.Size(75, 23);
         this.btSetPanelDrawingSize.TabIndex = 2;
         this.btSetPanelDrawingSize.Text = "Применить";
         this.btSetPanelDrawingSize.UseVisualStyleBackColor = true;
         this.btSetPanelDrawingSize.Click += new System.EventHandler(this.btSetPanelDrawingSize_Click);
         // 
         // lbСhooseResolution
         // 
         this.lbСhooseResolution.AutoSize = true;
         this.lbСhooseResolution.Location = new System.Drawing.Point(18, 1);
         this.lbСhooseResolution.Name = "lbСhooseResolution";
         this.lbСhooseResolution.Size = new System.Drawing.Size(195, 26);
         this.lbСhooseResolution.TabIndex = 1;
         this.lbСhooseResolution.Text = "Задайте размер окна для рисования\r\n                        (в пикселях):";
         // 
         // twMain
         // 
         this.twMain.BackColor = System.Drawing.Color.SteelBlue;
         this.twMain.Dock = System.Windows.Forms.DockStyle.Top;
         this.twMain.Location = new System.Drawing.Point(0, 0);
         this.twMain.Name = "twMain";
         treeNode1.Name = "NodeExtension";
         treeNode1.Text = "Размер";
         treeNode2.Name = "NodePallette";
         treeNode2.Text = "Палитра";
         treeNode3.Name = "NodeBackground";
         treeNode3.Text = "Фон";
         treeNode4.Name = "NodeMainConfigures";
         treeNode4.Text = "Основные параметры";
         treeNode5.Name = "NodeGeomFigures";
         treeNode5.Text = "Геометрические фигуры";
         treeNode6.Name = "NodeDrawing";
         treeNode6.Text = "Проектирование";
         this.twMain.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode6});
         this.twMain.Size = new System.Drawing.Size(237, 199);
         this.twMain.TabIndex = 28;
         this.twMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
         // 
         // pnOptions
         // 
         this.pnOptions.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnOptions.BackColor = System.Drawing.SystemColors.ActiveCaption;
         this.pnOptions.Controls.Add(this.btScaling);
         this.pnOptions.Controls.Add(this.btSelect);
         this.pnOptions.Location = new System.Drawing.Point(348, 0);
         this.pnOptions.Name = "pnOptions";
         this.pnOptions.Size = new System.Drawing.Size(136, 24);
         this.pnOptions.TabIndex = 2;
         // 
         // btScaling
         // 
         this.btScaling.BackgroundImage = global::EVGR.Properties.Resources.zoom;
         this.btScaling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
         this.btScaling.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.btScaling.Location = new System.Drawing.Point(40, 0);
         this.btScaling.Name = "btScaling";
         this.btScaling.Size = new System.Drawing.Size(30, 24);
         this.btScaling.TabIndex = 10;
         this.btScaling.UseVisualStyleBackColor = true;
         this.btScaling.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonScaling_MouseDown);
         // 
         // btSelect
         // 
         this.btSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
         this.btSelect.BackgroundImage = global::EVGR.Properties.Resources.select;
         this.btSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
         this.btSelect.Location = new System.Drawing.Point(4, 0);
         this.btSelect.Name = "btSelect";
         this.btSelect.Size = new System.Drawing.Size(30, 24);
         this.btSelect.TabIndex = 0;
         this.btSelect.UseVisualStyleBackColor = false;
         this.btSelect.Click += new System.EventHandler(this.button_select_Click);
         // 
         // btJournal
         // 
         this.btJournal.Dock = System.Windows.Forms.DockStyle.Top;
         this.btJournal.Location = new System.Drawing.Point(2, 2);
         this.btJournal.Name = "btJournal";
         this.btJournal.Size = new System.Drawing.Size(303, 33);
         this.btJournal.TabIndex = 1;
         this.btJournal.Text = "Журнал";
         this.btJournal.UseVisualStyleBackColor = true;
         this.btJournal.Click += new System.EventHandler(this.button_journal_Click);
         // 
         // pnPropertiesAndJournal
         // 
         this.pnPropertiesAndJournal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnPropertiesAndJournal.BackColor = System.Drawing.Color.SteelBlue;
         this.pnPropertiesAndJournal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
         this.pnPropertiesAndJournal.Controls.Add(this.pnPropertiesFiguresDUBLICATE);
         this.pnPropertiesAndJournal.Controls.Add(this.lbFiguresProps);
         this.pnPropertiesAndJournal.Controls.Add(this.pnForJournal);
         this.pnPropertiesAndJournal.Controls.Add(this.btJournal);
         this.pnPropertiesAndJournal.Dock = System.Windows.Forms.DockStyle.Right;
         this.pnPropertiesAndJournal.Location = new System.Drawing.Point(1028, 24);
         this.pnPropertiesAndJournal.Name = "pnPropertiesAndJournal";
         this.pnPropertiesAndJournal.Padding = new System.Windows.Forms.Padding(2);
         this.pnPropertiesAndJournal.Size = new System.Drawing.Size(311, 938);
         this.pnPropertiesAndJournal.TabIndex = 3;
         // 
         // pnPropertiesFiguresDUBLICATE
         // 
         this.pnPropertiesFiguresDUBLICATE.AutoScroll = true;
         this.pnPropertiesFiguresDUBLICATE.AutoSize = true;
         this.pnPropertiesFiguresDUBLICATE.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnPropertiesFiguresDUBLICATE.Controls.Add(this.pnFiguresProps);
         this.pnPropertiesFiguresDUBLICATE.Dock = System.Windows.Forms.DockStyle.Fill;
         this.pnPropertiesFiguresDUBLICATE.Location = new System.Drawing.Point(2, 427);
         this.pnPropertiesFiguresDUBLICATE.Name = "pnPropertiesFiguresDUBLICATE";
         this.pnPropertiesFiguresDUBLICATE.Size = new System.Drawing.Size(303, 505);
         this.pnPropertiesFiguresDUBLICATE.TabIndex = 6;
         // 
         // pnFiguresProps
         // 
         this.pnFiguresProps.AutoScroll = true;
         this.pnFiguresProps.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnFiguresProps.BackColor = System.Drawing.SystemColors.ActiveCaption;
         this.pnFiguresProps.Controls.Add(this.pnPolygonsProps);
         this.pnFiguresProps.Controls.Add(this.btPolygonsPropsOnOff);
         this.pnFiguresProps.Controls.Add(this.pnArcsProps);
         this.pnFiguresProps.Controls.Add(this.btArcsPropsOnOff);
         this.pnFiguresProps.Controls.Add(this.pnEllipsesProps);
         this.pnFiguresProps.Controls.Add(this.btEllipsesPropsOnOff);
         this.pnFiguresProps.Controls.Add(this.pnPiesProps);
         this.pnFiguresProps.Controls.Add(this.btPiesPropsOnOff);
         this.pnFiguresProps.Controls.Add(this.pnRectanglesProps);
         this.pnFiguresProps.Controls.Add(this.btRectanglesPropsOnOff);
         this.pnFiguresProps.Controls.Add(this.pnLinesProps);
         this.pnFiguresProps.Controls.Add(this.btLinesPropsOnOff);
         this.pnFiguresProps.Dock = System.Windows.Forms.DockStyle.Fill;
         this.pnFiguresProps.Location = new System.Drawing.Point(0, 0);
         this.pnFiguresProps.Name = "pnFiguresProps";
         this.pnFiguresProps.Size = new System.Drawing.Size(303, 505);
         this.pnFiguresProps.TabIndex = 3;
         // 
         // pnPolygonsProps
         // 
         this.pnPolygonsProps.AutoSize = true;
         this.pnPolygonsProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnPolygonsProps.Location = new System.Drawing.Point(0, 240);
         this.pnPolygonsProps.Name = "pnPolygonsProps";
         this.pnPolygonsProps.Size = new System.Drawing.Size(303, 0);
         this.pnPolygonsProps.TabIndex = 12;
         this.pnPolygonsProps.Visible = false;
         // 
         // btPolygonsPropsOnOff
         // 
         this.btPolygonsPropsOnOff.Dock = System.Windows.Forms.DockStyle.Top;
         this.btPolygonsPropsOnOff.Location = new System.Drawing.Point(0, 200);
         this.btPolygonsPropsOnOff.Name = "btPolygonsPropsOnOff";
         this.btPolygonsPropsOnOff.Size = new System.Drawing.Size(303, 40);
         this.btPolygonsPropsOnOff.TabIndex = 11;
         this.btPolygonsPropsOnOff.Text = "Многоугольники";
         this.btPolygonsPropsOnOff.UseVisualStyleBackColor = true;
         this.btPolygonsPropsOnOff.Click += new System.EventHandler(this.button_properties_polygonsOnOff_Click);
         // 
         // pnArcsProps
         // 
         this.pnArcsProps.AutoSize = true;
         this.pnArcsProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnArcsProps.Location = new System.Drawing.Point(0, 200);
         this.pnArcsProps.Name = "pnArcsProps";
         this.pnArcsProps.Size = new System.Drawing.Size(303, 0);
         this.pnArcsProps.TabIndex = 10;
         this.pnArcsProps.Visible = false;
         // 
         // btArcsPropsOnOff
         // 
         this.btArcsPropsOnOff.Dock = System.Windows.Forms.DockStyle.Top;
         this.btArcsPropsOnOff.Location = new System.Drawing.Point(0, 160);
         this.btArcsPropsOnOff.Name = "btArcsPropsOnOff";
         this.btArcsPropsOnOff.Size = new System.Drawing.Size(303, 40);
         this.btArcsPropsOnOff.TabIndex = 9;
         this.btArcsPropsOnOff.Text = "Дуги";
         this.btArcsPropsOnOff.UseVisualStyleBackColor = true;
         this.btArcsPropsOnOff.Click += new System.EventHandler(this.button_properties_arcsOnOff_Click);
         // 
         // pnEllipsesProps
         // 
         this.pnEllipsesProps.AutoSize = true;
         this.pnEllipsesProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnEllipsesProps.Location = new System.Drawing.Point(0, 160);
         this.pnEllipsesProps.Name = "pnEllipsesProps";
         this.pnEllipsesProps.Size = new System.Drawing.Size(303, 0);
         this.pnEllipsesProps.TabIndex = 8;
         this.pnEllipsesProps.Visible = false;
         // 
         // btEllipsesPropsOnOff
         // 
         this.btEllipsesPropsOnOff.Dock = System.Windows.Forms.DockStyle.Top;
         this.btEllipsesPropsOnOff.Location = new System.Drawing.Point(0, 120);
         this.btEllipsesPropsOnOff.Name = "btEllipsesPropsOnOff";
         this.btEllipsesPropsOnOff.Size = new System.Drawing.Size(303, 40);
         this.btEllipsesPropsOnOff.TabIndex = 7;
         this.btEllipsesPropsOnOff.Text = "Окружности";
         this.btEllipsesPropsOnOff.UseVisualStyleBackColor = true;
         this.btEllipsesPropsOnOff.Click += new System.EventHandler(this.button_properties_ellipsesOnOff_Click);
         // 
         // pnPiesProps
         // 
         this.pnPiesProps.AutoSize = true;
         this.pnPiesProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnPiesProps.Location = new System.Drawing.Point(0, 120);
         this.pnPiesProps.Name = "pnPiesProps";
         this.pnPiesProps.Size = new System.Drawing.Size(303, 0);
         this.pnPiesProps.TabIndex = 6;
         this.pnPiesProps.Visible = false;
         // 
         // btPiesPropsOnOff
         // 
         this.btPiesPropsOnOff.Dock = System.Windows.Forms.DockStyle.Top;
         this.btPiesPropsOnOff.Location = new System.Drawing.Point(0, 80);
         this.btPiesPropsOnOff.Name = "btPiesPropsOnOff";
         this.btPiesPropsOnOff.Size = new System.Drawing.Size(303, 40);
         this.btPiesPropsOnOff.TabIndex = 5;
         this.btPiesPropsOnOff.Text = "Секторы";
         this.btPiesPropsOnOff.UseVisualStyleBackColor = true;
         this.btPiesPropsOnOff.Click += new System.EventHandler(this.button_properties_piesOnOff_Click);
         // 
         // pnRectanglesProps
         // 
         this.pnRectanglesProps.AutoSize = true;
         this.pnRectanglesProps.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnRectanglesProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnRectanglesProps.Location = new System.Drawing.Point(0, 80);
         this.pnRectanglesProps.Name = "pnRectanglesProps";
         this.pnRectanglesProps.Size = new System.Drawing.Size(303, 0);
         this.pnRectanglesProps.TabIndex = 4;
         this.pnRectanglesProps.Visible = false;
         // 
         // btRectanglesPropsOnOff
         // 
         this.btRectanglesPropsOnOff.Dock = System.Windows.Forms.DockStyle.Top;
         this.btRectanglesPropsOnOff.Location = new System.Drawing.Point(0, 40);
         this.btRectanglesPropsOnOff.Name = "btRectanglesPropsOnOff";
         this.btRectanglesPropsOnOff.Size = new System.Drawing.Size(303, 40);
         this.btRectanglesPropsOnOff.TabIndex = 3;
         this.btRectanglesPropsOnOff.Text = "Прямоугольники";
         this.btRectanglesPropsOnOff.UseVisualStyleBackColor = true;
         this.btRectanglesPropsOnOff.Click += new System.EventHandler(this.button_properties_rectanglesOnOff_Click);
         // 
         // pnLinesProps
         // 
         this.pnLinesProps.AutoSize = true;
         this.pnLinesProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnLinesProps.Location = new System.Drawing.Point(0, 40);
         this.pnLinesProps.Name = "pnLinesProps";
         this.pnLinesProps.Size = new System.Drawing.Size(303, 0);
         this.pnLinesProps.TabIndex = 2;
         this.pnLinesProps.Visible = false;
         // 
         // btLinesPropsOnOff
         // 
         this.btLinesPropsOnOff.Dock = System.Windows.Forms.DockStyle.Top;
         this.btLinesPropsOnOff.Location = new System.Drawing.Point(0, 0);
         this.btLinesPropsOnOff.Name = "btLinesPropsOnOff";
         this.btLinesPropsOnOff.Size = new System.Drawing.Size(303, 40);
         this.btLinesPropsOnOff.TabIndex = 1;
         this.btLinesPropsOnOff.Text = "Линии";
         this.btLinesPropsOnOff.UseVisualStyleBackColor = true;
         this.btLinesPropsOnOff.Click += new System.EventHandler(this.button_properties_linesOnOff_Click);
         // 
         // lbFiguresProps
         // 
         this.lbFiguresProps.BackColor = System.Drawing.Color.SteelBlue;
         this.lbFiguresProps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.lbFiguresProps.Dock = System.Windows.Forms.DockStyle.Top;
         this.lbFiguresProps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
         this.lbFiguresProps.Location = new System.Drawing.Point(2, 405);
         this.lbFiguresProps.Name = "lbFiguresProps";
         this.lbFiguresProps.Size = new System.Drawing.Size(303, 22);
         this.lbFiguresProps.TabIndex = 4;
         this.lbFiguresProps.Text = "Список фигур и их параметры:";
         this.lbFiguresProps.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // pnForJournal
         // 
         this.pnForJournal.Controls.Add(this.tbJornal);
         this.pnForJournal.Dock = System.Windows.Forms.DockStyle.Top;
         this.pnForJournal.Location = new System.Drawing.Point(2, 35);
         this.pnForJournal.Name = "pnForJournal";
         this.pnForJournal.Size = new System.Drawing.Size(303, 370);
         this.pnForJournal.TabIndex = 2;
         // 
         // tbJornal
         // 
         this.tbJornal.BackColor = System.Drawing.SystemColors.Info;
         this.tbJornal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.tbJornal.Dock = System.Windows.Forms.DockStyle.Top;
         this.tbJornal.Location = new System.Drawing.Point(0, 0);
         this.tbJornal.Name = "tbJornal";
         this.tbJornal.ReadOnly = true;
         this.tbJornal.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
         this.tbJornal.Size = new System.Drawing.Size(303, 370);
         this.tbJornal.TabIndex = 0;
         this.tbJornal.Text = "";
         // 
         // msMain
         // 
         this.msMain.BackColor = System.Drawing.SystemColors.ActiveCaption;
         this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miPictureConfigures,
            this.miAboutProgram,
            this.miAboutCreator});
         this.msMain.Location = new System.Drawing.Point(0, 0);
         this.msMain.Name = "msMain";
         this.msMain.Size = new System.Drawing.Size(1339, 24);
         this.msMain.TabIndex = 4;
         this.msMain.Text = "menuStrip1";
         // 
         // miFile
         // 
         this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNew,
            this.miOpen,
            this.toolStripSeparator1,
            this.miSave,
            this.toolStripSeparator2,
            this.miExit});
         this.miFile.Name = "miFile";
         this.miFile.Size = new System.Drawing.Size(48, 20);
         this.miFile.Text = "&Файл";
         // 
         // miNew
         // 
         this.miNew.Image = global::EVGR.Properties.Resources.newFile;
         this.miNew.Name = "miNew";
         this.miNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
         this.miNew.Size = new System.Drawing.Size(239, 28);
         this.miNew.Text = "&Создать...";
         this.miNew.Click += new System.EventHandler(this.miNew_Click);
         // 
         // miOpen
         // 
         this.miOpen.Image = global::EVGR.Properties.Resources.open;
         this.miOpen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
         this.miOpen.Name = "miOpen";
         this.miOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
         this.miOpen.Size = new System.Drawing.Size(239, 28);
         this.miOpen.Text = "&Открыть...";
         this.miOpen.Click += new System.EventHandler(this.miOpen_Click);
         // 
         // toolStripSeparator1
         // 
         this.toolStripSeparator1.Name = "toolStripSeparator1";
         this.toolStripSeparator1.Size = new System.Drawing.Size(236, 6);
         // 
         // miSave
         // 
         this.miSave.Image = global::EVGR.Properties.Resources.saveAs;
         this.miSave.Name = "miSave";
         this.miSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
         this.miSave.Size = new System.Drawing.Size(239, 28);
         this.miSave.Text = "&Сохранить как...";
         this.miSave.Click += new System.EventHandler(this.miSaveAs_Click);
         // 
         // toolStripSeparator2
         // 
         this.toolStripSeparator2.Name = "toolStripSeparator2";
         this.toolStripSeparator2.Size = new System.Drawing.Size(236, 6);
         // 
         // miExit
         // 
         this.miExit.Name = "miExit";
         this.miExit.Size = new System.Drawing.Size(239, 28);
         this.miExit.Text = "Выход";
         this.miExit.Click += new System.EventHandler(this.ToolStripMenuItemExit_Click);
         // 
         // miPictureConfigures
         // 
         this.miPictureConfigures.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAntialiasing});
         this.miPictureConfigures.Name = "miPictureConfigures";
         this.miPictureConfigures.Size = new System.Drawing.Size(95, 20);
         this.miPictureConfigures.Text = "&Изображение";
         // 
         // miAntialiasing
         // 
         this.miAntialiasing.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAntialiasing_on,
            this.miAntialiasing_off});
         this.miAntialiasing.Name = "miAntialiasing";
         this.miAntialiasing.Size = new System.Drawing.Size(148, 22);
         this.miAntialiasing.Text = "Сглаживание";
         // 
         // miAntialiasing_on
         // 
         this.miAntialiasing_on.Checked = true;
         this.miAntialiasing_on.CheckState = System.Windows.Forms.CheckState.Checked;
         this.miAntialiasing_on.Image = global::EVGR.Properties.Resources.AntiAliasing_True;
         this.miAntialiasing_on.Name = "miAntialiasing_on";
         this.miAntialiasing_on.Size = new System.Drawing.Size(138, 22);
         this.miAntialiasing_on.Text = "Включено";
         this.miAntialiasing_on.Click += new System.EventHandler(this.ToolStripMenuItem_antialiasing_on_Click);
         // 
         // miAntialiasing_off
         // 
         this.miAntialiasing_off.Image = global::EVGR.Properties.Resources.AntiAliasingDisabled;
         this.miAntialiasing_off.Name = "miAntialiasing_off";
         this.miAntialiasing_off.Size = new System.Drawing.Size(138, 22);
         this.miAntialiasing_off.Text = "Отключено";
         this.miAntialiasing_off.Click += new System.EventHandler(this.ToolStripMenuItem_antialiasing_off_Click);
         // 
         // miAboutProgram
         // 
         this.miAboutProgram.Name = "miAboutProgram";
         this.miAboutProgram.Size = new System.Drawing.Size(94, 20);
         this.miAboutProgram.Text = "О программе";
         this.miAboutProgram.Click += new System.EventHandler(this.miAboutProgram_Click);
         // 
         // miAboutCreator
         // 
         this.miAboutCreator.Name = "miAboutCreator";
         this.miAboutCreator.Size = new System.Drawing.Size(75, 20);
         this.miAboutCreator.Text = "Об авторе";
         this.miAboutCreator.Click += new System.EventHandler(this.miAboutCreator_Click);
         // 
         // fdOpen
         // 
         this.fdOpen.FileName = "openFileDialog1";
         // 
         // pnUnderPanelDrawing
         // 
         this.pnUnderPanelDrawing.AutoScroll = true;
         this.pnUnderPanelDrawing.AutoSize = true;
         this.pnUnderPanelDrawing.BackColor = System.Drawing.SystemColors.ControlDark;
         this.pnUnderPanelDrawing.Controls.Add(this.pnPanelDrawing);
         this.pnUnderPanelDrawing.Dock = System.Windows.Forms.DockStyle.Fill;
         this.pnUnderPanelDrawing.Location = new System.Drawing.Point(239, 24);
         this.pnUnderPanelDrawing.Name = "pnUnderPanelDrawing";
         this.pnUnderPanelDrawing.Size = new System.Drawing.Size(789, 938);
         this.pnUnderPanelDrawing.TabIndex = 6;
         // 
         // pnPanelDrawing
         // 
         this.pnPanelDrawing.AllowDrop = true;
         this.pnPanelDrawing.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.pnPanelDrawing.BackColor = System.Drawing.Color.White;
         this.pnPanelDrawing.Cursor = System.Windows.Forms.Cursors.Cross;
         this.pnPanelDrawing.Location = new System.Drawing.Point(0, 0);
         this.pnPanelDrawing.Name = "pnPanelDrawing";
         this.pnPanelDrawing.Size = new System.Drawing.Size(768, 1024);
         this.pnPanelDrawing.TabIndex = 0;
         this.pnPanelDrawing.TabStop = true;
         // 
         // FormMain
         // 
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
         this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
         this.ClientSize = new System.Drawing.Size(1339, 962);
         this.Controls.Add(this.pnUnderPanelDrawing);
         this.Controls.Add(this.pnPropertiesAndJournal);
         this.Controls.Add(this.pnOptions);
         this.Controls.Add(this.pnFullOptionsOnLeft);
         this.Controls.Add(this.msMain);
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.IsMdiContainer = true;
         this.KeyPreview = true;
         this.MainMenuStrip = this.msMain;
         this.MinimumSize = new System.Drawing.Size(1100, 800);
         this.Name = "FormMain";
         this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "EVGR";
         this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_main_FormClosing);
         this.Load += new System.EventHandler(this.Form_main_Load);
         this.pnFullOptionsOnLeft.ResumeLayout(false);
         this.pnFullOptionsOnLeft.PerformLayout();
         this.pnFiguresToDraw.ResumeLayout(false);
         this.pnFiguresToDraw.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numWidth)).EndInit();
         this.ssMain.ResumeLayout(false);
         this.ssMain.PerformLayout();
         this.pnBackground.ResumeLayout(false);
         this.pnBackground.PerformLayout();
         this.pnForPallette.ResumeLayout(false);
         this.pnForPallette.PerformLayout();
         this.pnExtension.ResumeLayout(false);
         this.pnExtension.PerformLayout();
         this.pnOptions.ResumeLayout(false);
         this.pnPropertiesAndJournal.ResumeLayout(false);
         this.pnPropertiesAndJournal.PerformLayout();
         this.pnPropertiesFiguresDUBLICATE.ResumeLayout(false);
         this.pnFiguresProps.ResumeLayout(false);
         this.pnFiguresProps.PerformLayout();
         this.pnForJournal.ResumeLayout(false);
         this.msMain.ResumeLayout(false);
         this.msMain.PerformLayout();
         this.pnUnderPanelDrawing.ResumeLayout(false);
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnFullOptionsOnLeft;
        private System.Windows.Forms.Label lbСhooseResolution;
        private System.Windows.Forms.Button btColor1;
        private System.Windows.Forms.Label lbPallette;
        private System.Windows.Forms.Button btColor14;
        private System.Windows.Forms.Button btColor15;
        private System.Windows.Forms.Button btColor2;
        private System.Windows.Forms.Button btColor3;
        private System.Windows.Forms.Button btColor4;
        private System.Windows.Forms.Button btColor5;
        private System.Windows.Forms.Button btColor6;
        private System.Windows.Forms.Button btColor7;
        private System.Windows.Forms.Button btColor8;
        private System.Windows.Forms.Button btColor9;
        private System.Windows.Forms.Button btColor10;
        private System.Windows.Forms.Button btColor11;
        private System.Windows.Forms.Button btColor12;
        private System.Windows.Forms.Button btColor13;
        private System.Windows.Forms.Label lbChooseBackgroundColor;
        private System.Windows.Forms.Button btColor19;
        private System.Windows.Forms.Button btColor16;
        private System.Windows.Forms.Button btColor17;
        private System.Windows.Forms.Button btColor18;
        private System.Windows.Forms.ComboBox cbPanelDrawingBackground;
        private System.Windows.Forms.Button btColor20;
        private System.Windows.Forms.TreeView twMain;
        private System.Windows.Forms.Panel pnExtension;
        private System.Windows.Forms.Panel pnForPallette;
        private System.Windows.Forms.Panel pnBackground;
        private System.Windows.Forms.Panel pnOptions;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.Button btJournal;
        private System.Windows.Forms.Panel pnPropertiesAndJournal;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miNew;
        private System.Windows.Forms.ToolStripMenuItem miOpen;
        private System.Windows.Forms.OpenFileDialog fdOpen;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.SaveFileDialog fdSave;
        private System.Windows.Forms.Panel pnForJournal;
        private System.Windows.Forms.Panel pnFiguresProps;
        private System.Windows.Forms.Button btLinesPropsOnOff;
        private System.Windows.Forms.Panel pnLinesProps;
        private System.Windows.Forms.Button btRectanglesPropsOnOff;
        private System.Windows.Forms.Panel pnRectanglesProps;
        private System.Windows.Forms.Panel pnPiesProps;
        private System.Windows.Forms.Button btPiesPropsOnOff;
        private System.Windows.Forms.Button btEllipsesPropsOnOff;
        private System.Windows.Forms.Panel pnEllipsesProps;
        private System.Windows.Forms.Button btArcsPropsOnOff;
        private System.Windows.Forms.Panel pnArcsProps;
        private System.Windows.Forms.Button btPolygonsPropsOnOff;
        private System.Windows.Forms.Panel pnPolygonsProps;
        private System.Windows.Forms.StatusStrip ssMain;
        private System.Windows.Forms.NumericUpDown numWidth;
        private System.Windows.Forms.Label lbWidth;
        private System.Windows.Forms.Panel pnFiguresToDraw;
        private System.Windows.Forms.Button btColorFilling;
        private System.Windows.Forms.Label lbColorFilling;
        private System.Windows.Forms.Label lbFigures;
        private System.Windows.Forms.Button btRectangleDrawing;
        private System.Windows.Forms.Button btPolygonDrawing;
        private System.Windows.Forms.Button btPieDrawing;
        private System.Windows.Forms.Button btEllipseDrawing;
        private System.Windows.Forms.Button btArcDrawing;
        private System.Windows.Forms.Button btLineDrawing;
        private System.Windows.Forms.Label lbColorContour;
        private System.Windows.Forms.Button btColorContour;
        private System.Windows.Forms.CheckBox cbCountoring;
        private System.Windows.Forms.CheckBox cbFilling;
        private System.Windows.Forms.Label lbFiguresProps;
        private System.Windows.Forms.Panel pnPropertiesFiguresDUBLICATE;
        private System.Windows.Forms.RichTextBox tbJornal;
        private System.Windows.Forms.Button btScaling;
        private System.Windows.Forms.ToolStripMenuItem miPictureConfigures;
        private System.Windows.Forms.ToolStripMenuItem miAntialiasing;
        private System.Windows.Forms.ToolStripMenuItem miAntialiasing_on;
        private System.Windows.Forms.ToolStripMenuItem miAntialiasing_off;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private PanelUnderDrawingPanel pnUnderPanelDrawing;
        private DrawingPanel pnPanelDrawing;
        private System.Windows.Forms.ComboBox cbHatchStyle;
        private System.Windows.Forms.Label lbChooseHatchStyle;
        private System.Windows.Forms.ComboBox cbContourStyle;
        private System.Windows.Forms.Label lbChooseContourStyle;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox tbPanelDrawingY;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbPanelDrawingX;
      private System.Windows.Forms.Button btSetPanelDrawingSize;
      private System.Windows.Forms.ToolStripMenuItem miAboutProgram;
      private System.Windows.Forms.ToolStripMenuItem miAboutCreator;
      private System.Windows.Forms.ToolStripStatusLabel lbCoordinates;
      private System.Windows.Forms.ToolStripStatusLabel lbSize;
   }
}

