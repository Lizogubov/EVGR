﻿
using System;
using System.Drawing.Drawing2D;

namespace EVGR
{
   public static class SwitchConditions
   {
      private static bool panelDrawingResizing;
      //для включения рисования той или иной фигуры,шрифта, выделения
      private static bool lineDrawing;
      private static bool rectangleDrawing;
      private static bool polygonDrawing;
      private static bool pieDrawing;
      private static bool ellipseDrawing;
      private static bool arcDrawing;
      private static bool selecting;
      //состояния рисования для многоугольника,линии и прямоугольника
      private static bool line_pressed;
      private static bool rectangle_pressed;
      private static bool polygon_pressed;
      //свойство закрашивания
      private static bool filled;
      //свойство оконтовки
      private static bool contoured;
      //перемещение/изменение
      private static bool someFigureSelected; 
      private static bool moving;
      private static bool changingFigure;
      private static bool antialiasing = true;
      //стили контура
      private static DashStyle dashStyle;
      //стили штриховки 
      private static HatchStyle hatchStyle;
      private static bool isHatchStyleOn;
      

      public static bool LineDrawing
      {
         get { return lineDrawing; }
         set { lineDrawing = value; }
      }

      public static bool RectangleDrawing
      {
         get { return rectangleDrawing; }
         set { rectangleDrawing = value; }
      }

      public static bool PolygonDrawing
      {
         get { return polygonDrawing; }
         set { polygonDrawing = value; }
      } 

      public static bool PieDrawing
      {
         get { return pieDrawing; }
         set { pieDrawing = value; }
      }

      public static bool EllipseDrawing
      {
         get { return ellipseDrawing; }
         set { ellipseDrawing = value; }
      }

      public static bool ArcDrawing
      {
         get { return arcDrawing; }
         set { arcDrawing = value; }
      }

      public static bool Selecting
      {
         get { return selecting; }
         set { selecting = value; }
      }
      
      public static bool LinePressed
      {
         get { return line_pressed; }
         set { line_pressed = value; }
      }

      public static bool RectanglePressed
      {
         get { return rectangle_pressed; }
         set { rectangle_pressed = value; }
      }

      public static bool PolygonPressed
      {
         get { return polygon_pressed; }
         set { polygon_pressed = value; }
      }

      public static bool Contoured
      {
         get { return contoured; }
         set { contoured = value; }
      }

      public static bool Filled
      {
         get { return filled; }
         set { filled = value; }
      }

      public static bool Moving
      {
         get { return moving; }
         set { moving = value; }
      }

      public static bool SomeFigureSelected
      {
         get { return someFigureSelected; }
         set { someFigureSelected = value; }
      }

      public static bool Antialiasing
      {
         get { return antialiasing; }
         set { antialiasing = value; }
      }

      public static bool PanelDrawingResizing
      {
         get { return panelDrawingResizing; }
         set { panelDrawingResizing = value; }
      }

      public static bool ChangingFigure
      {
         get { return changingFigure; }
         set { changingFigure = value; }
      }

      public static DashStyle MyDashStyle
      {
         get { return dashStyle; }
         set { dashStyle = value; }
      }

      public static HatchStyle MyHatchStyle
      {
         get { return hatchStyle; }
         set { hatchStyle = value; }
      }

      public static bool IsHatchStyleOn
      {
         get { return isHatchStyleOn; }
         set { isHatchStyleOn = value; }
      }


      /// <summary>
      /// Метод обработки состояний(например, при переключении с рисования линии на выделение) 
      /// </summary>
      public static void ChangeCondition(string str)
      {
         LineDrawing = false;
         RectangleDrawing = false;
         PolygonDrawing = false;
         PieDrawing = false;
         EllipseDrawing = false;
         ArcDrawing = false;
         Selecting = false;

         switch (str)
         {
            case "lineDrawing":
               LineDrawing = true;
               break;
            case "rectangleDrawing":
               RectangleDrawing = true;
               break;
            case "polygonDrawing":
               PolygonDrawing = true;
               break;
            case "pieDrawing":
               PieDrawing = true;
               break;
            case "ellipseDrawing":
               EllipseDrawing = true;
               break;
            case "arcDrawing":
               ArcDrawing = true;
               break;
            case "selecting":
               Selecting = true;
               break;
            default: break;
         }
      }

      /// <summary>
      /// Проверка: включен ли режим рисования хоть для одной из фигур
      /// </summary>
      public static bool IsDrawingNow()
      {
         if (LineDrawing || RectangleDrawing || PolygonDrawing ||
            PieDrawing || EllipseDrawing || ArcDrawing)
            return true;
         else return false;
      }


      /// <summary>
      /// Переключение между состояниями перемещения и изменения фигур
      /// </summary>
      public static void ChangeMoveCondition(string str)
         {
            Moving = false;
            ChangingFigure = false;

            switch (str)
         {
            case "moving":
               Moving = true;
               break;
            case "changing":
               ChangingFigure = true;
               break;
            default: break;
         }


      }

      public static void SetDashStyle(int i)
      {
         MyDashStyle = (DashStyle)Enum.ToObject(typeof(DashStyle), i);
         //if (i == 1)
         //   MyDashStyle = DashStyle.Dash;
         //else if (i == 2)
         //   MyDashStyle = DashStyle.DashDot;
         //else if (i == 3)
         //   MyDashStyle = DashStyle.DashDotDot;
         //else if (i == 4)
         //   MyDashStyle = DashStyle.Dot;
         //else MyDashStyle = DashStyle.Solid;
      }

      public static void SetHatchStyle(int i)
      {
         if (i == 0  || i > 53)
            IsHatchStyleOn = false;
         else IsHatchStyleOn = true;
         
         if(i<54)
            MyHatchStyle = (HatchStyle)Enum.ToObject(typeof(HatchStyle), i-1);
        // else MyHatchStyle = (HatchStyle)Enum.ToObject(typeof(HatchStyle), 0);
         
        //switch (i)
        // {
        //    case 0: MyHatchStyle = HatchStyle.BackwardDiagonal;  break;
        //    case 1: MyHatchStyle = HatchStyle.Horizontal; break;
        //    case 2: MyHatchStyle = HatchStyle.Vertical; break;
        //    case 3: MyHatchStyle = HatchStyle.ForwardDiagonal; break;
        //    case 4: MyHatchStyle = HatchStyle.BackwardDiagonal; break;
        //    case 5: MyHatchStyle = HatchStyle.LargeGrid; break;
        //    case 6: MyHatchStyle = HatchStyle.DiagonalCross; break;
        //    case 7: MyHatchStyle = HatchStyle.Percent05; break;
        //    case 8: MyHatchStyle = HatchStyle.Percent10; break;
        //    case 9: MyHatchStyle = HatchStyle.Percent20; break;
        //    case 10: MyHatchStyle = HatchStyle.Percent25; break;
        //    case 11: MyHatchStyle = HatchStyle.Percent30; break;
        //    case 12: MyHatchStyle = HatchStyle.Percent40; break;
        //    case 13: MyHatchStyle = HatchStyle.Percent50; break;
        //    case 14: MyHatchStyle = HatchStyle.Percent60; break;
        //    case 15: MyHatchStyle = HatchStyle.Percent70; break;
        //    case 16: MyHatchStyle = HatchStyle.Percent75; break;
        //    case 17: MyHatchStyle = HatchStyle.Percent80; break;
        //    case 18: MyHatchStyle = HatchStyle.Percent90; break;
        //    case 19: MyHatchStyle = HatchStyle.LightDownwardDiagonal; break;
        //    case 20: MyHatchStyle = HatchStyle.LightUpwardDiagonal; break;
        //    case 21: MyHatchStyle = HatchStyle.DarkDownwardDiagonal; break;
        //    case 22: MyHatchStyle = HatchStyle.DarkUpwardDiagonal; break;
        //    case 23: MyHatchStyle = HatchStyle.WideDownwardDiagonal; break;
        //    case 24: MyHatchStyle = HatchStyle.WideUpwardDiagonal; break;
        //    case 25: MyHatchStyle = HatchStyle.LightVertical; break;
        //    case 26: MyHatchStyle = HatchStyle.LightHorizontal; break;
        //    case 27: MyHatchStyle = HatchStyle.NarrowVertical; break;
        //    case 28: MyHatchStyle = HatchStyle.NarrowHorizontal; break;
        //    case 29: MyHatchStyle = HatchStyle.DarkVertical; break;
        //    case 30: MyHatchStyle = HatchStyle.DarkHorizontal; break;
        //    case 31: MyHatchStyle = HatchStyle.DashedDownwardDiagonal; break;
        //    case 32: MyHatchStyle = HatchStyle.DashedUpwardDiagonal; break;
        //    case 33: MyHatchStyle = HatchStyle.DashedHorizontal; break;
        //    case 34: MyHatchStyle = HatchStyle.DashedVertical; break;
        //    case 35: MyHatchStyle = HatchStyle.SmallConfetti; break;
        //    case 36: MyHatchStyle = HatchStyle.LargeConfetti; break;
        //    case 37: MyHatchStyle = HatchStyle.ZigZag; break;
        //    case 38: MyHatchStyle = HatchStyle.Wave; break;
        //    case 39: MyHatchStyle = HatchStyle.DiagonalBrick; break;
        //    case 40: MyHatchStyle = HatchStyle.HorizontalBrick; break;
        //    case 41: MyHatchStyle = HatchStyle.Weave; break;
        //    case 42: MyHatchStyle = HatchStyle.Plaid; break;
        //    case 43: MyHatchStyle = HatchStyle.Divot; break;
        //    case 44: MyHatchStyle = HatchStyle.DottedGrid; break;
        //    case 45: MyHatchStyle = HatchStyle.DottedDiamond; break;
        //    case 46: MyHatchStyle = HatchStyle.Shingle ; break;
        //    case 47: MyHatchStyle = HatchStyle.Trellis; break;
        //    case 48: MyHatchStyle = HatchStyle.Sphere; break;
        //    case 49: MyHatchStyle = HatchStyle.SmallGrid; break;
        //    case 50: MyHatchStyle = HatchStyle.SmallCheckerBoard; break;
        //    case 51: MyHatchStyle = HatchStyle.LargeCheckerBoard; break;
        //    case 52: MyHatchStyle = HatchStyle.DottedDiamond; break;
        //    case 53: MyHatchStyle = HatchStyle.SolidDiamond; break;
        //    default: MyHatchStyle = HatchStyle.BackwardDiagonal; break;
        // }

      }
   }
}
