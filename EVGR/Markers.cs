﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace EVGR
{
   internal static class Markers
   {
      private static byte transparency = 240;
      private static short koef = 0;
      private static SolidBrush transparentSB = new SolidBrush(Color.FromArgb(transparency, 255, 255, 255));
      private static Pen blackTransparentPen = new Pen(Color.FromArgb(transparency, 0, 0, 0), 1);
      private static Pen whiteTransparentPen = new Pen(Color.FromArgb(transparency, 255, 255, 255), 1);



      public static void DrawMarkersForLine(PaintEventArgs e, MyLines item)
      {
         GetNewTransparentLevel();
         DrawStartMarker(e, item);
         DrawEndMarker(e, item);
         DrawReferenceMarker(e, item);
         CounterIncrementing();
         if(Figures.AngleChanging)
            DrawArrowToChangeAngle(e, item);
      }

      public static void DrawMarkersForRectangle(PaintEventArgs e, MyRectangle item)
      {
         GetNewTransparentLevel();
         DrawArrPointsMarkers(e, item, 4);
         DrawReferenceMarker(e, item);
         DrawMarkersForRectangleSides(e,item);
         CounterIncrementing();
         if (Figures.AngleChanging)
            DrawArrowToChangeAngle(e, item);
      }

      public static void DrawMarkersForPolygon(PaintEventArgs e, MyPolygon item)
      {
         GetNewTransparentLevel();
         DrawArrPointsMarkers(e, item, item.Points.Count());
         DrawReferenceMarker(e, item);
         CounterIncrementing();
         if (Figures.AngleChanging)
            DrawArrowToChangeAngle(e, item);
      }

      public static void DrawMarkersForEllipse(PaintEventArgs e, MyEllipse item)
      {
         GetNewTransparentLevel();
         DrawStartMarker(e, item);
         DrawEndMarker(e, item);

         // to do дорисовать стрелку при изменении радиуса
         CounterIncrementing();
      }

      public static void DrawMarkersForPie(PaintEventArgs e, MyPie item)
      {
         GetNewTransparentLevel();
         DrawStartMarker(e, item);
         DrawEndMarker(e, item);
         DrawReferenceMarker(e, item);
         if (Figures.AngleChanging)
            DrawArrowToChangeAngle(e, item);

         // to do дорисовать стрелку при изменении радиуса
         CounterIncrementing();
      }

      public static void DrawMarkersForArc(PaintEventArgs e, MyArc item)
      {
         GetNewTransparentLevel();
         DrawStartMarker(e, item);
         DrawEndMarker(e, item);
         DrawReferenceMarker(e, item);
         if (Figures.AngleChanging)
            DrawArrowToChangeAngle(e, item);

         // to do дорисовать стрелку при изменении радиуса
         CounterIncrementing();
      }


      /// <summary>
      /// маркер для точки опоры
      /// </summary>
      private static void DrawReferenceMarker(PaintEventArgs e, dynamic item)
      {
         e.Graphics.FillEllipse(transparentSB, item.Reference.X - 6, item.Reference.Y - 6, 12, 12);
         e.Graphics.DrawEllipse(blackTransparentPen, item.Reference.X - 5, item.Reference.Y - 5, 10, 10);
         e.Graphics.DrawLine(blackTransparentPen, item.Reference.X - 5, item.Reference.Y, item.Reference.X + 5,
            item.Reference.Y);
         e.Graphics.DrawLine(blackTransparentPen, item.Reference.X, item.Reference.Y - 5, item.Reference.X,
            item.Reference.Y + 5);
      }

      
      /// <summary>
      /// Метод выполняет отрисовку стрелки для поворота фигуры
      /// </summary>
      private static void DrawArrowToChangeAngle(PaintEventArgs e, dynamic item)
      {
         
         PointF pointEnd = DrawingPanel.Elocation;
         double x1 = item.Reference.X;
         double y1 = item.Reference.Y;
         double x2 = pointEnd.X;
         double y2 = pointEnd.Y;
         float startAngle = (float) (((Math.Atan2(y2 - y1, x2 - x1)*180)/Math.PI) - 60/2);
         float Wh = (float)(2.0 * Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))); 
         whiteTransparentPen.EndCap = LineCap.ArrowAnchor;
         whiteTransparentPen.StartCap = LineCap.ArrowAnchor;
         blackTransparentPen.EndCap = LineCap.ArrowAnchor;
         blackTransparentPen.StartCap = LineCap.ArrowAnchor;
         whiteTransparentPen.Width = 2;
         blackTransparentPen.Width = 1;
         if (Wh > 15)
         {
            e.Graphics.DrawArc(whiteTransparentPen, item.Reference.X - Wh/2, item.Reference.Y -
               Wh/2, Wh, Wh, startAngle-2,60.0f);
            e.Graphics.DrawArc(blackTransparentPen, item.Reference.X - Wh/2, item.Reference.Y -
               Wh/2, Wh, Wh, startAngle,56.0f);  
         }
      }

      /// <summary>
      /// маркер для точки начала
      /// </summary>
      private static void DrawStartMarker(PaintEventArgs e, dynamic item)
      {
         e.Graphics.FillEllipse(transparentSB, item.Start.X - 6, item.Start.Y - 6, 12, 12);
         e.Graphics.DrawEllipse(blackTransparentPen, item.Start.X - 5, item.Start.Y - 5, 10, 10);
      }

      /// <summary>
      /// маркер для точки конца
      /// </summary>
      private static void DrawEndMarker(PaintEventArgs e, dynamic item)
      {
         e.Graphics.FillEllipse(transparentSB, item.End.X - 6, item.End.Y - 6, 12, 12);
         e.Graphics.DrawEllipse(blackTransparentPen, item.End.X - 5, item.End.Y - 5, 10, 10);
      }


      /// <summary>
      /// Отрисовка маркеров для боковых сторон прямоугольника
      /// </summary>
      private static void DrawMarkersForRectangleSides(PaintEventArgs e, dynamic item)
      {
         for (int i = 0; i < 4; i++)
         {
            PointF sidePoint;
            if (i < 3)
            {
               sidePoint = new PointF((item.Points[i].X + item.Points[i + 1].X)/2,
                  (item.Points[i].Y + item.Points[i + 1].Y)/2);
            }
            else
            {
               sidePoint = new PointF((item.Points[i].X + item.Points[i - 3].X) / 2,
                  (item.Points[i].Y + item.Points[i - 3].Y) / 2);
            }
            e.Graphics.FillEllipse(transparentSB, sidePoint.X - 6, sidePoint.Y - 6, 12, 12);
            e.Graphics.DrawEllipse(blackTransparentPen, sidePoint.X - 5, sidePoint.Y - 5, 10, 10);
         }
      }

      /// <summary>
      /// маркер для массива точек
      /// </summary>
      private static void DrawArrPointsMarkers(PaintEventArgs e, dynamic item, int PointsCount)
      {
         for (int i = 0; i < PointsCount; i++)
         {  
            e.Graphics.FillEllipse(transparentSB, item.Points[i].X - 6, item.Points[i].Y - 6, 12, 12);
            e.Graphics.DrawEllipse(blackTransparentPen, item.Points[i].X - 5, item.Points[i].Y - 5, 10, 10);
         }
      }

      /// <summary>
      /// Изменение прозрачности в определенном промежутке значений
      /// </summary>
      private static void CounterIncrementing()
      {
         if (transparency == 240)
            koef = -1;
         else if (transparency == 40)
            koef = 1;
         transparency += (byte) koef;
      }
      
      /// <summary>
      /// Метод определяющий новый уровень прозрачности при каждой новой отрисовке маркера
      /// </summary>
      private static void GetNewTransparentLevel()
      {
         transparentSB = new SolidBrush(Color.FromArgb(transparency, 255, 255, 255));
         blackTransparentPen = new Pen(Color.FromArgb(transparency, 0, 0, 0), 1);
      }
   }
}
