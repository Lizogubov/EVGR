﻿

namespace EVGR
{
   internal static class Constants
   {
      public const double GRAD_TO_RAD = 3.14159265359/180.0;
      public const double RAD_TO_GRAD = 180.0/3.14159265359;
   }
}
